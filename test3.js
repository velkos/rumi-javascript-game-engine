/**
 * Created by vili on 3/28/2016.
 */
var game,
    poly,
    sprite;
window.onload = function() {
    var game = new Rumi.Game();
    /*
    poly = game.create.box(new Rumi.ESATVector(150, 150), 100, 100).toPolygon();
    game.attach(poly);
    game.attach(poly.anchor);
    poly.anchor.x = 50;
    setInterval(function() {
        poly.anchor.x = Rumi.math.randomInt(0, 100);
        poly.anchor.y = Rumi.math.randomInt(0, 100);
        poly.angle += Rumi.math.toRad(1);
    }, 16);*/

    var lm = new Rumi.LoadingManager();
    lm.images({
        player: 'img/sniper.png'
    });
    lm.load(function(result) {
        sprite = game.add.spritesheet(new Rumi.ESATBox(new Rumi.ESATVector(150, 150), 53, 63), result.player, new Rumi.ESATVector(8, 1));
        sprite.anchor.x = 13;
        sprite.anchor.y = 0;
        poly = game.add.polygon(
            new Rumi.ESATVector(
                sprite.box.pos.x,
                sprite.box.pos.y
            ),
            [
                new Rumi.ESATVector(13, 0),
                new Rumi.ESATVector(13, -200)
            ]
        );
        poly.anchor.copies(sprite.anchor);

        game.input.mouse.eventsManager.attachEventListener('onMove', function() {
            sprite.angle = Rumi.math.toRad(90) + Rumi.math.toRad(Rumi.math.angleBetweenPoints(
                    new Rumi.ESATVector(sprite.box.pos.x + sprite.anchor.x, sprite.box.pos.y + sprite.anchor.y), game.input.mouse.pagePos
                )
            );
            poly.rotation = sprite.angle;
        });
        game.input.mouse.eventsManager.attachEventListener('onLeftClick', function() {

        });
    });

};