Rumi.array = {
    hasElement: function(array, obj) {
        for(var i = 0, len = array.length; i < len; ++i) {
            if(array[i] === obj) {
                return true;
            }
        }
        return false;
    },
    addIfNotPresent: function(array, obj) {
        if(!Rumi.array.hasElement(array, obj)) {
            array.push(obj);
        }
    },
    removeByVal: function(array, obj) {
        for(var i = array.length - 1; i > -1; --i) {
            if(array[i] === obj) {
                array.splice(i, 1);
                return;
            }
        }
    },
    unique: function(array) {
        var result = [];
        for(var i = 0, len = array.length; i < len; ++i) {
            Rumi.array.addIfNotPresent(result, array[i]);
        }
        return result;
    }
};