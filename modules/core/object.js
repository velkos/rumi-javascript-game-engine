Rumi.object = {
    /*
    * Returns new object with no duplicate values
    */
    uniqueValues: function(object) {
        var result = {};
        for(var p in object) {
            var foundMatch = false;
            for(var p2 in object) {
                if(p !== p2 && object[p] === object[p2]) {
                    foundMatch = true;
                    break;
                }
            }
            if(!foundMatch) {
                result[p] = object[p];
            }
        }
        return result;
    },
    /**
    *Merges two objects creating a new one which is returned.
    */
    shallowMerge: function(a, b) {
        var result = {};
        for(var p in a) {
            result[p] = a[p];
        }
        for(p in b) {
            result[p] = b[p];
        }
        return result;
    },
    /**
    *Extends object a with b's properties without creating a new object
    */
    extend: function(a, b) {
        for(var p in b) {
            a[p] = b[p];
        }
    },
    unique_extend: function(a, b) {
        for(var p in b) {
            if(a[p] === undefined) a[p] = b[p];
        }
    },
    /**
    *Empties object recursively which may lead to stack overflow in most cases depending on the object!
    */
    destroy: function(obj) {
        if(!Rumi.object.isObject(obj)) return;
        for(var p in obj) {
            if(Rumi.object.isObject(obj[p])) {
                Rumi.object.destroy(obj[p]);
            }
            delete obj[p];
        }
    },
    /**
    *Returns true if the provided argument is object else - false
    */
    isObject: function(obj) {
        return obj === Object(obj);
    }
};