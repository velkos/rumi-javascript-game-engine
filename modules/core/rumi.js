/** 
	@global
	@namespace
*/
var Rumi = {};

/**
	@memberof Rumi
	@desc Generates unique id
	@returns {string}
**/
Rumi.uid = function() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(i) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (i=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
};