/**
	@param {Array} eventNames - Array containing event names as strings
*/
Rumi.EventsManager = function(eventNames) {
	this._events = {};
	this.registerEvent(eventNames);
};

Rumi.EventsManager.prototype = {
	/**
	 * @param {String} string - String containing single or multiple event names or only * character which means all events.
	 * When the string is * then all event names from the current instance are returned in array.
	 * In the other two cases is returned Array with single or multiple event names which are not guaranteed to be registered
	 * in the current instance!
	 * @returns {Array}
	 */
	extractEventNamesFromString: function(string) {
		if(string === '*') {
			var allEventNames = [];
			for(var eventName in this._events) {
				allEventNames.push(eventName);
			}
			return allEventNames;
		}
		else if(string != '' && string.split(' ').length > 1) {
			return string.split(' ');
		}
		else {
			return [string];
		}
	},
	triggerEvent: function(eventName, argPassedToListeners) {
		var result = this.extractEventNamesFromString(eventName);
		for(var i = 0, len = result.length; i < len; ++i) {
			if(this._events[result[i]] !== undefined) {
				this._events[result[i]].trigger(argPassedToListeners);
			}
		}
		return this;
	},
	suppressEvent: function(eventName) {
		var result = this.extractEventNamesFromString(eventName);
		for(var i = 0, len = result.length; i < len; ++i) {
			if(this._events[result[i]] !== undefined) {
				this._events[result[i]].suppressed = true;
			}
		}
	},
	unsuppressEvent: function(eventName) {
		var result = this.extractEventNamesFromString(eventName);
		for(var i = 0, len = result.length; i < len; ++i) {
			if(this._events[result[i]] !== undefined) {
				this._events[result[i]].suppressed = false;
			}
		}
	},
	registerEvent: function(eventName) {
		var result = this.extractEventNamesFromString(eventName);
		for(var i = 0, len = result.length; i < len; ++i) {
			if(this._events[result[i]] === undefined) {
				this._events[result[i]] = new Rumi.Event(result[i]);
			}
		}
		return this;
	},
	unregisterEvent: function(eventName) {
		var result = this.extractEventNamesFromString(eventName);
		for(var i = 0, len = result.length; i < len; ++i) {
			if(this._events[result[i]] !== undefined) {
				delete this._events[result[i]];
			}
		}
		return this;
	},
	attachEventListener: function(eventName, listenerFunction) {
		var result = this.extractEventNamesFromString(eventName);
		for(var i = 0, len = result.length; i < len; ++i) {
			if(this._events[result[i]] !== undefined) {
				this._events[result[i]].attachListener(listenerFunction);
			}
		}
		return this;
	},
	detachEventListener: function(eventName, listenerFunction) {
		var result = this.extractEventNamesFromString(eventName);
		for(var i = 0, len = result.length; i < len; ++i) {
			if(this._events[result[i]] !== undefined) {
				this._events[result[i]].detachListener(listenerFunction);
			}
		}
		return this;
	}
};
