/**
	@param {string} name - Name of the event
*/
Rumi.Event = function(name) {
	this._name = name;
	this._suppressed = false;
	this._handlers = [];
};

Rumi.Event.prototype = {
	/*
	*Returns the name of the event
	*/
	get name() {
		return this._name;
	},
	/**
	*Returns true if the event is suppressed else - false.
	*Info: If the event is suppressed, triggering it will have no effect and won't call it's handlers!
	*/
	get suppressed() {
		return this._suppressed;
	},
	/**
	*Sets the suppressed property of the event to either true or false
	*Info: If the suppressed property of this event object is set to true, triggering the event will have no effect and won't call it's handlers!
	*/
	set suppressed(newSuppressedState) {
		this._suppressed = newSuppressedState;
	},
	/**
	*Attaches a listener function to the event. Listeners get called when the event's trigger() method is invoked.
	*Info: If the listener function is already attached to this event object this method will have no effect. 
	*/
	attachListener: function(listenerFunction) {
		Rumi.array.addIfNotPresent(this._handlers, listenerFunction);
		return this;
	},
	/**
	*Detaches a listener function from the event. The next time the event gets triggered this function will not be called.
	Info: If the listener function is not attached to the this event object this method will have no effect.
	*/
	detachListener: function(listenerFunction) {
		Rumi.array.removeByVal(this._handlers, listenerFunction);
		return this;
	},
	/*
	*Triggers this event object - effectively calling each listener function in the order they were attached to the object.
	*Info: If the suppressed property of this event object is true this method will have no effect.
	*/
	trigger: function(argPassedToListeners) {
		if(this.suppressed) return;
		for(var i = 0, len = this._handlers.length; i < len; i++) {
			this._handlers[i].call(null, argPassedToListeners);
		}
		return this;
	}
};