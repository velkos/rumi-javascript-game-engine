Rumi.math = {
	_180_DIV_PI: 180 / Math.PI,
	_PI_DIV_180: Math.PI / 180,
	distance: function(x1, y1, x2, y2) {
		return  Math.sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) );
	},
	angle: function(x1, y1, x2, y2) {
		return (Math.atan2(y2 - y1, x2 - x1) * Rumi.math._180_DIV_PI);
	},
	distanceBetweenPoints: function(a, b) {
		return Rumi.math.distance(a.x, a.y, b.x, b.y);
	},
	angleBetweenPoints: function(a, b) {
		return Rumi.math.angle(a.x, a.y, b.x, b.y);
	},
	toDeg: function(rad) {
		return rad * Rumi.math._180_DIV_PI;
	},
	toRad: function(deg) {
		return deg * Rumi.math._PI_DIV_180;
	},
	inRange: function(val, min, max) {
		return (val >= min && val <= max);
	},
	isNumber: function(val) {
		return (
			!Number.isNaN(val) &&
			Number.isFinite(val)
		);
	},
	randomInt: function(min, max) {
		return Math.floor(this.randomFloat(min, max));
	},
	randomFloat: function(min, max) {
		return (Math.random() * (max - min)) + min;
	},
	clamp: function(val, min, max) {
		if(val >= min && val <= max) {
			return val;
		}
		else if(val < min) {
			return min
		}
		else if(val > max) {
			return max;
		}
	},
	percent: function(val, percent) {
		return val * (percent / 100);
	}
};