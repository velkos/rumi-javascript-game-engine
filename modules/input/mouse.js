/**
	Represents the computer mouse over an HTML element
	@class
	@constructor
	@param {HTMLElement} element - Element this.on which to register and process mouse events
*/
Rumi.Mouse = function(HTMLElement) {
	this.eventsManager = new Rumi.EventsManager([
		'onDoubleClickIntervalChange',
		'onElementChange',
		'onMove',
		'onLeftDown',
		'onLeftUp',
		'onLeftClick',
		'onLeftDoubleClick',
		'onScrollDown',
		'onScrollUp',
		'onScrollClick',
		'onScrollDoubleClick',
		'onRightDown',
		'onRightUp',
		'onRightClick',
		'onRightDoubleClick'
	].join(' '));
	this.pagePos = new Rumi.ESATVector();
	this.clientPos = new Rumi.ESATVector();
	this.screenPos = new Rumi.ESATVector();
	this.movement = new Rumi.ESATVector();
	this.offset = new Rumi.ESATVector();

	var _doubleClickMSInterval = 500,
		_leftDown = false,
		_lastLeftClickTime = 0,
		_scrollDown = false,
		_lastScrollClickTime = 0,
		_rightDown = false,
		_lastRightClickTime = 0,
		_element = HTMLElement,
		_that = this,
		_updatePointerCoordinates = function(mouseEvent) {
			if(!_on) return;
			_that.pagePos.x = mouseEvent.pageX;
			_that.pagePos.y = mouseEvent.pageY;
			_that.clientPos.x = mouseEvent.clientX;
			_that.clientPos.y = mouseEvent.clientY;
			_that.screenPos.x = mouseEvent.screenX;
			_that.screenPos.y = mouseEvent.screenY;

			_that.offset.x = mouseEvent.offsetX;
			_that.offset.y = mouseEvent.offsetY;
			_that.movement.x = mouseEvent.movementX;
			_that.movement.y = mouseEvent.movementY;
		},
		_onMouseDown = function (e) {
			if(!_on) return;
			if (_preventDefault) e.preventDefault();
			switch (e.button) {
				case 0:
					_leftDown = true;
					_that.eventsManager.triggerEvent('onLeftDown');
					break;
				case 1:
					_scrollDown = true;
					_that.eventsManager.triggerEvent('onScrollDown');
					break;
				case 2:
					_rightDown = true;
					_that.eventsManager.triggerEvent('onRightDown');
					break;
			}
		},
		_onMouseUp = function (e) {
			if(!_on) return;
			if (_preventDefault) e.preventDefault();
			switch (e.button) {
				case 0:
					_leftDown = false;
					_that.eventsManager.triggerEvent('onLeftUp');
					_that.eventsManager.triggerEvent('onLeftClick');
					if((new Date() - _lastLeftClickTime) <= _doubleClickMSInterval) {
						_that.eventsManager.triggerEvent('onLeftDoubleClick');
					}
					_lastLeftClickTime = new Date();
					break;
				case 1:
					_scrollDown = false;
					_that.eventsManager.triggerEvent('onScrollUp');
					_that.eventsManager.triggerEvent('onScrollClick');
					if((new Date() - _lastScrollClickTime) <= _doubleClickMSInterval) {
						_that.eventsManager.triggerEvent('onScrollDoubleClick');
					}
					_lastScrollClickTime = new Date();
					break;
				case 2:
					_rightDown = false;
					_that.eventsManager.triggerEvent('onRightUp');
					_that.eventsManager.triggerEvent('onRightClick');
					if((new Date() - _lastRightClickTime) <= _doubleClickMSInterval) {
						_that.eventsManager.triggerEvent('onRightDoubleClick');
					}
					_lastRightClickTime = new Date();
					break;
			}
		},
		_preventDefault = true,
		_onMouseMove = function (e) {
			if(!_on) return;
			if (_preventDefault) e.preventDefault();
			_updatePointerCoordinates(e);
			_that.eventsManager.triggerEvent('onMove');
		},
		_routeEventsToElement = function(element) {
			element.addEventListener('mousemove', _onMouseMove);
			element.addEventListener('mousedown', _onMouseDown);
			element.addEventListener('mouseup', _onMouseUp);
		},
		_disconnectEventsFromElement = function(element) {
			element.removeEventListener('mousemove', _onMouseMove);
			element.removeEventListener('mousedown', _onMouseDown);
			element.removeEventListener('mouseup', _onMouseUp);
		},
		_destroy = function() {
			_that.eventsManager = null;
			_disconnectEventsFromElement(_element);
			for(var p in _that) {
				delete _that[p];
			}
			_element = null;
		},
		_on = true;

	_routeEventsToElement(_element);

	Object.defineProperties(this, {
		element: {
			get: function() {
				return _element;
			},
			set: function(newElement) {
				if(_element != newElement) {
					_disconnectEventsFromElement(_element);
					_element = newElement;
					_routeEventsToElement(_element);
					_that.eventsManager.triggerEvent('onElementChange');
				}
			}
		},
		doubleClickInterval: {
			get: function () {
				return _doubleClickMSInterval;
			},
			set: function(newInterval) {
				if(_doubleClickMSInterval != newInterval) {
					_doubleClickMSInterval = newInterval;
					_that.eventsManager.triggerEvent('onDoubleClickIntervalChange');
				}
			}
		},
		preventDefault: {
			get: function() {
				return _preventDefault;
			},
			set: function(newPrevent) {
				if(_preventDefault != newPrevent) {
					_preventDefault = newPrevent;
					_that.eventsManager.triggerEvent('onPreventDefaultChange');
				}
			}
		},
		on: {
			get: function() {
				return _on;
			},
			set: function(newOn) {
				if(_on != newOn) {
					_on = newOn;
					_that.eventsManager.triggerEvent('onOnChanged');
				}
			}
		},
		destroy: {
			get: function() {
				return _destroy;
			}
		}
	});
};
