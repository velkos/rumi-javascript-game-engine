/**
	Represents computer keyboard
	@constructor
	@class
*/
Rumi.Keyboard = function() {
	this.eventsManager = new Rumi.EventsManager([
		'onKeyDown',
		'onKeyUp'
	].join(' '));
	var _that = this,
		_pressedKeyCodes = [],
		_onKeyDown = function(e) {
			var code = e.which;
			if(_pressedKeyCodes.join(' ').indexOf(code) === -1) {
				_pressedKeyCodes.push(code);
				_that.eventsManager.triggerEvent('onKeyDown');
			}
		},
		_onKeyUp = function(e) {
			var code = e.which;
			if(_pressedKeyCodes.join(' ').indexOf(code) >= 0) {
				for(var i = _pressedKeyCodes.length - 1; i > -1; --i) {
					if(_pressedKeyCodes[i] == code) {
						_pressedKeyCodes.splice(i , 1);
						_that.eventsManager.triggerEvent('onKeyUp');
						break;
					}
				}
			}
		},
		_destroy = function() {
			window.removeEventListener('keydown', _onKeyDown);
			window.removeEventListener('keyup', _onKeyUp);
		};

	Object.defineProperties(this, {
		pressedKeyCodesString: {
			get: function() {
				return _pressedKeyCodes.join(' ');
			}
		},
		destroy: {
			get: function() {
				return _destroy;
			}
		}
	});

	window.addEventListener('keydown', _onKeyDown);
	window.addEventListener('keyup', _onKeyUp);
};

Rumi.Keyboard.prototype.sequenceMatch = function(arrayOfKeys) {
	return (this.pressedKeyCodesString.indexOf(arrayOfKeys.join(' ')) > -1);
};

Rumi.Keyboard.prototype.combinationMatch = function(arrayOfKeys) {
	for(var i = 0, len = arrayOfKeys.length; i < len; ++i) {
		if(this.pressedKeyCodesString.indexOf(arrayOfKeys[i]) === -1) {
			return false;
		}
	}
	return true;
};

/*Table created using this info http://www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes*/
Rumi.Keyboard.prototype.Keys = {
	BACKSPACE: 8,
	TAB: 9,
	ENTER: 13,
	SHIFT: 16,
	CTRL: 17,
	ALT: 18,
	PAUSE_BREAK: 19,
	CAPS_LOCK: 20,
	ESCAPE: 27,
	SPACE: 32,
	PAGE_UP: 33,
	PAGE_DOWN: 34,
	END: 35,
	HOME: 36,
	LEFT: 37,
	UP: 38,
	RIGHT: 39,
	DOWN: 40,
	INSERT: 45,
	DELETE: 46,
	ZERO: 48,
	ONE: 49,
	TWO: 50,
	THREE: 51,
	FOUR: 52,
	FIVE: 53,
	SIX: 54,
	SEVEN: 55,
	EIGHT: 56,
	NINE: 57,
	A: 65,
	B: 66,
	C: 67,
	D: 68,
	E: 69,
	F: 70,
	G: 71,
	H: 72,
	I: 73,
	J: 74,
	K: 75,
	L: 76,
	M: 77,
	N: 78,
	O: 79,
	P: 80,
	Q: 81,
	R: 82,
	S: 83,
	T: 84,
	U: 85,
	V: 86,
	W: 87,
	X: 88,
	Y: 89,
	Z: 90,
	LEFT_META: 91,
	RIGHT_META: 92,
	SELECT: 93,
	NUM_0: 96,
	NUM_1: 97,
	NUM_2: 98,
	NUM_3: 99,
	NUM_4: 100,
	NUM_5: 101,
	NUM_6: 102,
	NUM_7: 103,
	NUM_8: 104,
	NUM_9: 105,
	ASTERISK: 106,
	PLUS: 107,
	MINUS: 109,
	F1: 112,
	F2: 113,
	F3: 114,
	F4: 115,
	F5: 116,
	F6: 117,
	F7: 118,
	F8: 119,
	F9: 120,
	F10: 121,
	F11: 122,
	NUM_LOCK: 144,
	SCROLL_LOCK: 145,
	SEMICOLON: 186,
	EQUAL: 187,
	COMMA: 188,
	DASH: 189,
	PERIOD: 190,
	SLASH: 191,
	APOSTROPHE: 192,
	OPENING_SQUARE_BRACKET: 219,
	CLOSING_SQUARE_BRACKET: 221
};
