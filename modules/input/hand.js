/**
 * Created by vili on 3/21/2016.
 */
Rumi.Hand = function(element) {
    this.eventsManager = new Rumi.EventsManager([
        'onTouchStart',
        'onTouchEnd',
        'onTouchCancel',
        'onTouchMove'
    ].join(' '));
    this.touches = [];
    var _element = element,
        _that = this,
        _onTouchStart = function(e) {
            for(var i = 0, len = e.changedTouches.length; i < len; ++i) {
                var vector = new Rumi.ESATVector(
                    e.changedTouches[i].pageX,
                    e.changedTouches[i].pageY
                );
                vector.id = e.changedTouches[i].identifier;
                _that.touches.push(vector);
            }
            _that.eventsManager.triggerEvent('onTouchStart');
        },
        _onTouchEnd = function(e) {
            for(var i = 0, len = e.changedTouches.length; i < len; ++i) {
                for (var i2 = _that.touches.length - 1; i2 > -1; --i2) {
                    if(_that.touches[i2].id === e.changedTouches[i].identifier) {
                        _that.touches.splice(i2, 1);
                    }
                }
            }
            _that.eventsManager.triggerEvent('onTouchEnd');
        },
        _onTouchCancel = function(e) {
            for(var i = 0, len = e.changedTouches.length; i < len; ++i) {
                for (var i2 = _that.touches.length - 1; i2 > -1; --i2) {
                    if(_that.touches[i2].id === e.changedTouches[i].identifier) {
                        _that.touches.splice(i2, 1);
                    }
                }
            }
            _that.eventsManager.triggerEvent('onTouchCancel');
        },
        _onTouchMove = function(e) {
            for(var i = 0, len = e.changedTouches.length; i < len; ++i) {
                for(var i2 = 0, len2 = _that.touches.length; i2 < len2; ++i2) {
                    if(_that.touches[i2].id === e.changedTouches[i].identifier) {
                        _that.touches[i2].x = e.changedTouches[i].pageX;
                        _that.touches[i2].y = e.changedTouches[i].pageY;
                    }
                }
            }
            _that.eventsManager.triggerEvent('onTouchMove');
        },
        _destroy = function() {
            _element.removeEventListener('touchstart', _onTouchStart);
            _element.removeEventListener('touchend', _onTouchEnd);
            _element.removeEventListener('touchcancel', _onTouchCancel);
            _element.removeEventListener('touchmove', _onTouchMove);
            _that.touches = null;
        };

    _element.addEventListener('touchstart', _onTouchStart);
    _element.addEventListener('touchend', _onTouchEnd);
    _element.addEventListener('touchcancel', _onTouchCancel);
    _element.addEventListener('touchmove', _onTouchMove);

    Object.defineProperties(this, {
        destroy: {
            get: function() {
                return _destroy;
            }
        }
    });
};

Rumi.Hand.prototype.touching = function(other) {
    for(var i = 0, len = this.touches.length; i < len; ++i) {
        if(this.touches[i].colliding(other)) {
            return true;
        }
    }
    return false;
};
