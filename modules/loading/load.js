Rumi.load = {
    images: function(pseudonymToUrlHash, done) {
        pseudonymToUrlHash = Rumi.object.uniqueValues(pseudonymToUrlHash);
        var images = {},
            expected = Object.keys(pseudonymToUrlHash).length,
            loaded = 0,
            missed = 0,
            cleanHandlers = function(image) {
                delete image.pseudonym;
                image.onerror = null;
                image.onload = null;
            },
            signalIfDone = function() {
                if(loaded + missed === expected) {
                    done(images);
                }
            },
            onLoad = function() {
                loaded++;
                images[this.pseudonym] = this;
                cleanHandlers(this);
                signalIfDone();
            },
            onError = function() {
                missed++;
                cleanHandlers(this);
                signalIfDone();
            };

        for(var p in pseudonymToUrlHash) {
            var img = document.createElement('img');
            img.onload = onLoad.bind(img);
            img.onerror = onError.bind(img);
            img.pseudonym = p;
            img.src = pseudonymToUrlHash[p];
        }
    },
    audios: function(pseudonymToUrlHash, done) {
        pseudonymToUrlHash = Rumi.object.uniqueValues(pseudonymToUrlHash);
        var audios = {},
            expected = Object.keys(pseudonymToUrlHash).length,
            loaded = 0,
            missed = 0,
            cleanHandlers = function(audio) {
                delete audio.pseudonym;
                audio.onerror = null;
                audio.oncanplaythrough = null;
            },
            signalIfDone = function() {
                if(loaded + missed === expected) {
                    done(audios);
                }
            },
            onCanPlayThrough = function() {
                loaded++;
                audios[this.pseudonym] = this;
                cleanHandlers(this);
                signalIfDone();
            },
            onError = function() {
                missed++;
                cleanHandlers(this);
                signalIfDone();
            };

        for(var p in pseudonymToUrlHash) {
            var audio = document.createElement('audio');
            audio.oncanplaythrough = onCanPlayThrough.bind(audio);
            audio.onerror = onError.bind(audio);
            audio.pseudonym = p;
            audio.src = pseudonymToUrlHash[p];
        }
    }
};