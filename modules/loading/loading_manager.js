/**
 * Created by vili on 3/30/2016.
 */
Rumi.LoadingManager = function() {
    this._images = {};
    this._audios = {};
};

Rumi.LoadingManager.prototype = {
    images: function(pseudonymToUrlHash) {
        this._images = Rumi.object.shallowMerge(this._images, pseudonymToUrlHash);
    },
    audios: function(pseudonymToUrlHash) {
        this._audios = Rumi.object.shallowMerge(this._audios, pseudonymToUrlHash);
    },
    load: function(done) {
        if(this._hasEmptyQueue()) {
            done();
            return;
        }
        var result = {},
            expectCount = 0,
            count = 0,
            onAnyReady = function(files) {
                count++;
                result = Rumi.object.shallowMerge(result, files);
                if(count === expectCount) {
                    this._images = {};
                    this._audios = {};
                    done(result);
                }
            }.bind(this);
        if(Object.keys(this._images).length > 0) expectCount++;
        if(Object.keys(this._audios).length > 0) expectCount++;
        Rumi.load.images(this._images, onAnyReady);
        Rumi.load.audios(this._audios, onAnyReady);
    },
    _hasEmptyQueue: function() {
        return (Object.keys(this._images).length === 0 && Object.keys(this._audios).length === 0);
    }
};