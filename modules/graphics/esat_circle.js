/*ESAT stays for ExtendedSAT (i.e. Extended SAT Box => ESATBox*/
Rumi.ESATCircle = function(position, radius) {
    this.initSociable([
        'PositionChange',
        'RadiusChange',
        'FillChange',
        'StrokeChange'
    ].join(' '));
    this.fill = new Rumi.Fill();
    this.stroke = new Rumi.Stroke();

    this.position = position ? position.copy() : new Rumi.ESATVector();
    this.radius = radius || 0;

    this.fill.on('*', function() {
        this.trigger('FillChange');
    }.bind(this));

    this.stroke.on('*', function() {
        this.trigger('StrokeChange');
    }.bind(this));

    this.position.on('XChange YChange', function() {
        this.trigger('PositionChange');
    }.bind(this));
};

Rumi.ESATCircle.prototype = {
    draw: function(context) {
        context.beginPath();
        context.arc(this.position.x, this.position.y, this.radius, 0, 2 * Math.PI);
        context.closePath();
        this.fill.apply(context);
        this.stroke.apply(context);
        return this;
    },
    copy: function() {
        var copy = new Rumi.ESATCircle(this.position, this.radius);
        copy.stroke.copies(this.stroke);
        copy.fill.copies(this.fill);
        return copy;
    },
    copies: function(other) {
        this.pos.copies(other.position);
        this.radius = other.radius;
        this.fill.copies(other.fill);
        this.stroke.copies(other.stroke);
        return this;
    },
    get radius() {
        return this._radius;
    },
    set radius(newRadius) {
        if(this._radius != newRadius) {
            this._radius = newRadius;
            this.trigger('RadiusChange');
        }
    },
    /*For backward compatibility with SAT.js*/
    get pos() {
        return this.position;
    },
    /*For backward compatibility with SAT.js*/
    get r() {
        return this._radius;
    },
    /*For backward compatibility with SAT.js*/
    set r(newRadius) {
        if(this._radius != newRadius) {
            this._radius = newRadius;
            this.trigger('RadiusChange');
        }
    }
};

Rumi.object.unique_extend(Rumi.ESATCircle.prototype, SAT.Circle.prototype);
Rumi.object.extend(Rumi.ESATCircle.prototype, Rumi.SociableInterface);
