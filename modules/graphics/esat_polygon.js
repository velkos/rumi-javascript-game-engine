/*ESAT stays for ExtendedSAT (i.e. Extended SAT Box => ESATBox*/
Rumi.ESATPolygon = function(position, points) {
     this.initSociable([
        'PositionChange',
        'AnchorChange',
        'AngleChange',
        'PointsChange',
        'OffsetChange',
        'FillChange',
        'StrokeChange'
    ].join(' '));
    this.position = position ? position.copy() : new Rumi.ESATVector();
    this.offset = new Rumi.ESATVector();
    this.fill = new Rumi.Fill();
    this.stroke = new Rumi.Stroke();
    this.anchor = new Rumi.ESATVector();
    this._lastAnchor = this.anchor.copy();
    this._angleRad = 0;
    this._angleDeg = 0;
    this._lastAngleRad = this._angleRad;

    this.fill.on('*', function() {
        this.trigger('FillChange');
    }.bind(this));

    this.stroke.on('*', function() {
        this.trigger('StrokeChange');
    }.bind(this));

    this.offset.on('XChange YChange', function() {
        this.setOffset(this.offset);
        this.trigger('OffsetChange');
    }.bind(this));

    this.pos.on('XChange YChange', function() {
        this.trigger('PositionChange');
    }.bind(this));

    this.anchor.on('XChange YChange', function() {
        this.trigger('AnchorChange');
        this._rotateAroundAnchor();
    }.bind(this));

    this.setPoints(points || []);
};

Rumi.ESATPolygon.prototype = {
    draw: function(context) {
        var points = this.calcPoints;
        if(points.length === 0) return;
        context.beginPath();
        context.moveTo(this.position.x + points[0].x, this.position.y + points[0].y);
        for(var i = 0; i < points.length; i++) {
            context.lineTo(this.position.x + points[i].x, this.position.y + points[i].y);
        }
        context.closePath();
        this.fill.apply(context);
        this.stroke.apply(context);
        return this;
    },
    _copyArrayOfVectors: function(array) {
        var copy = [];
        for(var i = 0, len = array.length; i < len; ++i) {
            copy[i] = array[i].copy();
        }
        return copy;
    },
    /*For backward compatibility with SAT*/
    setPoints: function(newPoints) {
        SAT.Polygon.prototype.setPoints.call(this, this._copyArrayOfVectors(newPoints));
        this.trigger('PointsChange');
        return this;
    },
    _rotateAroundAnchor: function() {
        this.translate(-this.anchor.x, -this.anchor.y);
        this.rotate(-this._lastAngleRad);
        this.rotate(this._angleRad);
        this._lastAngleRad = this._angleRad;
        /*Revert last last rotation and apply the new one rotate(-angle); rotate(newAngle);*/
        this.translate(this.anchor.x, this.anchor.y);
        return this;
    },
    copy: function() {
        copy = new Rumi.ESATPolygon(this.position.copy(), this._copyArrayOfVectors(this.points));
        copy.anchor.copies(this.anchor);
        copy.angle = this.angle;
        copy.stroke.copies(this.stroke);
        copy.fill.copies(this.fill);
        return copy;
    },
    copies: function(other) {
        this.position.copies(other.pos);
        this.anchor.copies(other.anchor);
        this.setPoints(this._copyArrayOfVectors(other.points));
        this.angle = other.angle;
        this.fill.copies(other.fill);
        this.stroke.copies(other.stroke);
        return this;
    },
    /*For backward compatibility with SAT*/
    get angle() {
        return this._angleRad;
    },
    /*For backward compatibility with SAT*/
    set angle(newAngle) {
        if(this._angleRad != newAngle) {
            this._angleRad = newAngle;
            this.trigger('AngleChange');
        }
    },
    get angleRad() {
        return this._angleRad;
    },
    set angleRad(newAngleRad) {
        if(this._angleRad != newAngleRad) {
            this._angleRad = newAngleRad;
            this._angleDeg = Rumi.math.toDeg(newAngleRad);
            this._rotateAroundAnchor();
            this.trigger('AngleChange');
        }
    },
    get angleDeg() {
        return Rumi.math.toDeg(this._angleRad);
    },
    set angleDeg(newAngleDeg) {
        if(this.angleDeg != newAngleDeg) {
            this._angleDeg = newAngleDeg;
            this._angleRad = Rumi.math.toRad(newAngleDeg);
            this._rotateAroundAnchor();
            this.trigger('AngleChange');
        }
    },
    /*For backward compatibility with SAT*/
    get pos() {
        return this.position;
    }
};

Rumi.object.extend(Rumi.ESATPolygon.prototype, SAT.Polygon.prototype);
Rumi.object.extend(Rumi.ESATPolygon.prototype, Rumi.SociableInterface);
