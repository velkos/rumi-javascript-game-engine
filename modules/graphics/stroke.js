Rumi.Stroke = function(thickness, color, blendMode) {
	/*Required by SociableObject interface*/
	this.initSociable([
		'ThicknessChange',
		'ColorChange',
		'BlendModeChange'
	].join(' '));
	this._thickness = thickness || 1;
	this._blendMode = blendMode || Rumi.BlendModes.SOURCE_OVER;
	this.color = color ? color.copy() : new Rumi.RGBA();

	this.color.on('*', function() {
		this.trigger('ColorChange');
	}.bind(this));
};

Rumi.Stroke.prototype = {
	get blendMode() {
		return this._blendMode;

	},
	set blendMode(newBlendMode) {
		if(this._blendMode != newBlendMode) {
			this._blendMode = newBlendMode;
			this.trigger('BlendModeChange');
		}
	},
	get thickness() {
		return this._thickness;
	},
	set thickness(newThickness) {
		if(this._thickness != newThickness) {
			this._thickness = newThickness;
			this.trigger('ThicknessChange');
		}
	},
	apply: function(context) {
		if(this.thickness <= 0) return;
		context.save();
			this.applyRaw(context);
			context.stroke();
		context.restore();
		return this;
	},
	/*
	*Polutes context but allows operations with the newly introduced changes to the context to be made !
 	*Note: This method doesn't call context.stroke automatically in order to be more flexible !
 	*/
	applyRaw: function(context) {
		context.strokeStyle = this.color.toRGBA();
		context.globalCompositeOperation = this.blendMode;
		context.lineWidth = this.thickness;
		return this;
	},
	/*
	*Returns a copy of this object.
	*NOTE: All connections between the two objects are cut.
	*They are independent objects sharing same values but not references !*/
	copy: function() {
		return new Rumi.Stroke(this.thickness, this.color, this.blendMode);
	},
	/*This copies other. Both instances share same values but not references !*/
	copies: function(other) {
		this.thickness = other.thickness;
		this.blendMode = other.blendMode;
		this.color.copies(other.color);
		return this;
	}
};

Rumi.object.extend(Rumi.Stroke.prototype, Rumi.SociableInterface);
