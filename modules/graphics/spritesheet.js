/*Requires geometry.js and graphics2d.js*/
/**
 Represents ingame _image object with more complex properties than regular _image
 @class
 @constructor
 @param {Rumi.ESATBox} box - Box used to initialize the position and dimension of the sprite
 @param {HTMLImageElement} _image - Image object to create the sprite with
 @param {Rumi.ESATVector} frames - Amount of vertical and horizontal frames represented by 2d points with x and y
 */
Rumi.Spritesheet = function(box, image, frames) {
	this.initSociable([
		'AnimationEnd',
		'BoxChange',
		'AnchorChange',
		'ScaleChange',
		'ImageChange',
		'AngleChange',
		'FrameChange',
		'FramesChange',
		'BlendModeChange',
		'AlphaChannelChange'
	].join(' '));
	this.box = box ? box.copy() : new Rumi.ESATBox(new Rumi.ESATVector(), 100, 100);
	this.anchor = new Rumi.ESATVector();
	this.frames = frames ? frames.copy() : new Rumi.ESATVector(1, 1);
	this._animations = {};
	this._animationFPS = 60;
	this._currentAnimationName = null;
	this._currentAnimationIndex = 0;
	this._lastFrameChangeTime = 0;
	this._image = image ? image.cloneNode() : document.createElement('img');
	this._angleRad = 0;
	this._angleDeg = 0;
	this._frame = 0;
	this._alpha = 1;
	this._blendMode = Rumi.BlendModes.SOURCE_OVER;
	this.scale = new Rumi.ESATVector(1, 1);

	this.box.on(
		'PositionChange WidthChange HeightChange',
		this.on.bind(this, 'BoxChange')
	);

	this.frames.on(
		'XChange YChange',
		this.on.bind(this, 'FramesChange')
	);

	this.scale.on(
		'XChange YChange',
		this.on.bind(this, 'ScaleChange')
	);

	this.anchor.on(
		'XChange YChange',
		this.on.bind(this, 'AnchorChange')
	);
};

Rumi.Spritesheet.prototype = {
	get image() {
		return this._image;
	},
	set image(newImage) {
		if(this._image != newImage) {
			this._image = newImage;
			this.trigger('ImageChange');
		}
	},
	get angleRad() {
		return this._angleRad;
	},
	set angleRad(newAngleRad) {
		if(this._angleRad != newAngleRad) {
			this._angleRad = newAngleRad;
			this._angleDeg = Rumi.math.toDeg(newAngleRad);
			this.trigger('AngleChange');
		}
	},
	get angleDeg() {
		return this._angleDeg;
	},
	set angleDeg(newAngleDeg) {
		if(this._angleRad != newAngleDeg) {
			this._angleDeg = newAngleDeg;
			this._angleRad = Rumi.math.toRad(newAngleDeg);
			this.trigger('AngleChange');
		}
	},
	get alpha() {
		return this._alpha;
	},
	set alpha(newAlpha) {
		if(this._alpha != newAlpha) {
			this._alpha = newAlpha;
			this.trigger('AlphaChannelChange');
		}
	},
	get blendMode() {
		return this._blendMode;
	},
	set blendMode(newBlendMode) {
		if(this._blendMode != newBlendMode) {
			this._blendMode = newBlendMode;
			this.trigger('BlendModeChange');
		}
	},
	get frame() {
		return this._frame;
	},
	set frame(newFrame) {
		if(this._frame != newFrame) {
			this._frame = newFrame;
			this.trigger('FrameChange');
		}
	},
	addAnimation: function(name, arrayOfIndexes) {
		if(this._animations[name] === undefined) {
			this._animations[name] = arrayOfIndexes;
		}
	},
	removeAnimation: function(name) {
		if(this._animations[name] !== undefined) {
			delete this._animations[name];
			if(this._currentAnimationName === name) {
				this._currentAnimationName = null;
			}
		}
	},
	playAnimation: function(name, fps) {
		if(this._animations[name] !== undefined) {
			this._currentAnimationName = name;
			if(this._currentAnimationName !== name) this._currentAnimationIndex = 0;
			this._animationFPS = fps;
		}
	},
	stopAnimation: function() {
		this._currentAnimationName = null;
	},
	restartAnimation: function() {
		if(this._currentAnimationName === null) return;
		this._currentAnimationIndex = 0;
		this._frame = this._animations[this._currentAnimationName][this._currentAnimationIndex];
	},
	updateAnimation: function() {
		if(this._currentAnimationName === null) return;
		if((new Date() - this._lastFrameChangeTime) > (1000 / this._animationFPS)) {
			if(this._currentAnimationIndex < this._animations[this._currentAnimationName].length - 1) {
				this._currentAnimationIndex++;
			}
			else {
				this._currentAnimationIndex = 0;
				this.trigger('AnimationEnd');
			}
			this._frame = this._animations[this._currentAnimationName][this._currentAnimationIndex];
			this._lastFrameChangeTime = new Date();
		}
	},
	/**
	 Draws the sprite with all of it's modifiers (angle, alpha, blend) to a given screen.
	 This method leaves the graphics object inner state intact !
	 @param {Rumi.Graphics2D} graphics - Graphics object on which to draw the sprite
	 @returns {Rumi.Sprite} This for chaining
	 */
	draw: function(context) {
		this.updateAnimation();
		var frameWidth = this._image.naturalWidth / this.frames.x,
			frameHeight = this._image.naturalHeight / this.frames.y,
			frameX = parseInt((this._frame) % this.frames.x) * frameWidth,
			frameY = parseInt((this._frame) / this.frames.x) * frameHeight;
		context.save();
		context.translate(this.box.position.x + this.anchor.x, this.box.position.y + this.anchor.y);
		context.rotate(this._angleRad);
		context.translate(-(this.box.position.x + this.anchor.x), -(this.box.position.y + this.anchor.y));
		context.scale(this.scale.x, this.scale.y);
		context.globalAlpha = this.alpha;
		context.globalCompositeOperation = this.blendMode;
		context.drawImage(
			this._image,
			Math.round(frameX),
			Math.round(frameY),
			Math.round(frameWidth),
			Math.round(frameHeight),
			Math.round((this.box.position.x / this.scale.x)),
			Math.round(this.box.position.y / this.scale.y),
			Math.round(this.box.width * Math.sign(this.scale.x)),
			Math.round(this.box.height * Math.sign(this.scale.y))
		);
		context.restore();
		return this;
	},
	boundingBox: function() {
		var collisionBox = new Rumi.ESATBox();
		if(this._angleRad === 0) {
			return collisionBox;
		}
		else {
			var poly = this.box.toPolygon();
			poly.anchor.copies(this.anchor);
			poly.angleDeg = this._angleDeg;
			return poly;
		}
	},
	colliding: function(other) {
		return this.boundingBox().colliding(other);
	}
};

Rumi.object.extend(Rumi.Spritesheet.prototype, Rumi.SociableInterface);
