/*ESAT stays for ExtendedSAT (i.e. Extended SAT Box => ESATBox*/
Rumi.ESATVector = function(x, y) {
    this.initSociable([
        'XChange',
        'YChange',
        'FillChange',
        'StrokeChange'
    ].join(' '));
    this.fill = new Rumi.Fill();
    this.stroke = new Rumi.Stroke();

    this._x = x || 0;
    this._y = y || 0;

    this.fill.on('*', function() {
        this.trigger('FillChange');
    }.bind(this));

    this.stroke.on('*', function() {
        this.trigger('StrokeChange');
    }.bind(this));
};

Rumi.ESATVector.prototype = {
    get x() {
        return this._x;
    },
    set x(newX) {
        if(this._x != newX) {
            this._x = newX;
            this.trigger('XChange');
        }
    },
    get y() {
        return this._y;
    },
    set y(newY) {
        if(this._y != newY) {
            this._y = newY;
            this.trigger('YChange');
        }
    },
    draw: function(context) {
        context.beginPath();
        context.rect(this.x, this.y, 1, 1);
        context.closePath();
        this.fill.apply(context);
        this.stroke.apply(context);
        return this;
    },
    copy: function() {
        var copy = new Rumi.ESATVector(this.x, this.y);
        copy.stroke.copies(this.stroke);
        copy.fill.copies(this.fill);
        return copy;
    },
    copies: function(other) {
        this.x = other.x;
        this.y = other.y;
        this.fill.copies(other.fill);
        this.stroke.copies(other.fill);
        return this;
    }
};

Rumi.object.unique_extend(Rumi.ESATVector.prototype, SAT.Vector.prototype);
Rumi.object.extend(Rumi.ESATVector.prototype, Rumi.SociableInterface);
