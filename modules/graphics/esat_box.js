/*ESAT stays for ExtendedSAT (i.e. Extended SAT Box => ESATBox*/
Rumi.ESATBox = function(position, width, height) {
    this.fill = new Rumi.Fill();
    this.stroke = new Rumi.Stroke();
    this.initSociable([
        'PositionChange',
        'WidthChange',
        'HeightChange',
        'FillChange',
        'StrokeChange'
    ].join(' '));

    this.position = position ? position.copy() : new Rumi.ESATVector();
    this._width = width || 0;
    this._height = height || 0;

    this.fill.on('*', function() {
        this.trigger('FillChange');
    }.bind(this));

    this.stroke.on('*', function() {
        this.trigger('StrokeChange');
    }.bind(this));

    this.pos.on('XChange YChange', function() {
        this.trigger('PositionChange');
    }.bind(this));
};

Rumi.ESATBox.prototype = {
    draw: function(context) {
        context.beginPath();
        context.rect(this.position.x, this.position.y, this.width, this.height);
        context.closePath();
        this.fill.apply(context);
        this.stroke.apply(context);
        return this;
    },
    copy: function() {
        var copy = new Rumi.ESATBox(this.position, this.width, this.height);
        copy.stroke.copies(this.stroke);
        copy.fill.copies(this.fill);
        return copy;
    },
    copies: function(other) {
        this.position.copies(other.position);
        this.width = other.width;
        this.height = other.height;
        this.fill.copies(other.fill);
        this.stroke.copies(other.stroke);
        return this;
    },
    /*Makes a polygon out of the current box and copies the additional properties of the box into it*/
    toPolygon: function() {
        var _poly = new Rumi.ESATPolygon(
            this.position,
            [
                new Rumi.ESATVector(),
                new Rumi.ESATVector(this.width, 0),
                new Rumi.ESATVector(this.width, this.height),
                new Rumi.ESATVector(0, this.height)
            ]
        );
        _poly.fill.copies(this.fill);
        _poly.stroke.copies(this.stroke);
        return _poly;
    },
    sameSizeAs: function(other) {
        return this.width === other.width && this.height === other.height;
    },
    above: function(other) {
        return this.position.y + this.height < other.position.y;
    },
    left: function(other) {
        return this.position.x + this.width < other.position.x;
    },
    below: function(other) {
        return this.position.y > other.position.y + other.height;
    },
    right: function(other) {
        return this.position.x > other.position.x + other.width;
    },
    neighbour: function(other) {
        return (
            this.above(other) ||
            this.left(other) ||
            this.below(other) ||
            this.right(other)
        );
    },
    get width() {
        return this._width;
    },
    set width(newWidth) {
        if(this._width != newWidth) {
            this._width = newWidth;
            this.trigger('WidthChange');
        }
    },
    get height() {
        return this._height;
    },
    set height(newHeight) {
        if(this._height != newHeight) {
            this._height = newHeight;
            this.trigger('HeightChange');
        }
    },
    /*For backward compatibility with SAT.js*/
    get pos() {
        return this.position;
    },
    /*For backward compatibility with SAT.js*/
    get w() {
        return this._width;
    },
    /*For backward compatibility with SAT.js*/
    set w(newWidth) {
        if(this._width != newWidth) {
            this._width = newWidth;
            this.trigger('WidthChange');
        }
    },
    /*For backward compatibility with SAT.js*/
    get h() {
        return this._height;
    },
    /*For backward compatibility with SAT.js*/
    set h(newHeight) {
        if(this._height != newHeight) {
            this._height = newHeight;
            this.trigger('HeightChange');
        }
    }
};

Rumi.object.unique_extend(Rumi.ESATBox.prototype, SAT.Box.prototype);
Rumi.object.extend(Rumi.ESATBox.prototype, Rumi.SociableInterface);
