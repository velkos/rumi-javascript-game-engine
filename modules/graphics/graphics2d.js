/**
 * Creates a drawable screen
 * @param {?Rumi.ESATBox} box - Box used to initialize the screen
 * @constructor
 */
Rumi.Graphics2D = function(box) {
	this.initSociable([
		'PositionChange',
		'WidthChange',
		'HeightChange'
	].join(' '));

	this.box = box ? box.copy() : new Rumi.ESATBox(new Rumi.ESATVector(), 800, 600);

	this._context = document.createElement('canvas').getContext('2d');
	this._context.canvas.style.position = 'absolute';

	this.box.on('PositionChange', function() {
		this._updateCanvasPosition();
		this.trigger('PositionChange');
	}.bind(this));

	this.box.on('WidthChange', function() {
		this._updateCanvasSize();
		this.trigger('WidthChange');
	}.bind(this));

	this.box.on('HeightChange', function() {
		this._updateCanvasSize();
		this.trigger('HeightChange');
	}.bind(this));

	/*Initialize canvas position and size*/
	this._updateCanvasPosition();
	this._updateCanvasSize();
};

Rumi.Graphics2D.prototype = {
	_savedContent: null,
	get context() {
		return this._context;
	},
	get canvas() {
		return this._context.canvas;
	},
	_updateCanvasPosition: function() {
		this._context.canvas.style.marginLeft = this.box.position.x + 'px';
		this._context.canvas.style.marginTop = this.box.position.y + 'px';
		return this;
	},
	_saveCanvasContent: function() {
		if(this.box.width > 0 && this.box.height > 0) {
			this._savedContent = this._context.getImageData(0, 0, this.box.width, this.box.height);
		}
		return this;
	},
	_restoreCanvasContent: function() {
		if(this._savedContent) {
			this._context.putImageData(this._savedContent, 0, 0);
			this._savedContent = null;
		}
		return this;
	},
	_updateCanvasSize: function() {
		this._saveCanvasContent();
		this._context.canvas.width = this.box.width;
		this._context.canvas.height = this.box.height;
		this._context.canvas.style.width = this.box.width + 'px';
		this._context.canvas.style.height = this.box.height + 'px';
		this._restoreCanvasContent();
		return this;
	},
	clear: function() {
		this._context.clearRect(0, 0, this.box.width, this.box.height);
		return this;
	}
};

Rumi.object.extend(Rumi.Graphics2D.prototype, Rumi.SociableInterface);
