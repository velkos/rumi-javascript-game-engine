Rumi.Fill = function(color, blendMode) {
	this.initSociable([
		'ColorChange',
		'BlendModeChange'
	].join(' '));
	this._blendMode = blendMode || Rumi.BlendModes.SOURCE_OVER,
	this.color = color ? color.copy() : new Rumi.RGBA();

	this.color.on('*', function() {
		this.trigger('ColorChange');
	}.bind(this));
};

Rumi.Fill.prototype = {
	get blendMode() {
		return this._blendMode;
	},
	set blendMode(newBlendMode) {
		if(this._blendMode != newBlendMode) {
			this._blendMode = newBlendMode;
			this.trigger('BlendModeChange');
		}
	},
	apply: function(context) {
		context.save();
			this.applyRaw(context);
			context.fill();
		context.restore();
		return this;
	},
	/*
	*Polutes context but allows operations with the newly introduced changes to the context to be made !
	*Note: This method doesn't call context.fill automatically in order to be more flexible !
	*/
	applyRaw: function(context) {
		context.fillStyle = this.color.toRGBA();
		context.globalCompositeOperation = this.blendMode;
		context.globalAlpha = this.color.alpha;
		return this;
	},
	/*Returns a copy of this object.
	 * NOTE: All connections between the two objects are cut.
	 * 		They are independent objects sharing same values but not references !*/
	copy: function() {
		return new Rumi.Fill(this.color.copy(), this.blendMode);
	},
	/*This copies other. Both instances share same values but not references !*/
	copies: function(other) {
		this.color.copies(other.color);
		this.blendMode = other.blendMode;
	}
};

Rumi.object.extend(Rumi.Fill.prototype, Rumi.SociableInterface);
