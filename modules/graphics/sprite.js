/*Requires geometry.js and graphics2d.js*/
/**
	Represents ingame image object with more complex properties than regular image
	@class
	@constructor
 	@param {Rumi.ESATBox} box - Box used to initialize the position and dimension of the sprite
	@param {HTMLImageElement} image - Image object to create the sprite with
*/
Rumi.Sprite = function(box, image) {
	this.initSociable([
		'BoxChange',
		'ClipBoxChange',
		'ScaleChange',
		'ImageChange',
		'AngleChange',
		'BlendModeChange',
		'AlphaChannelChange'
	].join(' '));
	this._image = image ? image.cloneNode() : document.createElement('img');
	this.box = box ? box.copy() : new Rumi.ESATBox(new Rumi.ESATVector(), this._image.naturalWidth, this._image.naturalHeight);
	this.anchor = new Rumi.ESATVector();
	this._angleRad = 0;
	this._angleDeg = 0;
	this._alpha = 1;
	this._blendMode = Rumi.BlendModes.SOURCE_OVER;
	this.clipBox = new Rumi.ESATBox(
		new Rumi.ESATVector(),
		this._image.naturalWidth,
		this._image.naturalHeight
	);
	this.scale = new Rumi.ESATVector(1, 1);

	this.box.on(
		'PositionChange WidthChange HeightChange',
		this.trigger.bind(this, 'BoxChange')
	);

	this.clipBox.on(
		'PositionChange WidthChange HeightChange',
		this.trigger.bind(this, 'ClipBoxChange')
	);

	this.scale.on(
		'XChange YChange',
		this.trigger.bind(this, 'ScaleChange')
	);

	this.anchor.on(
		'XChange YChange',
		this.trigger.bind(this, 'AnchorChange')
	);
};

Rumi.Sprite.prototype = {
	get image() {
		return this._image;
	},
	set image(newImage) {
		if(this._image != newImage) {
			this._image = newImage;
			this.trigger('ImageChange');
		}
	},
	get angleRad() {
		return this._angleRad;
	},
	set angleRad(newAngleRad) {
		if(this._angleRad != newAngleRad) {
			this._angleRad = newAngleRad;
			this._angleDeg = Rumi.math.toDeg(newAngleRad);
			this.trigger('AngleChange');
		}
	},
	get angleDeg() {
		return this._angleRad;
	},
	set angleDeg(newAngleDeg) {
		if(this._angleRad != newAngleDeg) {
			this._angleDeg = newAngleDeg;
			this._angleRad = Rumi.math.toRad(newAngleDeg);
			this.trigger('AngleChange');
		}
	},
	get alpha() {
		return this._alpha;
	},
	set alpha(newAlpha) {
		if(this._alpha != newAlpha) {
			this._alpha = newAlpha;
			this.trigger('AlphaChannelChange');
		}
	},
	get blendMode() {
		return this._blendMode;
	},
	set blendMode(newBlendMode) {
		if(this._blendMode != newBlendMode) {
			this._blendMode = newBlendMode;
			this.trigger('BlendModeChange');
		}
	},
	/**
		Draws the sprite with all of it's modifiers (angle, alpha, blend) to a given screen.
		This method leaves the graphics object inner state intact !
		@param {Rumi.Graphics2D} graphics - Graphics object on which to draw the sprite
		@returns {Rumi.Sprite} This for chaining
	*/
	draw: function(context) {
		context.save();
		context.translate(this.box.position.x + this.anchor.x, this.box.position.y + this.anchor.y);
		context.rotate(this._angleRad);
		context.translate(-(this.box.position.x + this.anchor.x), -(this.box.position.y + this.anchor.y));
		context.scale(this.scale.x, this.scale.y);
		context.globalAlpha = this._alpha;
		context.globalCompositeOperation = this._blendMode;
		context.drawImage(
			this._image,
			Math.round(this.clipBox.position.x),
			Math.round(this.clipBox.position.y),
			Math.round(this.clipBox.width),
			Math.round(this.clipBox.height),
			Math.round((this.box.position.x / this.scale.x)),
			Math.round(this.box.position.y / this.scale.y),
			Math.round(this.box.width * Math.sign(this.scale.x)),
			Math.round(this.box.height * Math.sign(this.scale.y))
		);
		context.restore();
		return this;
	},
	boundingBox: function() {
		var collisionBox = this.box.copy();
		if(this._angleRad !== 0) {
			collisionBox = collisionBox.toPolygon();
			collisionBox.anchor.copies(this.anchor);
			collisionBox.angleRad = this._angleRad;
		}
		return collisionBox;
	},
	colliding: function(other) {
		return this.boundingBox().colliding(other);
	}
};

Rumi.object.extend(Rumi.Sprite.prototype, Rumi.SociableInterface);
