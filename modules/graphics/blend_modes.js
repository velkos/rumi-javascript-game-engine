/** 
	@memberof Rumi
	@readonly
	@enum {string}
*/
Rumi.BlendModes = {
	RANDOM_EXCEPT: function(modesArray) {
		var rndMode = Rumi.BlendModes.RANDOM;
		return (!Rumi.array.hasElement(rndMode)) ? rndMode : Rumi.BlendModes.RANDOM_EXCEPT(modesArray);
	},
	/*Random BlendMode on each call to the RANDOM property of Rumi.BlendModes*/
	get RANDOM() {
		var keys = Object.keys(Rumi.BlendModes);
		return Rumi.BlendModes[keys[Rumi.math.randomInt(0, keys.length)]];
	},
	/**
		@desc This is the default setting and draws new shapes on top of the existing canvas content.
	*/
	SOURCE_OVER: 'source-over',
	/** 
		@desc The new shape is drawn only where both the new shape and the destination canvas overlap. Everything else is made transparent.
	*/
	SOURCE_IN: 'source-in',
	/**
		@desc The new shape is drawn where it doesn't overlap the existing canvas content.
	*/
	SOURCE_OUT: 'source-out',
	/** 
		@desc The new shape is only drawn where it overlaps the existing canvas content.
	*/
	SOURCE_ATOP: 'source-atop',
	/**
		@desc New shapes are drawn behind the existing canvas content.
	*/
	DESTINATION_OVER: 'destination-over',
	/**
		@desc The existing canvas content is kept where both the new shape and existing canvas content overlap. Everything else is made transparent.
	*/
	DESTINATION_IN: 'destination-in',
	/** 
		@desc The existing content is kept where it doesn't overlap the new shape.
	*/
	DESTINATION_OUT: 'destination-out',
	/** 
		@desc The existing canvas is only kept where it overlaps the new shape. The new shape is drawn behind the canvas content.
	*/
	DESTINATION_ATOP: 'destination-atop',
	/** 
		@desc Where both shapes overlap the color is determined by adding color values.
	*/
	LIGHTER: 'lighter',
	/** 
		@desc Only the existing canvas is present.
	*/
	COPY: 'copy',
	/** 
		@desc Shapes are made transparent where both overlap and drawn normal everywhere else.
	*/
	XOR: 'xor',
	/** 
		@desc The pixels are of the top layer are multiplied with the corresponding pixel of the bottom layer. A darker picture is the result.
	*/
	MULTIPLY: 'multiply',
	/** 
		@desc The pixels are inverted, multiplied, and inverted again. A lighter picture is the result (opposite of multiply)
	*/
	SCREEN: 'screen',
	/** 
		@desc A combination of multiply and screen. Dark parts on the base layer become darker, and light parts become lighter.
	*/
	OVERLAY: 'overlay',
	/** 
		@desc Retains the darkest pixels of both layers.
	*/
	DARKEN: 'darken',
	/** 
		@desc Retains the lightest pixels of both layers.
	*/
	LIGHTEN: 'lighten',
	/** 
		@desc Divides the bottom layer by the inverted top layer.
	*/
	COLOR_DODGE: 'color-dodge',
	/** 
		@desc Divides the inverted bottom layer by the top layer, and then inverts the result.
	*/
	COLOR_BURN: 'color-burn',
	/** 
		@desc A combination of multiply and screen like overlay, but with top and bottom layer swapped.
	*/
	HARD_LIGHT: 'hard-light',
	/** 
		@desc A softer version of hard-light. Pure black or white does not result in pure black or white.
	*/
	SOFT_LIGHT: 'soft-light',
	/** 
		@desc Subtracts the bottom layer from the top layer or the other way round to always get a positive value.
	*/
	DIFFERENCE: 'difference',
	/** 
		@desc Like difference, but with lower contrast.
	*/
	EXCLUSION: 'exclusion',
	/** 
		@desc Preserves the luma and chroma of the bottom layer, while adopting the hue of the top layer.
	*/
	HUE: 'hue',
	/** 
		@desc Preserves the luma and hue of the bottom layer, while adopting the chroma of the top layer.
	*/
	SATURATION: 'saturation',
	/** 
		@desc Preserves the luma of the bottom layer, while adopting the hue and chroma of the top layer.
	*/
	COLOR: 'color',
	/** 
		@desc Preserves the hue and chroma of the bottom layer, while adopting the luma of the top layer.
	*/
	LUMINOSITY: 'luminosity'
};