Rumi.ESATCollision = {
	vectorVsVector: function(vectorA, vectorB) {
		return (vectorA.x === vectorB.x && vectorA.y === vectorB.y);
	},
	vectorVsBox: function(vector, box) {
		return (
			!(vector.x < box.pos.x || vector.x > box.pos.x + box.width) &&
			!(vector.y < box.pos.y || vector.y > box.pos.y + box.height)
		);
	},
	vectorVsCircle: function(vector, circle) {
		return (
			!(vector.x < circle.pos.x - circle.radius || vector.x > circle.pos.x + circle.radius) &&
			!(vector.y < circle.pos.y - circle.radius || vector.y > circle.pos.y + circle.radius)
		);
	},
	vectorVsPolygon: function(vector, polygon) {
		return SAT.pointInPolygon(vector, polygon);
	},
	circleVsCircle: function(circleA, circleB) {
		return (
			!(
				circleA.pos.x - circleA.radius < circleB.pos.x - circleB.radius ||
				circleA.pos.x + circleA.radius > circleB.pos.x + circleB.radius
			)
			&&
			!(
				circleA.pos.y - circleA.radius < circleB.pos.y - circleB.radius ||
				circleA.pos.y + circleA.radius > circleB.pos.y + circleB.radius
			)
		);
	},
	circleVsBox: function(circle, box) {
		return (
			!(
				box.pos.x + box.width < circle.pos.x - circle.radius ||
				box.pos.x > circle.pos.x + circle.radius
			)
			&&
			!(
				box.pos.y + box.height < circle.pos.y - circle.radius ||
				box.pos.y > circle.pos.y + circle.radius
			)
		);
	},
	circleVsPolygon: function(circle, polygon) {
		return SAT.testPolygonCircle(polygon, circle);
	},
	boxVsBox: function(boxA, boxB) {
		return (
			!(boxA.pos.x < boxB.pos.x || boxA.pos.x > boxB.pos.x + boxB.width) &&
			!(boxA.pos.y < boxB.pos.y || boxA.pos.y > boxB.pos.y + boxB.height)
		);
	},
	boxVsPolygon: function(box, poly) {
		return SAT.testPolygonPolygon(box.toPolygon(), poly);
	},
	polygonVsPolygon: function(polygonA, polygonB) {
		return SAT.testPolygonPolygon(polygonA, polygonB);
	},
	getShapeType: function(shape) {
		if(shape instanceof Rumi.ESATVector) {
            return 'vector';
        }
        else if(shape instanceof Rumi.ESATBox) {
            return 'box';
        }
        else if(shape instanceof Rumi.ESATCircle) {
            return 'circle';
        }
        else if(shape instanceof Rumi.ESATPolygon) {
            return 'polygon';
        }
	}
};

/*These methods are defined after the object else during their creation the object is undefined !*/
Rumi.ESATCollision.circleVsVector = function(circle, vector) {
	return Rumi.ESATCollision.vectorVsCircle(vector, circle);
};

Rumi.ESATCollision.boxVsVector = function(box, vector) {
	return Rumi.ESATCollision.vectorVsBox(vector, box);
};

Rumi.ESATCollision.boxVsCircle = function(box, circle) {
	return Rumi.ESATCollision.circleVsBox(circle, box);
};

Rumi.ESATCollision.polygonVsVector = function(polygon, vector) {
	return Rumi.ESATCollision.vectorVsPolygon(vector, polygon);
};

Rumi.ESATCollision.polygonVsCircle = function(polygon, circle) {
	return Rumi.ESATCollision.circleVsPolygon(circle, polygon);
};

Rumi.ESATCollision.polygonVsBox = function(polygon, box) {
	return Rumi.ESATCollision.boxVsPolygon(box, polygon);
};

Rumi.ESATCollision._SHAPE_COLLISION_METHOD_LUT = {
	vector: {
		vector: Rumi.ESATCollision.vectorVsVector,
		circle: Rumi.ESATCollision.vectorVsCircle,
		box: Rumi.ESATCollision.vectorVsBox,
		polygon: Rumi.ESATCollision.vectorVsPolygon
	},
	circle: {
		vector: Rumi.ESATCollision.circleVsVector,
		circle: Rumi.ESATCollision.circleVsCircle,
		box: Rumi.ESATCollision.circleVsBox,
		polygon: Rumi.ESATCollision.circleVsPolygon
	},
	box: {
		vector: Rumi.ESATCollision.boxVsVector,
		circle: Rumi.ESATCollision.boxVsCircle,
		box: Rumi.ESATCollision.boxVsBox,
		polygon: Rumi.ESATCollision.boxVsPolygon
	},
	polygon: {
		vector: Rumi.ESATCollision.polygonVsVector,
		circle: Rumi.ESATCollision.polygonVsCircle,
		box: Rumi.ESATCollision.polygonVsBox,
		polygon: Rumi.ESATCollision.polygonVsBox
	}
};

Rumi.ESATCollision.anyVsAny = function(shapeA, shapeB) {
	var typeA = Rumi.ESATCollision.getShapeType(shapeA),
		typeB = Rumi.ESATCollision.getShapeType(shapeB);
	if(typeA && typeB) {
		return Rumi.ESATCollision._SHAPE_COLLISION_METHOD_LUT[typeA][typeB](shapeA, shapeB);
	}
	return false;
};

Rumi.ESATPolygon.prototype.colliding = function(other) {
	return Rumi.ESATCollision.anyVsAny(this, other);
};

Rumi.ESATVector.prototype.colliding = Rumi.ESATBox.prototype.colliding =
Rumi.ESATCircle.prototype.colliding = Rumi.ESATPolygon.prototype.colliding;
