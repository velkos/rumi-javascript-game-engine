Rumi.Text = function(position, font, string) {
	console.log(this);
	this.initSociable([
		'BoxChange',
		'StringChange',
		'SizeChange',
		'FontChange',
		'FillChange',
		'StrokeChange',
		'AngleChange'
	].join(' '));
	this.box = new Rumi.ESATBox((position ? position.copy() : new Rumi.ESATVector()), 0, 0);
	this._string = string;
	this.font = font ? font.copy() : new Rumi.Font();
	this._context = document.createElement('canvas').getContext('2d');
	this._angleRad = 0;
	this._angleDeg = 0;
	this.fill = new Rumi.Fill();
	this.stroke = new Rumi.Stroke();
	this.fill.on('*', this.trigger.bind(this, 'FillChange'));
	this.stroke.on('*', this.trigger.bind(this, 'StrokeChange'));
	this.box.on('PositionChange onWidthChange onHeightChange', this.trigger.bind(this, 'BoxChange'));
	this.font.on('*', this.trigger.bind(this, 'FontChange'));

	this._recalculateBoundingBox();
};

Rumi.Text.prototype = {
	get angleRad() {
		return this._angleRad;
	},
	set angleRad(newAngleRad) {
		if(this._angleRad != newAngleRad) {
			this._angleRad = newAngleRad;
			this._angleDeg = Rumi.math.toDeg(newAngleRad);
		}
	},
	get angleDeg() {
		return this._angleDeg;
	},
	set angleDeg(newAngleDeg) {
		if(this._angleDeg != newAngleDeg) {
			this._angleDeg = newAngleDeg;
			this._angleRad = Rumi.math.toRad(newAngleDeg);
		}
	},
	get string() {
		return this._string;
	},
	set string(newString) {
		if(this._string != newString) {
			this._string = newString;
			this.trigger('StringChange');
		}
	},
	_recalculateBoundingBox: function() {
		this._context.font = this.font.computed;
		this.box.width = this._context.measureText(this._string).width;
		this.box.height = this._context.measureText('M').width * 1.5;
	},
	draw: function(context) {
		context.save();
			context.translate(this.box.position.x, this.box.position.y);
			context.rotate(this._angleRad);
			context.translate(-this.box.position.x, -this.box.position.y);
			context.font = this.font.computed;
			context.textBaseline = 'top';
			this.fill.applyRaw(context);
			context.fillText(this._string, this.box.position.x, this.box.position.y);
			this.stroke.applyRaw(context);
			context.strokeText(this._string, this.box.position.x, this.box.position.y);
		context.restore();
		return this;
	},
	get boundingBox() {
		var collisionBox = this.box.copy();
		if(this._angleRad !== 0) {
			collisionBox = collisionBox.toPolygon();
			collisionBox.angleRad = this._angleRad;
		}
		return collisionBox;
	},
	colliding: function(other) {
		return this.boundingBox.colliding(other);
	}
};

Rumi.object.extend(Rumi.Text.prototype, Rumi.SociableInterface);
