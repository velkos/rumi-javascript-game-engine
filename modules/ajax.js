/**
 * Created by vili on 3/14/2016.
 */
Rumi.ajax = {
    load: function(url, success, fail, done) {
        if(XMLHttpRequest !== undefined) {
            var request = new XMLHttpRequest();
            request.onerror = fail;
            request.onloadend = done;
            request.onreadystatechange = function() {
                if(request.readyState === 4 && request.status === 200) {
                    success(request.response);
                }
            };
            request.open('GET', url, true);
            request.send('');
        }
    }
};