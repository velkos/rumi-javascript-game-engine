/**
 * Created by vili on 3/27/2016.
 */
Rumi.QuadTree = function(box) {
    this.box = box;
    this.entities = [];
    this.topLeft = null;
    this.bottomLeft = null;
    this.bottomRight = null;
    this.topRight = null;
};

Rumi.QuadTree.prototype.insert = function(entity) {
    if(!this.box.colliding(entity)) return false;
    if(this.entities.length < this.NODE_CAPACITY) {
        this.entities.push(entity);
        return true;
    }
    if(this.topLeft === null) this.subdivide();

    if(
        this.topLeft.insert(entity) ||
        this.bottomLeft.insert(entity) ||
        this.bottomRight.insert(entity) ||
        this.topRight.insert(entity)) {
        return true;
    }
    return false;
};

Rumi.QuadTree.prototype.subdivide = function() {
    var halfWidth = this.box.w / 2,
        halfHeight = this.box.h / 2;
    this.topLeft = new Rumi.QuadTree(
        new Rumi.ESATBox(
            new Rumi.ESATVector(
                this.box.pos.x,
                this.box.pos.y
            ),
            halfWidth,
            halfHeight
        )
    );
    this.bottomLeft = new Rumi.QuadTree(
        new Rumi.ESATBox(
            new Rumi.ESATVector(
                this.box.pos.x,
                this.box.pos.y + halfHeight
            ),
            halfWidth,
            halfHeight
        )
    );
    this.bottomRight = new Rumi.QuadTree(
        new Rumi.ESATBox(
            new Rumi.ESATVector(
                this.box.pos.x + halfWidth,
                this.box.pos.y + halfHeight
            ),
            halfWidth,
            halfHeight
        )
    );
    this.topRight = new Rumi.QuadTree(
        new Rumi.ESATBox(
            new Rumi.ESATVector(
                this.box.pos.x + halfWidth,
                this.box.pos.y
            ),
            halfWidth,
            halfHeight
        )
    );
};

Rumi.QuadTree.prototype.queryBox = function(box) {
    var result = [];
    if(!this.box.colliding(box)) {
        return result;
    }
    for(var i = 0, len = this.entities.length; i < len; ++i) {
        if(box.colliding(this.entities[i])) {
            result.push(this.entities[i]);
        }
    }

    if(this.topLeft === null) return result;
    result = result.concat(this.topLeft.queryBox(box));
    result = result.concat(this.bottomLeft.queryBox(box));
    result = result.concat(this.bottomRight.queryBox(box));
    result = result.concat(this.topRight.queryBox(box));
    return result;
};

Rumi.QuadTree.prototype.NODE_CAPACITY = 4;