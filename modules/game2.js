/**
 * Created by vili on 3/23/2016.
 */
Rumi.Game = function(box, onPreload, onCreate, onUpdate) {
    this.eventsManager = new Rumi.EventsManager([
        'onPreload',
        'onCreate',
        'onUpdate',
        'onRender'
    ]);

    if(onPreload) this.eventsManager.attachEventListener('onPreload', onPreload.bind(this));
    if(onCreate) this.eventsManager.attachEventListener('onCreate', onCreate.bind(this));
    if(onUpdate) this.eventsManager.attachEventListener('onUpdate', onUpdate.bind(this));

    this.graphics = new Rumi.Graphics2D(box);

    this.loadingManager = new Rumi.LoadingManager();

    this._timerManager = new Rumi.TimerManager();

    this._tweenManager = new Rumi.TweenManager();

    var _that = this;

    this.input = {
        hand: new Rumi.Hand(_that.graphics.context.canvas),
        mouse: new Rumi.Mouse(_that.graphics.context.canvas),
        keyboard: new Rumi.Keyboard()
    };

    this._fpsm = new Rumi.FPSMeter();

    this.fps = 0;

    this.add = {
        vector: function(x, y) {
            var v = _that.create.vector(x, y);
            _that._renderables.push(v);
            return v;
        },
        circle: function(pos, radius) {
            var c = _that.create.circle(pos, radius);
            _that._renderables.push(c);
            return c;
        },
        box: function(pos, width, height) {
            var box = _that.create.box(pos, width, height);
            _that._renderables.push(box);
            return box;
        },
        polygon: function(pos, points) {
            var poly = _that.create.polygon(pos, points);
            _that._renderables.push(poly);
            return poly;
        },
        sprite: function(box, image) {
            var sprite = _that.create.sprite(box, image);
            _that._renderables.push(sprite);
            return sprite;
        },
        spritesheet: function(box, image, frames) {
            var sheet = _that.create.spritesheet(box, image, frames);
            _that._renderables.push(sheet);
            return sheet;
        },
        text: function(pos, string) {
            var text = _that.create.text(pos, string);
            _that._renderables.push(text);
            return text;
        },
        timer: function(interval) {
            var t = _that.create.timer(interval);
            _that._timerManager.add(t);
            return t;
        },
        tween: function(object, propValues, duration, onlyIntegers) {
            var t = _that.create.tween(object, propValues, duration, onlyIntegers);
            _that._tweenManager.add(t);
            return t;
        }
    };

    this.create = {
        vector: function(x, y) {
            return new Rumi.ESATVector(x, y);
        },
        circle: function(pos, radius) {
            return new Rumi.ESATCircle(pos, radius);
        },
        box: function(pos, width, height) {
            return new Rumi.ESATBox(pos, width, height);
        },
        polygon: function(pos, points) {
            return new Rumi.ESATPolygon(pos, points);
        },
        sprite: function(box, image) {
            return new Rumi.Sprite(box, image);
        },
        spritesheet: function(box, image, frames) {
            return new Rumi.Spritesheet(box, image, frames);
        },
        text: function(pos, string) {
            return new Rumi.Text(pos, string);
        },
        timer: function(interval) {
            return new Rumi.Timer(interval);
        },
        tween: function(object, propValues, duration, onlyIntegers) {
            return new Rumi.Tween(object, propValues, duration, onlyIntegers);
        }
    };

    this.attach = function(obj) {
        if(this._isRenderable(obj)) {
            Rumi.array.addIfNotPresent(this._renderables, obj);
        }
        else if(obj instanceof Rumi.Timer) {
            this._timerManager.add(obj);
        }
        else if(obj instanceof Rumi.Tween) {
            this._tweenManager.add(obj);
        }
    };

    this.detach = function(obj) {
        if(this._isRenderable(obj)) {
            Rumi.array.removeByVal(this._renderables, obj);
        }
        else if(obj instanceof Rumi.Timer) {
            this._timerManager.delete(obj);
        }
        else if(obj instanceof Rumi.Tween) {
            this._tweenManager.delete(obj);
        }
    };
    this._renderables = [];

    this._initialSequenceTimeoutID = setTimeout(function() {
        this.eventsManager.triggerEvent('onPreload');
        this.loadingManager.load(function(assets) {
            if(assets !== undefined) {
                this.assets = assets; 
            }
            this.eventsManager.triggerEvent('onCreate');
            this.eventsManager.triggerEvent('onUpdate');
            delete this._initialSequenceTimeoutID;
            this.update();
        }.bind(this));
    }.bind(this), 1);
};

Rumi.Game.prototype._isRenderable = function(obj) {
    for(var i = 0, len = this._renderableTypes.length; i < len; i++) {
        if(obj instanceof this._renderableTypes[i]) return true;
    }
    return false;
};

Rumi.Game.prototype._renderableTypes = [
    Rumi.ESATVector,
    Rumi.ESATCircle,
    Rumi.ESATBox,
    Rumi.ESATPolygon,
    Rumi.Sprite,
    Rumi.Spritesheet,
    Rumi.Text
];

Rumi.Game.prototype.update = function() {
    this.fps = this._fpsm.getFPS();
    this.eventsManager.triggerEvent('onUpdate');
    this.graphics.clear();
    this._renderables.forEach(function(e) {
        e.draw(this.graphics.context);
    }, this);
    this._timerManager.update();
    this._tweenManager.update();
    this.eventsManager.triggerEvent('onRender');
    this.nextAnimationFrameID = requestAnimationFrame(this.update.bind(this));
};

Rumi.Game.prototype.nextAnimationFrameID = null;

Rumi.Game.prototype.destroy = function() {
    if(this._initialSequenceTimeoutID) {
        clearTimeout(this._initialSequenceTimeoutID);
    }
    cancelAnimationFrame(this.nextAnimationFrameID);
    this.graphics.destroy();
    this.input.mouse.destroy();
    this.input.hand.destroy();
    for(var p in this) {
        delete this[p];
    }
};