/** Measures frames per second (FPS)
	@class
	@constructor
*/
Rumi.FPSMeter = function() {
	var startTime = 0,
		frameNumber = 0;
	this.getFPS = function() {
		frameNumber++;
		var d = new Date().getTime(),
			currentTime = ( d - startTime ) / 1000,
			result = Math.floor( ( frameNumber / currentTime ) );

		if( currentTime > 1 ){
			startTime = new Date().getTime();
			frameNumber = 0;
		}
		return result;
	};
};