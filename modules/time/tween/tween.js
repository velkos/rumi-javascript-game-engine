/**
 * Created by vili on 3/29/2016.
 */
Rumi.Tween = function(object, propValues, duration, onlyIntegers) {
    this.eventsManager = new Rumi.EventsManager([
        'onUpdate',
        'onEnd'
    ].join(' '));
    this.onlyIntegers = onlyIntegers;
    this.object = object;
    this.propValues = propValues;
    this.duration = duration;
    for(var p in propValues) {
        /*If start is ommited it defaults to the current value of the object property*/
        if(this.propValues[p].start === undefined) this.propValues[p].start = this.object[p];
        this.propValues[p].delta = (this.propValues[p].end - this.propValues[p].start) / this.duration;
    }
    this.started = new Date();
    this.ended = false;
};

Rumi.Tween.prototype.update = function() {
    if(new Date() - this.started > this.duration) {
        if(!this.ended) {
            this.ended = true;
            /*Due to time inaccuracy force the values to their ends*/
            for(var p in this.propValues) {
                this.object[p] = this.propValues[p].end;
            }
            this.eventsManager.triggerEvent('onEnd');
        }
        return;
    }
    for(var p in this.propValues) {
        var value = this.propValues[p].start + (this.propValues[p].delta * (new Date() - this.started));
        this.object[p] = this.onlyIntegers ? parseInt(value) : value;
    }
    this.eventsManager.triggerEvent('onUpdate');
};
