/**
 * Created by vili on 3/30/2016.
 */
Rumi.TweenManager = function() {
    this._tweens = [];
};

Rumi.TweenManager.prototype.add = function(tween) {
    Rumi.array.addIfNotPresent(this._tweens, tween);
};

Rumi.TweenManager.prototype.delete = function(tween) {
    Rumi.array.removeByVal(this._tweens, tween);
};

Rumi.TweenManager.prototype.update = function() {
    for(var i = this._tweens.length - 1; i > -1; --i) {
        if(this._tweens[i].ended) {
            this._tweens.splice(i, 1);
        }
        else {
            this._tweens[i].update();
        }
    }
};