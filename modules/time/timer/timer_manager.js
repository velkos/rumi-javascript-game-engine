/**
 * Created by vili on 3/30/2016.
 */
Rumi.TimerManager = function() {
    this._timers = [];
};

Rumi.TimerManager.prototype.add = function(timer) {
    Rumi.array.addIfNotPresent(this._timers, timer);
};

Rumi.TimerManager.prototype.delete = function(timer) {
    Rumi.array.removeByVal(this._timers, timer);
};

Rumi.TimerManager.prototype.update = function() {
    for(var i = 0, len = this._timers.length; i < len; ++i) {
        this._timers[i].update();
    }
};