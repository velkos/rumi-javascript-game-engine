/**
 * Created by vili on 3/23/2016.
 */
Rumi.Timer = function(interval) {
    this.eventsManager = new Rumi.EventsManager([
        'onStart',
        'onTick',
        'onStop'
    ].join(' '));
    this._interval = interval;
    this._running = false;
    this._startedAt = 0;
};

Rumi.Timer.prototype.start = function() {
    if(!this._running) {
        this._startedAt = new Date();
        this._running = true;
        this.eventsManager.triggerEvent('onStart');
    }
};

Rumi.Timer.prototype.stop = function() {
    if(this._running) {
        this._startedAt = 0;
        this._running = false;
        this.eventsManager.triggerEvent('onStop');
    }
};

Rumi.Timer.prototype.running = function() {
    return this._running;
};

Rumi.Timer.prototype.update = function() {
    if(this._running && (new Date() - this._startedAt) >= this._interval) {
        this._startedAt = new Date();
        this.eventsManager.triggerEvent('onTick');
    }
};
