/*Requires resource_manager.js, graphics2d.js, keyboardv2.js, mouse.js, sprite.js, spritesheet.js*/
/** Creates the game object
	@class
	@constructor
	@param {number} xAxis - Where the top left corner of the viewport resides on the x axis
	@param {number} yAxis - Where the top left corner of the viewport resides on the y axis
	@param {number} graphicsWidth - Width of the viewport in pixels
	@param {number} graphicsheight - Height of the viewport in pixels
	@param {function} atpreload - Function to call to start the preload phase (load resources)
	@param {function} atcreate - Function in which the user should initialize everything and setup for work
	@param {function} atupdate - Function to execute on every game update (at the start of each itteration of the game loop)
	@param {function} atrender - Function to execute after each render (at the end of each iteration of the game loop) 
*/
Rumi.Game = function(xAxis, yAxis, graphicsWidth, graphicsHeight, atpreload, atcreate, atupdate, atrender) {
	this.graphicalObjects = [];
	this.animations = [];
	this.onpreload = atpreload !== undefined ? atpreload.bind(this) : function(){}.bind(this);
	this.oncreate = atcreate !== undefined ? atcreate.bind(this) : function(){}.bind(this);
	this.onupdate = atupdate !== undefined ? atupdate.bind(this) : function(){}.bind(this);
	this.onrender = atrender !== undefined ? atrender.bind(this) : function(){}.bind(this);
	this.RM = new Rumi.ResourceManager();
	this.G = new Rumi.Graphics2D(xAxis, yAxis, graphicsWidth, graphicsHeight);
	this.K = new Rumi.Keyboard();
	this.M = new Rumi.Mouse(this.G.visibleContext.canvas);
	this.FPSM = new Rumi.FPSMeter();
	this.AP = new Rumi.AudioPlayer();

	/*All prototype methods should have for their this the current game instance*/
	for(var p in this.add) {
		this.add[p] = this.add[p].bind(this);
	}

	for(var p in this.make) {
		this.make[p] = this.make[p].bind(this);
	}

	/*This is needed because requestAnimationFrame this is the global context and after the first call to the loop 
	which is within the constructor. The 'this' for theLoop() is Window and not the current game instance.
	To prevent memory leak by binding the function call to the loop from requestAnimationFrame. This bind is here ! 
	*/
	this.theLoop = this.theLoop.bind(this);

	setTimeout(function() {
		/*Fire the preloading routine*/
		this.onpreload();
		/*Once loading is done (even if no files were requested this callback gets called!)
		Call the this.oncreate callback and start this.theLoop*/
		/*In order to RETURN THE GAME OBJECT we need to set the whole process of 
		loading start,
		loading end, 
		creation of the game,
		and starting of the game loop in a timeout
		so there time for this constructor function
		to return 
		*/
		this.RM.load(function() {
			this.oncreate();
			this.theLoop();
		}.bind(this));	
	}.bind(this), 1);
};

Rumi.Game.prototype.fps = 0;

Rumi.Game.prototype.theLoop = function() {
	this.fps = this.FPSM.getFPS();
	this.onupdate();
	var animationsLen = this.animations.length;
	for(var i = animationsLen - 1; i > - 1; i--) {
		this.animations[i].update();
	}
	var graphicalObjectsLen = this.graphicalObjects.length;
	for(var i = 0; i < graphicalObjectsLen; i++) {
		var obj = this.graphicalObjects[i];
		obj.x += obj.velocity.x;
		obj.y += obj.velocity.y;
		obj.wrappingDrawRoutine(this.G.bufferContext);
	}
	this.G.render();
	this.onrender()
	requestAnimationFrame(this.theLoop);
};

/*
	Destroy objects/resources
*/
Rumi.Game.prototype.destroyObject = function(obj) {
	for(var property in obj) {
		delete obj[property];
	}
};

Rumi.Game.prototype.isAnimation = function(obj) {
	return (obj instanceof Rumi.Animation);
};

Rumi.Game.prototype.hasAnyObjectWithSuchID = function(obj) {
	var arrayToSearch = null;
	if(this.isAnimation(obj)) {
		arrayToSearch = this.animations;
	}
	else {
		arrayToSearch = this.graphicalObjects;
	}
	var arrayToSearchLen = arrayToSearch.length;
	for(var i = 0; i < arrayToSearchLen; i++) {
		if(arrayToSearch[i].uid == obj.uid) {
			return true;
		}
	}
	return false;
};

Rumi.Game.prototype.textSize = function(text) {
	var ctx = this.G.bufferContext,
		w = h = 0,
		fontString = text.fontsize + 'px ' + text.fontfamily;
	ctx.save();
		ctx.font = fontString;
		w = ctx.measureText(text.string).width;
		h = ctx.measureText('M').width;
	ctx.restore();
	return {
		width: w,
		height: h
	};
};

Rumi.Game.prototype.colliding = function(objA, objB) {
	return Rumi.Geometry.collidingSATShapes(objA.SATObject(), objB.SATObject());
};

Rumi.Game.prototype.destroy = function(obj) {
	this.detach(obj);
	this.destroyObject(obj);
};

/*Make objects without binding them to the game instance*/
Rumi.Game.prototype.make = {
	sprite: function(imageName, x, y, width, height) {
		return new Rumi.Sprite(this.RM.getImageByName(imageName), x, y, width, height);
	},
	spritesheet: function(imageName, horizontal, vertical, x, y, width, height) {
		return new Rumi.Spritesheet(this.RM.getImageByName(imageName), horizontal, vertical, x, y, width, height);
	},
	point: function(x, y) {
		return new Rumi.Geometry.Point(x, y);
	},
	circle: function(x, y, r) {
		return new Rumi.Geometry.Circle(x, y, r);
	},
	box: function(x, y, w, h) {
		return new Rumi.Geometry.Box(x, y, w, h);
	},
	polygon: function(x, y, points) {
		return new Rumi.Geometry.Polygon(x, y, points);
	},
	text: function(text, x, y) {
		return new Rumi.Text(text, x, y);
	},
	animation: function(duration, stepFunc) {
		return new Rumi.Animation(duration, stepFunc);
	}
};

/*
	Add objects to the scene
*/
Rumi.Game.prototype.add = {
	/** 
		Add sprite to the scene
		@param {string} name - Name of the image. This is the name this was used when the image was loaded
		@param {number} x - Where to position the sprite on the viewport along the x axis (in pixels)
		@param {number} y - Where to position the sprite on the viewport along the y axis (in pixels)
		@param {number} w - Desired display width of the sprite on the viewport (in pixels)
		@param {number} h - Desired display height of the sprite on the viewport (in pixels)
		@returns {Rumi.Sprite}
	*/
	sprite: function(imageName, x, y, w, h) {
		var s = this.make.sprite(imageName, x, y, w, h);
		this.graphicalObjects.push(s);
		return s;
	},

	/** 
		Add spritesheet to the scene
		@inner
		@apram {string} name - Name of the image. This is the name this was used when the image was loaded
		@param {number} horizontal - Number of frames along the x axis
		@param {number} vertical - Number of frames along the y axis
		@param {number} x - Where to position the spritesheet on the viewport along the x axis (in pixels)
		@param {number} y - Where to position the spritesheet on the viewport along the y axis (in pixels)
		@apram {number} w - Desired display width of the spritesheet on the viewport (in pixels)
		@param {number} h - Desired display height of the spritesheet on the viewport (in pixels)
		@returns {Rumi.Spritesheet}
	*/
	spritesheet: function(imageName, horizontal, vertical, x, y, width, height) {
		var s = this.make.spritesheet(imageName, horizontal, vertical, x, y, width, height);
		this.graphicalObjects.push(s);
		return s;
	},
	point: function(x, y) {
		var v = this.make.point(x, y);
		this.graphicalObjects.push(v);
		return v;
	},
	circle: function(x, y, r) {
		var c = this.make.circle(x, y, r);
		this.graphicalObjects.push(c);
		return c;
	},
	box: function(x, y, w, h) {
		var b = this.make.box(x, y, w, h);
		this.graphicalObjects.push(b);
		return b;
	},
	polygon: function(x, y, points) {
		var p = this.make.polygon(x, y, points);
		this.graphicalObjects.push(p);
		return p;
	},
	animation: function(duration, stepFunc) {
		var a = this.make.animation(duration, stepFunc);
		this.animations.push(a);
		return a;
	},
	text: function(text, x, y) {
		var t = new this.make.text(text, x, y);
		this.graphicalObjects.push(t);
		return t;
	}
};

Rumi.Game.prototype.attach = function(obj) {
	if(!this.hasAnyObjectWithSuchID(obj)) {
		if(this.isAnimation(obj)) {
			this.animations.push(obj);
		}
		else {
			this.graphicalObjects.push(obj);
		}
	}
	return this;
};

Rumi.Game.prototype.detach = function(obj) {
	if(this.hasAnyObjectWithSuchID(obj)) {
		var arrayFromWhichToRemove = null;
		if(this.isAnimation(obj)) {
			arrayFromWhichToRemove = this.animations;
		}
		else {
			arrayFromWhichToRemove = this.graphicalObjects;
		}
		var arrayFromWhichToRemoveLen = arrayFromWhichToRemove.length;
		for(var i = arrayFromWhichToRemoveLen - 1; i > -1; i--) {
			if(arrayFromWhichToRemove[i].uid === obj.uid) {
				arrayFromWhichToRemove.splice(i, 1);
				break;
			}
		}
	}
	return this;
};

Rumi.Game.prototype.browserViewportSize = function () {
	var e = window,
		a = 'inner';
	if(!('innerWidth' in window)) {
		a = 'client';
		e = document.documentElement || document.body;
	}
	return {
		width : e[ a+'Width' ],
		height : e[ a+'Height' ]
	};
};