/*Requiring the object implementing this interface to first implement SociableInterface*/
Rumi.StateChangeTrackingInterface = {
	initStateChangeTracking: function(options) {
		this._stateChangeTrackingVariables = {};
		this._parseInitializationObject(options);
	},
	_prepareGetter: function(variableName, initial, optionsObject) {
		/*Analyzes the options object and registers the events needed for the variable value to be properly tracked*/
		var _notifyBefore = optionsObject.each === true ? true : false,
			_internalVariableName = ['_', variableName].join(''),
			_beforeEventName = _notifyBefore ? ['Before', variableName, 'Retrieve'] : undefined,
			_that = this;
		this._stateChangeTrackingVariables[_internalVariableName] = initial;
		if(_beforeEventName) {
			this.register(_beforeEventName);
		}
		/*End of the SociableInterface configuration part so we can return the getter itself after we have all the info*/
		return function() {
			if(_notifyBefore) {
				_that.trigger(_beforeEventName);
			}
			return _that._stateChangeTrackingVariables[_internalVariableName];
		};
	},
	_prepareSetter: function(variableName, optionsObject) {
		var _before = !!optionsObject.before,
			_beforeEach = _before ? !!optionsObject.before.each : false,
			_beforeEachEventName = _beforeEach ? ['BeforeAny', variableName, 'Assignment'].join('') : undefined,
			_beforeChange = _before ? !!optionsObject.before.change : false,
			_beforeChangeEventName = _beforeChange ? ['BeforeSuccessful', variableName, 'Assignment'].join('') : undefined,
			_beforeSame = _before ? !!optionsObject.before.same : false,
			_beforeSameEventName = _beforeSame ? ['BeforeUnsuccessful', variableName, 'Assignment'].join('') : undefined,
			_after = !!optionsObject.after,
			_afterEach = _after ? !!optionsObject.after.each : false,
			_afterEachEventName = _afterEach ? ['AfterAny', variableName, 'Assignment'].join('') : undefined,
			_afterChange = _after ? !!optionsObject.after.change : false,
			_afterChangeEventName = _afterChange ? ['AfterSuccessful', variableName, 'Assignment'].join('') : undefined,
			_afterSame = _after ? !!optionsObject.after.same : false,
			_afterSameEventName = _afterSame ? ['AfterUnsuccessful', variableName, 'Assignment'].join('') : undefined,
			_internalVariableName = ['_', variableName].join(''),
			_that = this;

		if(_beforeEachEventName) this.register(_beforeEachEventName);
		if(_beforeChangeEventName) this.register(_beforeChangeEventName);
		if(_beforeSameEventName) this.register(_beforeSameEventName);
		if(_afterEachEventName) this.register(_afterEachEventName);
		if(_afterChangeEventName) this.register(_afterChangeEventName);
		if(_afterSameEventName) this.register(_afterSameEventName);

		if((_before && (_beforeEach || _beforeChange || _beforeSame)) ||
			(_after && (_afterEach || _afterChange || _afterSame))) {
				return function(newVal) {
						if(_beforeEach) {
							_that.trigger(_beforeEachEventName);
						}
						var _changing = _that._stateChangeTrackingVariables[_internalVariableName] != newVal;
						if(_changing) {
							if(_beforeChange) {
								_that.trigger(_beforeChangeEventName)
							}
							_that._stateChangeTrackingVariables[_internalVariableName] = newVal;
						}
						else if(_beforeSame) {
							_that.trigger(_beforeSameEventName);
						}
						if(_afterEach) {
							_that.trigger(_afterEachEventName);
						}
						if(_changing && _afterChange) {
							_that.trigger(_afterChangeEventName);
						}
						if(!_changing && _afterSame) {
							_that.trigger(_afterSameEventName);
						}
					}
		}
	},
	_parseInitializationObject: function(obj) {
		for(var variable in obj) {
			var getter = this._prepareGetter(variable, obj[variable].initial, obj[variable].get),
				setter = this._prepareSetter(variable, obj[variable].set);
			Object.defineProperty(this, variable, {
				get: getter,
				set: setter
			});
		}
	}
	/*
	{
		variable: {
			intial: (*optional, default: undefined) 'initial value of the variable here',
			get: { (*optional, but if provided a getter for the variable will be created)
				(*optional) before: {
					each: true/false (*optional) - notify before each try to get the value of the variable,
				},
			},
			set: { (*optional, but if a getter was provided and a setter object is provided a setter will automatically be created for the variable)
				(*optional) before: {
					each: true/false (*optional) - notify before each try to change the value of the variable either successful or not,
					change: true/false (*optional) - notify before each try that will successful modify the variable value (different value than the current),
					same: true/false (*optional) - notify before each try to modify the value that will have no effect (same value as current)
				},
				(*optional) after: {
					each: true/false (*optional) - notify after each try to change the value of the variable either successful or not,
					change: true/false (*optional) - notify after each try that successfully modified the variable value (to a different value than the previous),
					same: true/false (*optional) - notify after each try to modify the value that had no effect (same value as previous)
				}
			}
		}
	}
	*/
};
