/*
	The interface should be implemented by objects
	which can take advantage of rotation (angle value change tracking).
	The main purpose of this interface is to remove redundant code
	because most of the objects support rotation.
	*	"implementing the interface" refers to the act of
		an object extending itself with the properties
		in the interface object and/or calling additional
		methods in it's constructor or explicitly after implementation
		in order to personally configure the interface
		so there are no shared objects amongst instances implementing the
		same interface !

	This interface can be properly implemented only by objects
	that previously implemented the SociableInterface.
	It relies that the object which implements it
	has introduced AngleChange event which will be
	triggered when the angleRad/angleDeg properties
	inherited from this interface change values.
	If angleDeg's (in degrees) value changes then
	angleRad will be synced with it's value but
	represented in radians and vice versa.
	Each object which implements this interface should
	call it's newly introduced method initRotatable
	in it's constructor or explicitly after implementing the interface !.
*/
Rumi.RotatableInterface = {
	initRotatable: function() {
		this._angleChangeCallback = function() {
			this.trigger('AngleChange');
		}.bind(this);
		var _that = this;
		Object.defineProperties(this, {
			angleDeg: {
				get: function() {
					return _that._angleDeg;
				},
				set: function(newAngleDeg) {
					if(_that._angleDeg != newAngleDeg) {
						_that._angleDeg = newAngleDeg;
						_that._syncRadWithDeg();
						_that._angleChangeCallback();
					}
				}
			},
			angleRad: {
				get: function() {
					return _that._angleRad;
				},
				set: function(newAngleRad) {
					if(_that._angleRad != newAngleRad) {
						_that._angleRad = newAngleRad;
						_that._syncDegWithRad();
						_that._angleChangeCallback();
					}
				}
			}
		});
	},
	_angleRad: 0,
	_angleDeg: 0,
	_angleChangeCallback: null,
	_syncDegWithRad: function() {
		this._angleDeg = Rumi.math.toDeg(this._angleRad);
		return this;
	},
	_syncRadWithDeg: function() {
		this._angleRad = Rumi.math.toRad(this._angleDeg);
		return this;
	}
};
