/*
*Sociable object is abstract type for an object which is able to communicate with other objects
therefore having instance of Rumi.EventsManager into it and it wraps it in intuitive way
*/
Rumi.SociableInterface = {
	initSociable: function(eventNames) {
		this._eventsManager = new Rumi.EventsManager(eventNames);
	},
	on: function(eventNames, listenerFunction) {
		this._eventsManager.attachEventListener(eventNames, listenerFunction);
		return this;
	},
	off: function(eventNames, listenerFunction) {
		this._eventsManager.detachEventListener(eventNames, listenerFunction);
		return this;
	},
	mute: function(eventNames) {
		this._eventsManager.suppressEvent(eventNames);
		return this;
	},
	unmute: function(eventNames) {
		this._eventsManager.unsuppresssEvent(eventNames);
		return this;
	},
	register: function(eventNames) {
		this._eventsManager.registerEvent(eventNames);
		return this;
	},
	unregister: function(eventNames) {
		this._eventsManager.unregisterEvent(eventNames);
		return this;
	},
	trigger: function(eventNames) {
		this._eventsManager.triggerEvent(eventNames);
		return this;
	}
};
