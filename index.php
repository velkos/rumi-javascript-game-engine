<!DOCTYPE html>
<html>
	<head>
		<link type="text/css" rel="stylesheet" href="index.css">
		<title></title>
	</head>
	<body>
	<?php
		$output_file_name = 'rumi-full.js';
		$moduleNames = array(
			"./modules/core/rumi.js",
			"./modules/core/event.js",
			"./modules/core/events_manager.js",
			"./modules/core/SAT_editted_vector_copy_conflict.js",
			"./modules/core/math.js",
			"./modules/core/array.js",
			"./modules/core/object.js",
			"./modules/interfaces/sociable_interface.js",
			"./modules/interfaces/rotatable_interface.js",
			"./modules/interfaces/state_change_tracking_interface.js",
			"./modules/graphics/rgba.js",
			"./modules/graphics/stroke.js",
			"./modules/graphics/fill.js",
			"./modules/graphics/blend_modes.js",
			"./modules/graphics/esat_vector.js",
			"./modules/graphics/esat_circle.js",
			"./modules/graphics/esat_box.js",
			"./modules/graphics/esat_polygon.js",
			"./modules/graphics/esat_collision.js",
			"./modules/graphics/graphics2d.js",
            "./modules/loading/load.js",
            "./modules/loading/loading_manager.js",
            "./modules/ajax.js",
            "./modules/graphics/sprite.js",
            "./modules/graphics/spritesheet.js",
            "./modules/graphics/text.js",
            "./modules/input/keyboard.js",
            "./modules/input/mouse.js",
            "./modules/input/hand.js",
            "./modules/time/timer/timer.js",
            "./modules/time/timer/timer_manager.js",
            "./modules/time/fps_meter.js",
            "./modules/quad_tree.js",
            "./modules/time/tween/tween.js",
            "./modules/time/tween/tween_manager.js",
			"./modules/game2.js"
		);
		$outputfile = fopen( $output_file_name, "w" );

		for($i = 0; $i < count( $moduleNames ); $i++) {
			$content = file_get_contents( $moduleNames[$i] );
			fwrite( $outputfile,  $content );
		}

		fclose( $outputfile );
	?>
	<div id="test"></div>
	<script src="rumi-full.js"></script>
	<script src='test.js'></script>
	</body>
</html>
