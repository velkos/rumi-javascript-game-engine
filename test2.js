/**
 * Created by vili on 3/27/2016.
 */
var game,
    tree,
    Array
    rndBox = function(maxW, maxH) {
        return new Rumi.ESATBox(
            new Rumi.ESATVector(
                Rumi.math.randomInt(0, maxW),
                Rumi.math.randomInt(0, maxH)
            ),
            50,
            50
        );
    },
    classicApproach = function() {
        var start = new Date();
        for(var i = 0, len = game._renderables.length; i < len; ++i) {
            if(game.input.mouse.pagePos.colliding(game._renderables[i])) {
                game._renderables[i].fill.color.randomize();
            }
        }
        console.log('classic: ' + (new Date() - start));
    },
    qTreeApproach = function() {
        var start = new Date();
        var box = new Rumi.ESATBox(
            game.input.mouse.pagePos,
            1,
            1
        );
        var elements = tree.queryBox(box);
        for(var i = 0, len = elements.length; i < len; ++i) {
            if(game.input.mouse.pagePos.colliding(elements[i])) {
                elements[i].fill.color.randomize();
            }
        }
        console.log('qtree: ' + (new Date() - start))
    };
window.onload = function() {
    game = new Rumi.Game();
    tree = new Rumi.QuadTree(game.graphics.box);
    for(var i = 0; i < 50; i++) {
        var box = rndBox(game.graphics.box.w, game.graphics.box.h);
        game.attach.renderable(box);
        tree.insert(box);
    }

    game.input.mouse.eventsManager.attachEventListener('onLeftClick', function() {
        classicApproach();
        qTreeApproach();
    });
};