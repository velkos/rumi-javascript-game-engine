<!DOCTYPE html>
<html>
	<head>
		<title></title>
	</head>
	<body>
	<?php
		$output_file_name = 'rumi-full.js';
		$moduleNames = array(
			"./modules/rumi.js",
			"./modules/SAT.js",
			"./modules/blend_modes.js",
			"./modules/graphics2d.js",
			"./modules/graphical_object.js",
			"./modules/geometry.js",
			"./modules/resource_manager.js",
			"./modules/audio_player.js",
			"./modules/sprite.js",
			"./modules/spritesheet.js",
			'./modules/text.js',
			"./modules/keyboard.js",
			"./modules/mouse.js",
			"./modules/fps_meter.js",
			"./modules/animation.js",
			"./modules/game.js"
		);
		$outputfile = fopen( $output_file_name, "w" );
		
		for($i = 0; $i < count( $moduleNames ); $i++) {
			$content = file_get_contents( $moduleNames[$i] );
			fwrite( $outputfile,  $content );
		}
		
		fclose( $outputfile );	
	?>
	</body>
</html>