/*var g = new Rumi.Graphics2D();
document.body.appendChild(g.canvas);
var lm = new Rumi.LoadingManager();
lm.images({
	'test': 'img/bgr.png'
});
var sprite = null;

lm.load(function(files) {
	sprite = new Rumi.Sprite(new Rumi.ESATBox(null, 250, 250), files.test);
	setInterval(function() {
		g.clear();
		sprite.angleRad += 0.01;
		sprite.draw(g.context);
		var box = sprite.boundingBox();
		box.fill.color.CurrentPlayerHealth = 0.2;
		box.draw(g.context);
	}, 1000/60);
});*/
/*
Test = function() {
	this.initSociable('AngleChange');
	this.initRotatable();
	this.initStateChangeTracking({
		Health: {
			initial: 0,
			get: {
				before: {
					each: true
				},
			},
			set: {
				before: {
					each: true,
					same: true,
					change: true
				},
				after: {
					each: true,
					change: true,
					same: true
				}
			}
		},
		Mana: {
			initial: 'string',
			get: {
				before: {
					each: true
				}
			},
			set: {
				before: {
					each: true,
					same: true,
					change: true
				},
				after: {
					each: true,
					change: true,
					same: true
				}
			}
		}
	});
};

Rumi.object.extend(Test.prototype, Rumi.SociableInterface);
Rumi.object.extend(Test.prototype, Rumi.RotatableInterface);
Rumi.object.extend(Test.prototype, Rumi.StateChangeTrackingInterface);

var g = new Rumi.Graphics2D(),
	r = new Rumi.ESATBox(null, 50, 50),
	m = new Rumi.Mouse(g.canvas),
	dragging = false,
	dragPoint = new Rumi.ESATVector(),
	tween = null;

r.fill.color.fromColorName('purple');
r.fill.color.alpha = 0.7;
r.stroke.color.fromColorName('red');
r.stroke.color.alpha = 0.5;

m.eventsManager.attachEventListener('onLeftDown', function() {
	if(m.pagePos.colliding(r)) {
		dragPoint.x = m.pagePos.x - r.position.x;
		dragPoint.y = m.pagePos.y - r.position.y;
		dragging = true;
		tween = new Rumi.Tween(
			r.stroke,
			{
				thickness: {
					start: 0,
					end: 15
				}
			},
			1500
		);
	}
});

m.eventsManager.attachEventListener('onLeftUp', function() {
	if(dragging) dragging = false;
});

m.eventsManager.attachEventListener('onMove', function() {
	if(dragging) {
		r.position.copies(m.pagePos);
		r.position.x = m.pagePos.x - dragPoint.x;
		r.position.y = m.pagePos.y - dragPoint.y;
	}
});

document.body.appendChild(g.canvas);

setInterval(function() {
	g.clear();
	if(tween) tween.update();
	r.draw(g.context);
}, 1000 / 60)
*/

var objects = [],
	graphics = new Rumi.Graphics2D(),
	mouse = new Rumi.Mouse(graphics.canvas),
	start = null;

mouse.eventsManager.attachEventListener('onMove', function() {
	//start = new Date().getTime();
	objects.forEach(function(obj) {
		if(mouse.pagePos.colliding(obj)) {
			obj.fill.color.alpha = 0.2
		}
		else {
			obj.fill.color.alpha = 1;
		}
	});
	//console.log('operation took', new Date().getTime() - start);
});

for(var i = 0; i < 50; ++i) {
	objects.push(
		new Rumi.ESATCircle(
			new Rumi.ESATVector(
				Rumi.math.randomInt(
					0,
					graphics.box.width
				),
				Rumi.math.randomInt(
					0,
					graphics.box.height
				)
			),
			Rumi.math.randomInt(5, 15)
		)
	);
}

document.body.appendChild(graphics.canvas);

setInterval(function() {
	graphics.clear();
	objects.forEach(function(obj) {
		obj.draw(graphics.context);
	});
}, 1000 / 60);
