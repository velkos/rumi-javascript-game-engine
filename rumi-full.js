/** 
	@global
	@namespace
*/
var Rumi = {};

/**
	@memberof Rumi
	@desc Generates unique id
	@returns {string}
**/
Rumi.uid = function() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(i) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (i=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
};/**
	@param {string} name - Name of the event
*/
Rumi.Event = function(name) {
	this._name = name;
	this._suppressed = false;
	this._handlers = [];
};

Rumi.Event.prototype = {
	/*
	*Returns the name of the event
	*/
	get name() {
		return this._name;
	},
	/**
	*Returns true if the event is suppressed else - false.
	*Info: If the event is suppressed, triggering it will have no effect and won't call it's handlers!
	*/
	get suppressed() {
		return this._suppressed;
	},
	/**
	*Sets the suppressed property of the event to either true or false
	*Info: If the suppressed property of this event object is set to true, triggering the event will have no effect and won't call it's handlers!
	*/
	set suppressed(newSuppressedState) {
		this._suppressed = newSuppressedState;
	},
	/**
	*Attaches a listener function to the event. Listeners get called when the event's trigger() method is invoked.
	*Info: If the listener function is already attached to this event object this method will have no effect. 
	*/
	attachListener: function(listenerFunction) {
		Rumi.array.addIfNotPresent(this._handlers, listenerFunction);
		return this;
	},
	/**
	*Detaches a listener function from the event. The next time the event gets triggered this function will not be called.
	Info: If the listener function is not attached to the this event object this method will have no effect.
	*/
	detachListener: function(listenerFunction) {
		Rumi.array.removeByVal(this._handlers, listenerFunction);
		return this;
	},
	/*
	*Triggers this event object - effectively calling each listener function in the order they were attached to the object.
	*Info: If the suppressed property of this event object is true this method will have no effect.
	*/
	trigger: function(argPassedToListeners) {
		if(this.suppressed) return;
		for(var i = 0, len = this._handlers.length; i < len; i++) {
			this._handlers[i].call(null, argPassedToListeners);
		}
		return this;
	}
};/**
	@param {Array} eventNames - Array containing event names as strings
*/
Rumi.EventsManager = function(eventNames) {
	this._events = {};
	this.registerEvent(eventNames);
};

Rumi.EventsManager.prototype = {
	/**
	 * @param {String} string - String containing single or multiple event names or only * character which means all events.
	 * When the string is * then all event names from the current instance are returned in array.
	 * In the other two cases is returned Array with single or multiple event names which are not guaranteed to be registered
	 * in the current instance!
	 * @returns {Array}
	 */
	extractEventNamesFromString: function(string) {
		if(string === '*') {
			var allEventNames = [];
			for(var eventName in this._events) {
				allEventNames.push(eventName);
			}
			return allEventNames;
		}
		else if(string != '' && string.split(' ').length > 1) {
			return string.split(' ');
		}
		else {
			return [string];
		}
	},
	triggerEvent: function(eventName, argPassedToListeners) {
		var result = this.extractEventNamesFromString(eventName);
		for(var i = 0, len = result.length; i < len; ++i) {
			if(this._events[result[i]] !== undefined) {
				this._events[result[i]].trigger(argPassedToListeners);
			}
		}
		return this;
	},
	suppressEvent: function(eventName) {
		var result = this.extractEventNamesFromString(eventName);
		for(var i = 0, len = result.length; i < len; ++i) {
			if(this._events[result[i]] !== undefined) {
				this._events[result[i]].suppressed = true;
			}
		}
	},
	unsuppressEvent: function(eventName) {
		var result = this.extractEventNamesFromString(eventName);
		for(var i = 0, len = result.length; i < len; ++i) {
			if(this._events[result[i]] !== undefined) {
				this._events[result[i]].suppressed = false;
			}
		}
	},
	registerEvent: function(eventName) {
		var result = this.extractEventNamesFromString(eventName);
		for(var i = 0, len = result.length; i < len; ++i) {
			if(this._events[result[i]] === undefined) {
				this._events[result[i]] = new Rumi.Event(result[i]);
			}
		}
		return this;
	},
	unregisterEvent: function(eventName) {
		var result = this.extractEventNamesFromString(eventName);
		for(var i = 0, len = result.length; i < len; ++i) {
			if(this._events[result[i]] !== undefined) {
				delete this._events[result[i]];
			}
		}
		return this;
	},
	attachEventListener: function(eventName, listenerFunction) {
		var result = this.extractEventNamesFromString(eventName);
		for(var i = 0, len = result.length; i < len; ++i) {
			if(this._events[result[i]] !== undefined) {
				this._events[result[i]].attachListener(listenerFunction);
			}
		}
		return this;
	},
	detachEventListener: function(eventName, listenerFunction) {
		var result = this.extractEventNamesFromString(eventName);
		for(var i = 0, len = result.length; i < len; ++i) {
			if(this._events[result[i]] !== undefined) {
				this._events[result[i]].detachListener(listenerFunction);
			}
		}
		return this;
	}
};
// Version 0.5.0 - Copyright 2012 - 2015 -  Jim Riecken <jimr@jimr.ca>
//
// Released under the MIT License - https://github.com/jriecken/sat-js
//
// A simple library for determining intersections of circles and
// polygons using the Separating Axis Theorem.
/** @preserve SAT.js - Version 0.5.0 - Copyright 2012 - 2015 - Jim Riecken <jimr@jimr.ca> - released under the MIT License. https://github.com/jriecken/sat-js */

/*global define: false, module: false*/
/*jshint shadow:true, sub:true, forin:true, noarg:true, noempty:true, 
  eqeqeq:true, bitwise:true, strict:true, undef:true, 
  curly:true, browser:true */

// Create a UMD wrapper for SAT. Works in:
//
//  - Plain browser via global SAT variable
//  - AMD loader (like require.js)
//  - Node.js
//
// The quoted properties all over the place are used so that the Closure Compiler
// does not mangle the exposed API in advanced mode.
/**
 * @param {*} root - The global scope
 * @param {Function} factory - Factory that creates SAT module
 */
(function (root, factory) {
  "use strict";
  if (typeof define === 'function' && define['amd']) {
    define(factory);
  } else if (typeof exports === 'object') {
    module['exports'] = factory();
  } else {
    root['SAT'] = factory();
  }
}(this, function () {
  "use strict";

  var SAT = {};

  //
  // ## Vector
  //
  // Represents a vector in two dimensions with `x` and `y` properties.


  // Create a new Vector, optionally passing in the `x` and `y` coordinates. If
  // a coordinate is not specified, it will be set to `0`
  /** 
   * @param {?number=} x The x position.
   * @param {?number=} y The y position.
   * @constructor
   */
  function Vector(x, y) {
    this['x'] = x || 0;
    this['y'] = y || 0;
  }
  SAT['Vector'] = Vector;
  // Alias `Vector` as `V`
  SAT['V'] = Vector;


  // Copy the values of another Vector into this one.
  /**
   * @param {Vector} other The other Vector.
   * @return {Vector} This for chaining.
   */
  Vector.prototype['copy_'] = Vector.prototype.copy_ = function(other) {
    this['x'] = other['x'];
    this['y'] = other['y'];
    return this;
  };

  // Create a new vector with the same coordinates as this on.
  /**
   * @return {Vector} The new cloned vector
   */
  Vector.prototype['clone'] = Vector.prototype.clone = function() {
    return new Vector(this['x'], this['y']);
  };

  // Change this vector to be perpendicular to what it was before. (Effectively
  // roatates it 90 degrees in a clockwise direction)
  /**
   * @return {Vector} This for chaining.
   */
  Vector.prototype['perp'] = Vector.prototype.perp = function() {
    var x = this['x'];
    this['x'] = this['y'];
    this['y'] = -x;
    return this;
  };

  // Rotate this vector (counter-clockwise) by the specified angle (in radians).
  /**
   * @param {number} angle The angle to rotate (in radians)
   * @return {Vector} This for chaining.
   */
  Vector.prototype['rotate'] = Vector.prototype.rotate = function (angle) {
    var x = this['x'];
    var y = this['y'];
    this['x'] = x * Math.cos(angle) - y * Math.sin(angle);
    this['y'] = x * Math.sin(angle) + y * Math.cos(angle);
    return this;
  };

  // Reverse this vector.
  /**
   * @return {Vector} This for chaining.
   */
  Vector.prototype['reverse'] = Vector.prototype.reverse = function() {
    this['x'] = -this['x'];
    this['y'] = -this['y'];
    return this;
  };
  

  // Normalize this vector.  (make it have length of `1`)
  /**
   * @return {Vector} This for chaining.
   */
  Vector.prototype['normalize'] = Vector.prototype.normalize = function() {
    var d = this.len();
    if(d > 0) {
      this['x'] = this['x'] / d;
      this['y'] = this['y'] / d;
    }
    return this;
  };
  
  // Add another vector to this one.
  /**
   * @param {Vector} other The other Vector.
   * @return {Vector} This for chaining.
   */
  Vector.prototype['add'] = Vector.prototype.add = function(other) {
    this['x'] += other['x'];
    this['y'] += other['y'];
    return this;
  };
  
  // Subtract another vector from this one.
  /**
   * @param {Vector} other The other Vector.
   * @return {Vector} This for chaiing.
   */
  Vector.prototype['sub'] = Vector.prototype.sub = function(other) {
    this['x'] -= other['x'];
    this['y'] -= other['y'];
    return this;
  };
  
  // Scale this vector. An independant scaling factor can be provided
  // for each axis, or a single scaling factor that will scale both `x` and `y`.
  /**
   * @param {number} x The scaling factor in the x direction.
   * @param {?number=} y The scaling factor in the y direction.  If this
   *   is not specified, the x scaling factor will be used.
   * @return {Vector} This for chaining.
   */
  Vector.prototype['scale'] = Vector.prototype.scale = function(x,y) {
    this['x'] *= x;
    this['y'] *= y || x;
    return this; 
  };
  
  // Project this vector on to another vector.
  /**
   * @param {Vector} other The vector to project onto.
   * @return {Vector} This for chaining.
   */
  Vector.prototype['project'] = Vector.prototype.project = function(other) {
    var amt = this.dot(other) / other.len2();
    this['x'] = amt * other['x'];
    this['y'] = amt * other['y'];
    return this;
  };
  
  // Project this vector onto a vector of unit length. This is slightly more efficient
  // than `project` when dealing with unit vectors.
  /**
   * @param {Vector} other The unit vector to project onto.
   * @return {Vector} This for chaining.
   */
  Vector.prototype['projectN'] = Vector.prototype.projectN = function(other) {
    var amt = this.dot(other);
    this['x'] = amt * other['x'];
    this['y'] = amt * other['y'];
    return this;
  };
  
  // Reflect this vector on an arbitrary axis.
  /**
   * @param {Vector} axis The vector representing the axis.
   * @return {Vector} This for chaining.
   */
  Vector.prototype['reflect'] = Vector.prototype.reflect = function(axis) {
    var x = this['x'];
    var y = this['y'];
    this.project(axis).scale(2);
    this['x'] -= x;
    this['y'] -= y;
    return this;
  };
  
  // Reflect this vector on an arbitrary axis (represented by a unit vector). This is
  // slightly more efficient than `reflect` when dealing with an axis that is a unit vector.
  /**
   * @param {Vector} axis The unit vector representing the axis.
   * @return {Vector} This for chaining.
   */
  Vector.prototype['reflectN'] = Vector.prototype.reflectN = function(axis) {
    var x = this['x'];
    var y = this['y'];
    this.projectN(axis).scale(2);
    this['x'] -= x;
    this['y'] -= y;
    return this;
  };
  
  // Get the dot product of this vector and another.
  /**
   * @param {Vector}  other The vector to dot this one against.
   * @return {number} The dot product.
   */
  Vector.prototype['dot'] = Vector.prototype.dot = function(other) {
    return this['x'] * other['x'] + this['y'] * other['y'];
  };
  
  // Get the squared length of this vector.
  /**
   * @return {number} The length^2 of this vector.
   */
  Vector.prototype['len2'] = Vector.prototype.len2 = function() {
    return this.dot(this);
  };
  
  // Get the length of this vector.
  /**
   * @return {number} The length of this vector.
   */
  Vector.prototype['len'] = Vector.prototype.len = function() {
    return Math.sqrt(this.len2());
  };
  
  // ## Circle
  //
  // Represents a circle with a position and a radius.

  // Create a new circle, optionally passing in a position and/or radius. If no position
  // is given, the circle will be at `(0,0)`. If no radius is provided, the circle will
  // have a radius of `0`.
  /**
   * @param {Vector=} pos A vector representing the position of the center of the circle
   * @param {?number=} r The radius of the circle
   * @constructor
   */
  function Circle(pos, r) {
    this['pos'] = pos || new Vector();
    this['r'] = r || 0;
  }
  SAT['Circle'] = Circle;
  
  // Compute the axis-aligned bounding box (AABB) of this Circle.
  //
  // Note: Returns a _new_ `Polygon` each time you call this.
  /**
   * @return {Polygon} The AABB
   */
  Circle.prototype['getAABB'] = Circle.prototype.getAABB = function() {
    var r = this['r'];
    var corner = this["pos"].clone().sub(new Vector(r, r));
    return new Box(corner, r*2, r*2).toPolygon();
  };

  // ## Polygon
  //
  // Represents a *convex* polygon with any number of points (specified in counter-clockwise order)
  //
  // Note: Do _not_ manually change the `points`, `angle`, or `offset` properties. Use the
  // provided setters. Otherwise the calculated properties will not be updated correctly.
  //
  // `pos` can be changed directly.

  // Create a new polygon, passing in a position vector, and an array of points (represented
  // by vectors relative to the position vector). If no position is passed in, the position
  // of the polygon will be `(0,0)`.
  /**
   * @param {Vector=} pos A vector representing the origin of the polygon. (all other
   *   points are relative to this one)
   * @param {Array.<Vector>=} points An array of vectors representing the points in the polygon,
   *   in counter-clockwise order.
   * @constructor
   */
  function Polygon(pos, points) {
    this['pos'] = pos || new Vector();
    this['angle'] = 0;
    this['offset'] = new Vector();
    this.setPoints(points || []);
  }
  SAT['Polygon'] = Polygon;
  
  // Set the points of the polygon.
  //
  // Note: The points are counter-clockwise *with respect to the coordinate system*.
  // If you directly draw the points on a screen that has the origin at the top-left corner
  // it will _appear_ visually that the points are being specified clockwise. This is just
  // because of the inversion of the Y-axis when being displayed.
  /**
   * @param {Array.<Vector>=} points An array of vectors representing the points in the polygon,
   *   in counter-clockwise order.
   * @return {Polygon} This for chaining.
   */
  Polygon.prototype['setPoints'] = Polygon.prototype.setPoints = function(points) {
    // Only re-allocate if this is a new polygon or the number of points has changed.
    var lengthChanged = !this['points'] || this['points'].length !== points.length;
    if (lengthChanged) {
      var i;
      var calcPoints = this['calcPoints'] = [];
      var edges = this['edges'] = [];
      var normals = this['normals'] = [];
      // Allocate the vector arrays for the calculated properties
      for (i = 0; i < points.length; i++) {
        calcPoints.push(new Vector());
        edges.push(new Vector());
        normals.push(new Vector());
      }
    }
    this['points'] = points;
    this._recalc();
    return this;
  };

  // Set the current rotation angle of the polygon.
  /**
   * @param {number} angle The current rotation angle (in radians).
   * @return {Polygon} This for chaining.
   */
  Polygon.prototype['setAngle'] = Polygon.prototype.setAngle = function(angle) {
    this['angle'] = angle;
    this._recalc();
    return this;
  };

  // Set the current offset to apply to the `points` before applying the `angle` rotation.
  /**
   * @param {Vector} offset The new offset vector.
   * @return {Polygon} This for chaining.
   */
  Polygon.prototype['setOffset'] = Polygon.prototype.setOffset = function(offset) {
    this['offset'] = offset;
    this._recalc();
    return this;
  };

  // Rotates this polygon counter-clockwise around the origin of *its local coordinate system* (i.e. `pos`).
  //
  // Note: This changes the **original** points (so any `angle` will be applied on top of this rotation).
  /**
   * @param {number} angle The angle to rotate (in radians)
   * @return {Polygon} This for chaining.
   */
  Polygon.prototype['rotate'] = Polygon.prototype.rotate = function(angle) {
    var points = this['points'];
    var len = points.length;
    for (var i = 0; i < len; i++) {
      points[i].rotate(angle);
    }
    this._recalc();
    return this;
  };

  // Translates the points of this polygon by a specified amount relative to the origin of *its own coordinate
  // system* (i.e. `pos`).
  //
  // This is most useful to change the "center point" of a polygon. If you just want to move the whole polygon, change
  // the coordinates of `pos`.
  //
  // Note: This changes the **original** points (so any `offset` will be applied on top of this translation)
  /**
   * @param {number} x The horizontal amount to translate.
   * @param {number} y The vertical amount to translate.
   * @return {Polygon} This for chaining.
   */
  Polygon.prototype['translate'] = Polygon.prototype.translate = function (x, y) {
    var points = this['points'];
    var len = points.length;
    for (var i = 0; i < len; i++) {
      points[i].x += x;
      points[i].y += y;
    }
    this._recalc();
    return this;
  };


  // Computes the calculated collision polygon. Applies the `angle` and `offset` to the original points then recalculates the
  // edges and normals of the collision polygon.
  /**
   * @return {Polygon} This for chaining.
   */
  Polygon.prototype._recalc = function() {
    // Calculated points - this is what is used for underlying collisions and takes into account
    // the angle/offset set on the polygon.
    var calcPoints = this['calcPoints'];
    // The edges here are the direction of the `n`th edge of the polygon, relative to
    // the `n`th point. If you want to draw a given edge from the edge value, you must
    // first translate to the position of the starting point.
    var edges = this['edges'];
    // The normals here are the direction of the normal for the `n`th edge of the polygon, relative
    // to the position of the `n`th point. If you want to draw an edge normal, you must first
    // translate to the position of the starting point.
    var normals = this['normals'];
    // Copy the original points array and apply the offset/angle
    var points = this['points'];
    var offset = this['offset'];
    var angle = this['angle'];
    var len = points.length;
    var i;
    for (i = 0; i < len; i++) {
      var calcPoint = calcPoints[i].copy_(points[i]);
      calcPoint.x += offset.x;
      calcPoint.y += offset.y;
      if (angle !== 0) {
        calcPoint.rotate(angle);
      }
    }
    // Calculate the edges/normals
    for (i = 0; i < len; i++) {
      var p1 = calcPoints[i];
      var p2 = i < len - 1 ? calcPoints[i + 1] : calcPoints[0];
      var e = edges[i].copy_(p2).sub(p1);
      normals[i].copy_(e).perp().normalize();
    }
    return this;
  };
  
  
  // Compute the axis-aligned bounding box. Any current state
  // (translations/rotations) will be applied before constructing the AABB.
  //
  // Note: Returns a _new_ `Polygon` each time you call this.
  /**
   * @return {Polygon} The AABB
   */
  Polygon.prototype["getAABB"] = Polygon.prototype.getAABB = function() {
    var points = this["calcPoints"];
    var len = points.length;
    var xMin = points[0]["x"];
    var yMin = points[0]["y"];
    var xMax = points[0]["x"];
    var yMax = points[0]["y"];
    for (var i = 1; i < len; i++) {
      var point = points[i];
      if (point["x"] < xMin) {
        xMin = point["x"];
      }
      else if (point["x"] > xMax) {
        xMax = point["x"];
      }
      if (point["y"] < yMin) {
        yMin = point["y"];
      }
      else if (point["y"] > yMax) {
        yMax = point["y"];
      }
    }
    return new Box(this["pos"].clone().add(new Vector(xMin, yMin)), xMax - xMin, yMax - yMin).toPolygon();
  };
  

  // ## Box
  //
  // Represents an axis-aligned box, with a width and height.


  // Create a new box, with the specified position, width, and height. If no position
  // is given, the position will be `(0,0)`. If no width or height are given, they will
  // be set to `0`.
  /**
   * @param {Vector=} pos A vector representing the bottom-left of the box (i.e. the smallest x and smallest y value).
   * @param {?number=} w The width of the box.
   * @param {?number=} h The height of the box.
   * @constructor
   */
  function Box(pos, w, h) {
    this['pos'] = pos || new Vector();
    this['w'] = w || 0;
    this['h'] = h || 0;
  }
  SAT['Box'] = Box;

  // Returns a polygon whose edges are the same as this box.
  /**
   * @return {Polygon} A new Polygon that represents this box.
   */
  Box.prototype['toPolygon'] = Box.prototype.toPolygon = function() {
    var pos = this['pos'];
    var w = this['w'];
    var h = this['h'];
    return new Polygon(new Vector(pos['x'], pos['y']), [
     new Vector(), new Vector(w, 0), 
     new Vector(w,h), new Vector(0,h)
    ]);
  };
  
  // ## Response
  //
  // An object representing the result of an intersection. Contains:
  //  - The two objects participating in the intersection
  //  - The vector representing the minimum change necessary to extract the first object
  //    from the second one (as well as a unit vector in that direction and the magnitude
  //    of the overlap)
  //  - Whether the first object is entirely inside the second, and vice versa.
  /**
   * @constructor
   */  
  function Response() {
    this['a'] = null;
    this['b'] = null;
    this['overlapN'] = new Vector();
    this['overlapV'] = new Vector();
    this.clear();
  }
  SAT['Response'] = Response;

  // Set some values of the response back to their defaults.  Call this between tests if
  // you are going to reuse a single Response object for multiple intersection tests (recommented
  // as it will avoid allcating extra memory)
  /**
   * @return {Response} This for chaining
   */
  Response.prototype['clear'] = Response.prototype.clear = function() {
    this['aInB'] = true;
    this['bInA'] = true;
    this['overlap'] = Number.MAX_VALUE;
    return this;
  };

  // ## Object Pools

  // A pool of `Vector` objects that are used in calculations to avoid
  // allocating memory.
  /**
   * @type {Array.<Vector>}
   */
  var T_VECTORS = [];
  for (var i = 0; i < 10; i++) { T_VECTORS.push(new Vector()); }
  
  // A pool of arrays of numbers used in calculations to avoid allocating
  // memory.
  /**
   * @type {Array.<Array.<number>>}
   */
  var T_ARRAYS = [];
  for (var i = 0; i < 5; i++) { T_ARRAYS.push([]); }

  // Temporary response used for polygon hit detection.
  /**
   * @type {Response}
   */
  var T_RESPONSE = new Response();

  // Unit square polygon used for polygon hit detection.
  /**
   * @type {Polygon}
   */
  var UNIT_SQUARE = new Box(new Vector(), 1, 1).toPolygon();

  // ## Helper Functions

  // Flattens the specified array of points onto a unit vector axis,
  // resulting in a one dimensional range of the minimum and
  // maximum value on that axis.
  /**
   * @param {Array.<Vector>} points The points to flatten.
   * @param {Vector} normal The unit vector axis to flatten on.
   * @param {Array.<number>} result An array.  After calling this function,
   *   result[0] will be the minimum value,
   *   result[1] will be the maximum value.
   */
  function flattenPointsOn(points, normal, result) {
    var min = Number.MAX_VALUE;
    var max = -Number.MAX_VALUE;
    var len = points.length;
    for (var i = 0; i < len; i++ ) {
      // The magnitude of the projection of the point onto the normal
      var dot = points[i].dot(normal);
      if (dot < min) { min = dot; }
      if (dot > max) { max = dot; }
    }
    result[0] = min; result[1] = max;
  }
  
  // Check whether two convex polygons are separated by the specified
  // axis (must be a unit vector).
  /**
   * @param {Vector} aPos The position of the first polygon.
   * @param {Vector} bPos The position of the second polygon.
   * @param {Array.<Vector>} aPoints The points in the first polygon.
   * @param {Array.<Vector>} bPoints The points in the second polygon.
   * @param {Vector} axis The axis (unit sized) to test against.  The points of both polygons
   *   will be projected onto this axis.
   * @param {Response=} response A Response object (optional) which will be populated
   *   if the axis is not a separating axis.
   * @return {boolean} true if it is a separating axis, false otherwise.  If false,
   *   and a response is passed in, information about how much overlap and
   *   the direction of the overlap will be populated.
   */
  function isSeparatingAxis(aPos, bPos, aPoints, bPoints, axis, response) {
    var rangeA = T_ARRAYS.pop();
    var rangeB = T_ARRAYS.pop();
    // The magnitude of the offset between the two polygons
    var offsetV = T_VECTORS.pop().copy_(bPos).sub(aPos);
    var projectedOffset = offsetV.dot(axis);
    // Project the polygons onto the axis.
    flattenPointsOn(aPoints, axis, rangeA);
    flattenPointsOn(bPoints, axis, rangeB);
    // Move B's range to its position relative to A.
    rangeB[0] += projectedOffset;
    rangeB[1] += projectedOffset;
    // Check if there is a gap. If there is, this is a separating axis and we can stop
    if (rangeA[0] > rangeB[1] || rangeB[0] > rangeA[1]) {
      T_VECTORS.push(offsetV); 
      T_ARRAYS.push(rangeA); 
      T_ARRAYS.push(rangeB);
      return true;
    }
    // This is not a separating axis. If we're calculating a response, calculate the overlap.
    if (response) {
      var overlap = 0;
      // A starts further left than B
      if (rangeA[0] < rangeB[0]) {
        response['aInB'] = false;
        // A ends before B does. We have to pull A out of B
        if (rangeA[1] < rangeB[1]) { 
          overlap = rangeA[1] - rangeB[0];
          response['bInA'] = false;
        // B is fully inside A.  Pick the shortest way out.
        } else {
          var option1 = rangeA[1] - rangeB[0];
          var option2 = rangeB[1] - rangeA[0];
          overlap = option1 < option2 ? option1 : -option2;
        }
      // B starts further left than A
      } else {
        response['bInA'] = false;
        // B ends before A ends. We have to push A out of B
        if (rangeA[1] > rangeB[1]) { 
          overlap = rangeA[0] - rangeB[1];
          response['aInB'] = false;
        // A is fully inside B.  Pick the shortest way out.
        } else {
          var option1 = rangeA[1] - rangeB[0];
          var option2 = rangeB[1] - rangeA[0];
          overlap = option1 < option2 ? option1 : -option2;
        }
      }
      // If this is the smallest amount of overlap we've seen so far, set it as the minimum overlap.
      var absOverlap = Math.abs(overlap);
      if (absOverlap < response['overlap']) {
        response['overlap'] = absOverlap;
        response['overlapN'].copy_(axis);
        if (overlap < 0) {
          response['overlapN'].reverse();
        }
      }      
    }
    T_VECTORS.push(offsetV); 
    T_ARRAYS.push(rangeA); 
    T_ARRAYS.push(rangeB);
    return false;
  }
  
  // Calculates which Voronoi region a point is on a line segment.
  // It is assumed that both the line and the point are relative to `(0,0)`
  //
  //            |       (0)      |
  //     (-1)  [S]--------------[E]  (1)
  //            |       (0)      |
  /**
   * @param {Vector} line The line segment.
   * @param {Vector} point The point.
   * @return  {number} LEFT_VORONOI_REGION (-1) if it is the left region,
   *          MIDDLE_VORONOI_REGION (0) if it is the middle region,
   *          RIGHT_VORONOI_REGION (1) if it is the right region.
   */
  function voronoiRegion(line, point) {
    var len2 = line.len2();
    var dp = point.dot(line);
    // If the point is beyond the start of the line, it is in the
    // left voronoi region.
    if (dp < 0) { return LEFT_VORONOI_REGION; }
    // If the point is beyond the end of the line, it is in the
    // right voronoi region.
    else if (dp > len2) { return RIGHT_VORONOI_REGION; }
    // Otherwise, it's in the middle one.
    else { return MIDDLE_VORONOI_REGION; }
  }
  // Constants for Voronoi regions
  /**
   * @const
   */
  var LEFT_VORONOI_REGION = -1;
  /**
   * @const
   */
  var MIDDLE_VORONOI_REGION = 0;
  /**
   * @const
   */
  var RIGHT_VORONOI_REGION = 1;
  
  // ## Collision Tests

  // Check if a point is inside a circle.
  /**
   * @param {Vector} p The point to test.
   * @param {Circle} c The circle to test.
   * @return {boolean} true if the point is inside the circle, false if it is not.
   */
  function pointInCircle(p, c) {
    var differenceV = T_VECTORS.pop().copy_(p).sub(c['pos']);
    var radiusSq = c['r'] * c['r'];
    var distanceSq = differenceV.len2();
    T_VECTORS.push(differenceV);
    // If the distance between is smaller than the radius then the point is inside the circle.
    return distanceSq <= radiusSq;
  }
  SAT['pointInCircle'] = pointInCircle;

  // Check if a point is inside a convex polygon.
  /**
   * @param {Vector} p The point to test.
   * @param {Polygon} poly The polygon to test.
   * @return {boolean} true if the point is inside the polygon, false if it is not.
   */
  function pointInPolygon(p, poly) {
    UNIT_SQUARE['pos'].copy_(p);
    T_RESPONSE.clear();
    var result = testPolygonPolygon(UNIT_SQUARE, poly, T_RESPONSE);
    if (result) {
      result = T_RESPONSE['aInB'];
    }
    return result;
  }
  SAT['pointInPolygon'] = pointInPolygon;

  // Check if two circles collide.
  /**
   * @param {Circle} a The first circle.
   * @param {Circle} b The second circle.
   * @param {Response=} response Response object (optional) that will be populated if
   *   the circles intersect.
   * @return {boolean} true if the circles intersect, false if they don't. 
   */
  function testCircleCircle(a, b, response) {
    // Check if the distance between the centers of the two
    // circles is greater than their combined radius.
    var differenceV = T_VECTORS.pop().copy_(b['pos']).sub(a['pos']);
    var totalRadius = a['r'] + b['r'];
    var totalRadiusSq = totalRadius * totalRadius;
    var distanceSq = differenceV.len2();
    // If the distance is bigger than the combined radius, they don't intersect.
    if (distanceSq > totalRadiusSq) {
      T_VECTORS.push(differenceV);
      return false;
    }
    // They intersect.  If we're calculating a response, calculate the overlap.
    if (response) { 
      var dist = Math.sqrt(distanceSq);
      response['a'] = a;
      response['b'] = b;
      response['overlap'] = totalRadius - dist;
      response['overlapN'].copy_(differenceV.normalize());
      response['overlapV'].copy_(differenceV).scale(response['overlap']);
      response['aInB']= a['r'] <= b['r'] && dist <= b['r'] - a['r'];
      response['bInA'] = b['r'] <= a['r'] && dist <= a['r'] - b['r'];
    }
    T_VECTORS.push(differenceV);
    return true;
  }
  SAT['testCircleCircle'] = testCircleCircle;
  
  // Check if a polygon and a circle collide.
  /**
   * @param {Polygon} polygon The polygon.
   * @param {Circle} circle The circle.
   * @param {Response=} response Response object (optional) that will be populated if
   *   they interset.
   * @return {boolean} true if they intersect, false if they don't.
   */
  function testPolygonCircle(polygon, circle, response) {
    // Get the position of the circle relative to the polygon.
    var circlePos = T_VECTORS.pop().copy_(circle['pos']).sub(polygon['pos']);
    var radius = circle['r'];
    var radius2 = radius * radius;
    var points = polygon['calcPoints'];
    var len = points.length;
    var edge = T_VECTORS.pop();
    var point = T_VECTORS.pop();
    
    // For each edge in the polygon:
    for (var i = 0; i < len; i++) {
      var next = i === len - 1 ? 0 : i + 1;
      var prev = i === 0 ? len - 1 : i - 1;
      var overlap = 0;
      var overlapN = null;
      
      // Get the edge.
      edge.copy_(polygon['edges'][i]);
      // Calculate the center of the circle relative to the starting point of the edge.
      point.copy_(circlePos).sub(points[i]);
      
      // If the distance between the center of the circle and the point
      // is bigger than the radius, the polygon is definitely not fully in
      // the circle.
      if (response && point.len2() > radius2) {
        response['aInB'] = false;
      }
      
      // Calculate which Voronoi region the center of the circle is in.
      var region = voronoiRegion(edge, point);
      // If it's the left region:
      if (region === LEFT_VORONOI_REGION) {
        // We need to make sure we're in the RIGHT_VORONOI_REGION of the previous edge.
        edge.copy_(polygon['edges'][prev]);
        // Calculate the center of the circle relative the starting point of the previous edge
        var point2 = T_VECTORS.pop().copy_(circlePos).sub(points[prev]);
        region = voronoiRegion(edge, point2);
        if (region === RIGHT_VORONOI_REGION) {
          // It's in the region we want.  Check if the circle intersects the point.
          var dist = point.len();
          if (dist > radius) {
            // No intersection
            T_VECTORS.push(circlePos); 
            T_VECTORS.push(edge);
            T_VECTORS.push(point); 
            T_VECTORS.push(point2);
            return false;
          } else if (response) {
            // It intersects, calculate the overlap.
            response['bInA'] = false;
            overlapN = point.normalize();
            overlap = radius - dist;
          }
        }
        T_VECTORS.push(point2);
      // If it's the right region:
      } else if (region === RIGHT_VORONOI_REGION) {
        // We need to make sure we're in the left region on the next edge
        edge.copy_(polygon['edges'][next]);
        // Calculate the center of the circle relative to the starting point of the next edge.
        point.copy_(circlePos).sub(points[next]);
        region = voronoiRegion(edge, point);
        if (region === LEFT_VORONOI_REGION) {
          // It's in the region we want.  Check if the circle intersects the point.
          var dist = point.len();
          if (dist > radius) {
            // No intersection
            T_VECTORS.push(circlePos); 
            T_VECTORS.push(edge); 
            T_VECTORS.push(point);
            return false;              
          } else if (response) {
            // It intersects, calculate the overlap.
            response['bInA'] = false;
            overlapN = point.normalize();
            overlap = radius - dist;
          }
        }
      // Otherwise, it's the middle region:
      } else {
        // Need to check if the circle is intersecting the edge,
        // Change the edge into its "edge normal".
        var normal = edge.perp().normalize();
        // Find the perpendicular distance between the center of the 
        // circle and the edge.
        var dist = point.dot(normal);
        var distAbs = Math.abs(dist);
        // If the circle is on the outside of the edge, there is no intersection.
        if (dist > 0 && distAbs > radius) {
          // No intersection
          T_VECTORS.push(circlePos); 
          T_VECTORS.push(normal); 
          T_VECTORS.push(point);
          return false;
        } else if (response) {
          // It intersects, calculate the overlap.
          overlapN = normal;
          overlap = radius - dist;
          // If the center of the circle is on the outside of the edge, or part of the
          // circle is on the outside, the circle is not fully inside the polygon.
          if (dist >= 0 || overlap < 2 * radius) {
            response['bInA'] = false;
          }
        }
      }
      
      // If this is the smallest overlap we've seen, keep it. 
      // (overlapN may be null if the circle was in the wrong Voronoi region).
      if (overlapN && response && Math.abs(overlap) < Math.abs(response['overlap'])) {
        response['overlap'] = overlap;
        response['overlapN'].copy_(overlapN);
      }
    }
    
    // Calculate the final overlap vector - based on the smallest overlap.
    if (response) {
      response['a'] = polygon;
      response['b'] = circle;
      response['overlapV'].copy_(response['overlapN']).scale(response['overlap']);
    }
    T_VECTORS.push(circlePos); 
    T_VECTORS.push(edge); 
    T_VECTORS.push(point);
    return true;
  }
  SAT['testPolygonCircle'] = testPolygonCircle;
  
  // Check if a circle and a polygon collide.
  //
  // **NOTE:** This is slightly less efficient than polygonCircle as it just
  // runs polygonCircle and reverses everything at the end.
  /**
   * @param {Circle} circle The circle.
   * @param {Polygon} polygon The polygon.
   * @param {Response=} response Response object (optional) that will be populated if
   *   they interset.
   * @return {boolean} true if they intersect, false if they don't.
   */
  function testCirclePolygon(circle, polygon, response) {
    // Test the polygon against the circle.
    var result = testPolygonCircle(polygon, circle, response);
    if (result && response) {
      // Swap A and B in the response.
      var a = response['a'];
      var aInB = response['aInB'];
      response['overlapN'].reverse();
      response['overlapV'].reverse();
      response['a'] = response['b'];
      response['b'] = a;
      response['aInB'] = response['bInA'];
      response['bInA'] = aInB;
    }
    return result;
  }
  SAT['testCirclePolygon'] = testCirclePolygon;
  
  // Checks whether polygons collide.
  /**
   * @param {Polygon} a The first polygon.
   * @param {Polygon} b The second polygon.
   * @param {Response=} response Response object (optional) that will be populated if
   *   they interset.
   * @return {boolean} true if they intersect, false if they don't.
   */
  function testPolygonPolygon(a, b, response) {
    var aPoints = a['calcPoints'];
    var aLen = aPoints.length;
    var bPoints = b['calcPoints'];
    var bLen = bPoints.length;
    // If any of the edge normals of A is a separating axis, no intersection.
    for (var i = 0; i < aLen; i++) {
      if (isSeparatingAxis(a['pos'], b['pos'], aPoints, bPoints, a['normals'][i], response)) {
        return false;
      }
    }
    // If any of the edge normals of B is a separating axis, no intersection.
    for (var i = 0;i < bLen; i++) {
      if (isSeparatingAxis(a['pos'], b['pos'], aPoints, bPoints, b['normals'][i], response)) {
        return false;
      }
    }
    // Since none of the edge normals of A or B are a separating axis, there is an intersection
    // and we've already calculated the smallest overlap (in isSeparatingAxis).  Calculate the
    // final overlap vector.
    if (response) {
      response['a'] = a;
      response['b'] = b;
      response['overlapV'].copy_(response['overlapN']).scale(response['overlap']);
    }
    return true;
  }
  SAT['testPolygonPolygon'] = testPolygonPolygon;

  return SAT;
}));
Rumi.math = {
	_180_DIV_PI: 180 / Math.PI,
	_PI_DIV_180: Math.PI / 180,
	distance: function(x1, y1, x2, y2) {
		return  Math.sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) );
	},
	angle: function(x1, y1, x2, y2) {
		return (Math.atan2(y2 - y1, x2 - x1) * Rumi.math._180_DIV_PI);
	},
	distanceBetweenPoints: function(a, b) {
		return Rumi.math.distance(a.x, a.y, b.x, b.y);
	},
	angleBetweenPoints: function(a, b) {
		return Rumi.math.angle(a.x, a.y, b.x, b.y);
	},
	toDeg: function(rad) {
		return rad * Rumi.math._180_DIV_PI;
	},
	toRad: function(deg) {
		return deg * Rumi.math._PI_DIV_180;
	},
	inRange: function(val, min, max) {
		return (val >= min && val <= max);
	},
	isNumber: function(val) {
		return (
			!Number.isNaN(val) &&
			Number.isFinite(val)
		);
	},
	randomInt: function(min, max) {
		return Math.floor(this.randomFloat(min, max));
	},
	randomFloat: function(min, max) {
		return (Math.random() * (max - min)) + min;
	},
	clamp: function(val, min, max) {
		if(val >= min && val <= max) {
			return val;
		}
		else if(val < min) {
			return min
		}
		else if(val > max) {
			return max;
		}
	},
	percent: function(val, percent) {
		return val * (percent / 100);
	}
};Rumi.array = {
    hasElement: function(array, obj) {
        for(var i = 0, len = array.length; i < len; ++i) {
            if(array[i] === obj) {
                return true;
            }
        }
        return false;
    },
    addIfNotPresent: function(array, obj) {
        if(!Rumi.array.hasElement(array, obj)) {
            array.push(obj);
        }
    },
    removeByVal: function(array, obj) {
        for(var i = array.length - 1; i > -1; --i) {
            if(array[i] === obj) {
                array.splice(i, 1);
                return;
            }
        }
    },
    unique: function(array) {
        var result = [];
        for(var i = 0, len = array.length; i < len; ++i) {
            Rumi.array.addIfNotPresent(result, array[i]);
        }
        return result;
    }
};Rumi.object = {
    /*
    * Returns new object with no duplicate values
    */
    uniqueValues: function(object) {
        var result = {};
        for(var p in object) {
            var foundMatch = false;
            for(var p2 in object) {
                if(p !== p2 && object[p] === object[p2]) {
                    foundMatch = true;
                    break;
                }
            }
            if(!foundMatch) {
                result[p] = object[p];
            }
        }
        return result;
    },
    /**
    *Merges two objects creating a new one which is returned.
    */
    shallowMerge: function(a, b) {
        var result = {};
        for(var p in a) {
            result[p] = a[p];
        }
        for(p in b) {
            result[p] = b[p];
        }
        return result;
    },
    /**
    *Extends object a with b's properties without creating a new object
    */
    extend: function(a, b) {
        for(var p in b) {
            a[p] = b[p];
        }
    },
    unique_extend: function(a, b) {
        for(var p in b) {
            if(a[p] === undefined) a[p] = b[p];
        }
    },
    /**
    *Empties object recursively which may lead to stack overflow in most cases depending on the object!
    */
    destroy: function(obj) {
        if(!Rumi.object.isObject(obj)) return;
        for(var p in obj) {
            if(Rumi.object.isObject(obj[p])) {
                Rumi.object.destroy(obj[p]);
            }
            delete obj[p];
        }
    },
    /**
    *Returns true if the provided argument is object else - false
    */
    isObject: function(obj) {
        return obj === Object(obj);
    }
};/*
*Sociable object is abstract type for an object which is able to communicate with other objects
therefore having instance of Rumi.EventsManager into it and it wraps it in intuitive way
*/
Rumi.SociableInterface = {
	initSociable: function(eventNames) {
		this._eventsManager = new Rumi.EventsManager(eventNames);
	},
	on: function(eventNames, listenerFunction) {
		this._eventsManager.attachEventListener(eventNames, listenerFunction);
		return this;
	},
	off: function(eventNames, listenerFunction) {
		this._eventsManager.detachEventListener(eventNames, listenerFunction);
		return this;
	},
	mute: function(eventNames) {
		this._eventsManager.suppressEvent(eventNames);
		return this;
	},
	unmute: function(eventNames) {
		this._eventsManager.unsuppresssEvent(eventNames);
		return this;
	},
	register: function(eventNames) {
		this._eventsManager.registerEvent(eventNames);
		return this;
	},
	unregister: function(eventNames) {
		this._eventsManager.unregisterEvent(eventNames);
		return this;
	},
	trigger: function(eventNames) {
		this._eventsManager.triggerEvent(eventNames);
		return this;
	}
};
/*
	The interface should be implemented by objects
	which can take advantage of rotation (angle value change tracking).
	The main purpose of this interface is to remove redundant code
	because most of the objects support rotation.
	*	"implementing the interface" refers to the act of
		an object extending itself with the properties
		in the interface object and/or calling additional
		methods in it's constructor or explicitly after implementation
		in order to personally configure the interface
		so there are no shared objects amongst instances implementing the
		same interface !

	This interface can be properly implemented only by objects
	that previously implemented the SociableInterface.
	It relies that the object which implements it
	has introduced AngleChange event which will be
	triggered when the angleRad/angleDeg properties
	inherited from this interface change values.
	If angleDeg's (in degrees) value changes then
	angleRad will be synced with it's value but
	represented in radians and vice versa.
	Each object which implements this interface should
	call it's newly introduced method initRotatable
	in it's constructor or explicitly after implementing the interface !.
*/
Rumi.RotatableInterface = {
	initRotatable: function() {
		this._angleChangeCallback = function() {
			this.trigger('AngleChange');
		}.bind(this);
		var _that = this;
		Object.defineProperties(this, {
			angleDeg: {
				get: function() {
					return _that._angleDeg;
				},
				set: function(newAngleDeg) {
					if(_that._angleDeg != newAngleDeg) {
						_that._angleDeg = newAngleDeg;
						_that._syncRadWithDeg();
						_that._angleChangeCallback();
					}
				}
			},
			angleRad: {
				get: function() {
					return _that._angleRad;
				},
				set: function(newAngleRad) {
					if(_that._angleRad != newAngleRad) {
						_that._angleRad = newAngleRad;
						_that._syncDegWithRad();
						_that._angleChangeCallback();
					}
				}
			}
		});
	},
	_angleRad: 0,
	_angleDeg: 0,
	_angleChangeCallback: null,
	_syncDegWithRad: function() {
		this._angleDeg = Rumi.math.toDeg(this._angleRad);
		return this;
	},
	_syncRadWithDeg: function() {
		this._angleRad = Rumi.math.toRad(this._angleDeg);
		return this;
	}
};
/*Requiring the object implementing this interface to first implement SociableInterface*/
Rumi.StateChangeTrackingInterface = {
	initStateChangeTracking: function(options) {
		this._stateChangeTrackingVariables = {};
		this._parseInitializationObject(options);
	},
	_prepareGetter: function(variableName, initial, optionsObject) {
		/*Analyzes the options object and registers the events needed for the variable value to be properly tracked*/
		var _notifyBefore = optionsObject.each === true ? true : false,
			_internalVariableName = ['_', variableName].join(''),
			_beforeEventName = _notifyBefore ? ['Before', variableName, 'Retrieve'] : undefined,
			_that = this;
		this._stateChangeTrackingVariables[_internalVariableName] = initial;
		if(_beforeEventName) {
			this.register(_beforeEventName);
		}
		/*End of the SociableInterface configuration part so we can return the getter itself after we have all the info*/
		return function() {
			if(_notifyBefore) {
				_that.trigger(_beforeEventName);
			}
			return _that._stateChangeTrackingVariables[_internalVariableName];
		};
	},
	_prepareSetter: function(variableName, optionsObject) {
		var _before = !!optionsObject.before,
			_beforeEach = _before ? !!optionsObject.before.each : false,
			_beforeEachEventName = _beforeEach ? ['BeforeAny', variableName, 'Assignment'].join('') : undefined,
			_beforeChange = _before ? !!optionsObject.before.change : false,
			_beforeChangeEventName = _beforeChange ? ['BeforeSuccessful', variableName, 'Assignment'].join('') : undefined,
			_beforeSame = _before ? !!optionsObject.before.same : false,
			_beforeSameEventName = _beforeSame ? ['BeforeUnsuccessful', variableName, 'Assignment'].join('') : undefined,
			_after = !!optionsObject.after,
			_afterEach = _after ? !!optionsObject.after.each : false,
			_afterEachEventName = _afterEach ? ['AfterAny', variableName, 'Assignment'].join('') : undefined,
			_afterChange = _after ? !!optionsObject.after.change : false,
			_afterChangeEventName = _afterChange ? ['AfterSuccessful', variableName, 'Assignment'].join('') : undefined,
			_afterSame = _after ? !!optionsObject.after.same : false,
			_afterSameEventName = _afterSame ? ['AfterUnsuccessful', variableName, 'Assignment'].join('') : undefined,
			_internalVariableName = ['_', variableName].join(''),
			_that = this;

		if(_beforeEachEventName) this.register(_beforeEachEventName);
		if(_beforeChangeEventName) this.register(_beforeChangeEventName);
		if(_beforeSameEventName) this.register(_beforeSameEventName);
		if(_afterEachEventName) this.register(_afterEachEventName);
		if(_afterChangeEventName) this.register(_afterChangeEventName);
		if(_afterSameEventName) this.register(_afterSameEventName);

		if((_before && (_beforeEach || _beforeChange || _beforeSame)) ||
			(_after && (_afterEach || _afterChange || _afterSame))) {
				return function(newVal) {
						if(_beforeEach) {
							_that.trigger(_beforeEachEventName);
						}
						var _changing = _that._stateChangeTrackingVariables[_internalVariableName] != newVal;
						if(_changing) {
							if(_beforeChange) {
								_that.trigger(_beforeChangeEventName)
							}
							_that._stateChangeTrackingVariables[_internalVariableName] = newVal;
						}
						else if(_beforeSame) {
							_that.trigger(_beforeSameEventName);
						}
						if(_afterEach) {
							_that.trigger(_afterEachEventName);
						}
						if(_changing && _afterChange) {
							_that.trigger(_afterChangeEventName);
						}
						if(!_changing && _afterSame) {
							_that.trigger(_afterSameEventName);
						}
					}
		}
	},
	_parseInitializationObject: function(obj) {
		for(var variable in obj) {
			var getter = this._prepareGetter(variable, obj[variable].initial, obj[variable].get),
				setter = this._prepareSetter(variable, obj[variable].set);
			Object.defineProperty(this, variable, {
				get: getter,
				set: setter
			});
		}
	}
	/*
	{
		variable: {
			intial: (*optional, default: undefined) 'initial value of the variable here',
			get: { (*optional, but if provided a getter for the variable will be created)
				(*optional) before: {
					each: true/false (*optional) - notify before each try to get the value of the variable,
				},
			},
			set: { (*optional, but if a getter was provided and a setter object is provided a setter will automatically be created for the variable)
				(*optional) before: {
					each: true/false (*optional) - notify before each try to change the value of the variable either successful or not,
					change: true/false (*optional) - notify before each try that will successful modify the variable value (different value than the current),
					same: true/false (*optional) - notify before each try to modify the value that will have no effect (same value as current)
				},
				(*optional) after: {
					each: true/false (*optional) - notify after each try to change the value of the variable either successful or not,
					change: true/false (*optional) - notify after each try that successfully modified the variable value (to a different value than the previous),
					same: true/false (*optional) - notify after each try to modify the value that had no effect (same value as previous)
				}
			}
		}
	}
	*/
};
Rumi.RGBA = function(red, green, blue, alpha) {
	/*Required by the sociable object interface*/
	this.initSociable([
		'RedComponentChange',
		'GreenComponentChange',
		'BlueComponentChange',
		'AlphaChannelChange'
	].join(' '));
	this.fromRGBA(
		red || 0,
		green || 0,
		blue || 0,
		alpha || 1
	);
};

Rumi.RGBA.prototype = {
	get red() {
		return this._red;
	},
	set red(newRedValue) {
		if(this._red !== newRedValue) {
			this._red = newRedValue;
			this.trigger('RedComponentChange');
		}
	},
	get green() {
		return this._green;
	},
	set green(newGreenValue) {
		if(this._green !== newGreenValue) {
			this._green = newGreenValue;
			this.trigger('GreenComponentChange');
		}
	},
	get blue() {
		return this._blue;
	},
	set blue(newBlueValue) {
		if(this._blue !== newBlueValue) {
			this._blue = newBlueValue;
			this.trigger('BlueComponentChange');
		}
	},
	get alpha() {
		return this._alpha;
	},
	set alpha(newAlphaValue) {
		if(this._alpha !== newAlphaValue) {
			this._alpha = newAlphaValue;
			this.trigger('AlphaChannelChange');
		}
	},
	RGBToHex: function(r, g, b) {
	    return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
	},
	HextoRGB: function(hex) {
	    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
	    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
	    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
	        return r + r + g + g + b + b;
	    });

	    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	    return result ? {
	        r: parseInt(result[1], 16),
	        g: parseInt(result[2], 16),
	        b: parseInt(result[3], 16)
	    } : null;
	},
	toHex: function() {
		return this.RGBToHex(this._red, this._green, this._blue);
	},
	toRGBA: function() {
		return (this._alpha === 1) ?
				"rgb(" + this._red + ', ' + this._green + ', ' + this._blue + ')' :
				"rgba(" + this._red + ', ' + this._green + ', ' + this._blue + ', ' + this._alpha + ')';
	},
	fromRGBA: function(r, g, b, a) {
		this.red = r;
		this.green = g;
		this.blue = b;
		this.alpha = a;
		return this;
	},
	fromHex: function(hexString) {
		var rgb = this.HextoRGB(hexString);
		this.red = rgb.r;
		this.green = rgb.g;
		this.blue = rgb.b;
		return this;
	},
	fromColorName: function(colorName) {
		if(this.COLORS[colorName] !== undefined) {
			this.fromHex(this.COLORS[colorName]);
		}
		return this;
	},
	randomize: function() {
		this.red = Rumi.math.randomInt(0, 256);
		this.green = Rumi.math.randomInt(0, 256);
		this.blue = Rumi.math.randomInt(0, 256);
		return this;
	},
	/*
	*Returns a copy of this object.
 	*NOTE: All connections between the two objects are cut.They are independent objects sharing same values but not references!
 	*/
	copy: function() {
		return new Rumi.RGBA(this._red, this._green, this._blue, this._alpha);
	},
	/*This copies other. Both instances share same values but not references !*/
	copies: function(other) {
		this.red = other._red;
		this.green = other._green;
		this.blue = other._blue;
		this.alpha = other._alpha;
		return this;
	},
	COLORS: {
		aliceblue: 'f0f8ff',
		antiquewhite: 'faebd7',
		aqua: '00ffff',
		aquamarine: '7fffd4',
		azure: 'f0ffff',
		beige: 'f5f5dc',
		bisque: 'ffe4c4',
		black: '000000',
		blanchedalmond: 'ffebcd',
		blue: '0000ff',
		blueviolet: '8a2be2',
		brown: 'a52a2a',
		burlywood: 'deb887',
		cadetblue: '5f9ea0',
		chartreuse: '7fff00',
		chocolate: 'd2691e',
		coral: 'ff7f50',
		cornflowerblue: '6495ed',
		cornsilk: 'fff8dc',
		crimson: 'dc143c',
		cyan: '00ffff',
		darkblue: '00008b',
		darkcyan: '008b8b',
		darkgoldenrod: 'b8860b',
		darkgray: 'a9a9a9',
		darkgreen: '006400',
		darkkhaki: 'bdb76b',
		darkmagenta: '8b008b',
		darkolivegreen: '556b2f',
		darkorange: 'ff8c00',
		darkorchid: '9932cc',
		darkred: '8b0000',
		darksalmon: 'e9967a',
		darkseagreen: '8fbc8f',
		darkslateblue: '483d8b',
		darkslategray: '2f4f4f',
		darkturquoise: '00ced1',
		darkviolet: '9400d3',
		deeppink: 'ff1493',
		deepskyblue: '00bfff',
		dimgray: '696969',
		dodgerblue: '1e90ff',
		feldspar: 'd19275',
		firebrick: 'b22222',
		floralwhite: 'fffaf0',
		forestgreen: '228b22',
		fuchsia: 'ff00ff',
		gainsboro: 'dcdcdc',
		ghostwhite: 'f8f8ff',
		gold: 'ffd700',
		goldenrod: 'daa520',
		gray: '808080',
		green: '008000',
		greenyellow: 'adff2f',
		honeydew: 'f0fff0',
		hotpink: 'ff69b4',
		indianred : 'cd5c5c',
		indigo : '4b0082',
		ivory: 'fffff0',
		khaki: 'f0e68c',
		lavender: 'e6e6fa',
		lavenderblush: 'fff0f5',
		lawngreen: '7cfc00',
		lemonchiffon: 'fffacd',
		lightblue: 'add8e6',
		lightcoral: 'f08080',
		lightcyan: 'e0ffff',
		lightgoldenrodyellow: 'fafad2',
		lightgrey: 'd3d3d3',
		lightgreen: '90ee90',
		lightpink: 'ffb6c1',
		lightsalmon: 'ffa07a',
		lightseagreen: '20b2aa',
		lightskyblue: '87cefa',
		lightslateblue: '8470ff',
		lightslategray: '778899',
		lightsteelblue: 'b0c4de',
		lightyellow: 'ffffe0',
		lime: '00ff00',
		limegreen: '32cd32',
		linen: 'faf0e6',
		magenta: 'ff00ff',
		maroon: '800000',
		mediumaquamarine: '66cdaa',
		mediumblue: '0000cd',
		mediumorchid: 'ba55d3',
		mediumpurple: '9370d8',
		mediumseagreen: '3cb371',
		mediumslateblue: '7b68ee',
		mediumspringgreen: '00fa9a',
		mediumturquoise: '48d1cc',
		mediumvioletred: 'c71585',
		midnightblue: '191970',
		mintcream: 'f5fffa',
		mistyrose: 'ffe4e1',
		moccasin: 'ffe4b5',
		navajowhite: 'ffdead',
		navy: '000080',
		oldlace: 'fdf5e6',
		olive: '808000',
		olivedrab: '6b8e23',
		orange: 'ffa500',
		orangered: 'ff4500',
		orchid: 'da70d6',
		palegoldenrod: 'eee8aa',
		palegreen: '98fb98',
		paleturquoise: 'afeeee',
		palevioletred: 'd87093',
		papayawhip: 'ffefd5',
		peachpuff: 'ffdab9',
		peru: 'cd853f',
		pink: 'ffc0cb',
		plum: 'dda0dd',
		powderblue: 'b0e0e6',
		purple: '800080',
		red: 'ff0000',
		rosybrown: 'bc8f8f',
		royalblue: '4169e1',
		saddlebrown: '8b4513',
		salmon: 'fa8072',
		sandybrown: 'f4a460',
		seagreen: '2e8b57',
		seashell: 'fff5ee',
		sienna: 'a0522d',
		silver: 'c0c0c0',
		skyblue: '87ceeb',
		slateblue: '6a5acd',
		slategray: '708090',
		snow: 'fffafa',
		springgreen: '00ff7f',
		steelblue: '4682b4',
		tan: 'd2b48c',
		teal: '008080',
		thistle: 'd8bfd8',
		tomato: 'ff6347',
		turquoise: '40e0d0',
		violet: 'ee82ee',
		violetred: 'd02090',
		wheat: 'f5deb3',
		white: 'ffffff',
		whitesmoke: 'f5f5f5',
		yellow: 'ffff00',
		yellowgreen: '9acd32'
	}
};

Rumi.object.extend(Rumi.RGBA.prototype, Rumi.SociableInterface);
Rumi.Stroke = function(thickness, color, blendMode) {
	/*Required by SociableObject interface*/
	this.initSociable([
		'ThicknessChange',
		'ColorChange',
		'BlendModeChange'
	].join(' '));
	this._thickness = thickness || 1;
	this._blendMode = blendMode || Rumi.BlendModes.SOURCE_OVER;
	this.color = color ? color.copy() : new Rumi.RGBA();

	this.color.on('*', function() {
		this.trigger('ColorChange');
	}.bind(this));
};

Rumi.Stroke.prototype = {
	get blendMode() {
		return this._blendMode;

	},
	set blendMode(newBlendMode) {
		if(this._blendMode != newBlendMode) {
			this._blendMode = newBlendMode;
			this.trigger('BlendModeChange');
		}
	},
	get thickness() {
		return this._thickness;
	},
	set thickness(newThickness) {
		if(this._thickness != newThickness) {
			this._thickness = newThickness;
			this.trigger('ThicknessChange');
		}
	},
	apply: function(context) {
		if(this.thickness <= 0) return;
		context.save();
			this.applyRaw(context);
			context.stroke();
		context.restore();
		return this;
	},
	/*
	*Polutes context but allows operations with the newly introduced changes to the context to be made !
 	*Note: This method doesn't call context.stroke automatically in order to be more flexible !
 	*/
	applyRaw: function(context) {
		context.strokeStyle = this.color.toRGBA();
		context.globalCompositeOperation = this.blendMode;
		context.lineWidth = this.thickness;
		return this;
	},
	/*
	*Returns a copy of this object.
	*NOTE: All connections between the two objects are cut.
	*They are independent objects sharing same values but not references !*/
	copy: function() {
		return new Rumi.Stroke(this.thickness, this.color, this.blendMode);
	},
	/*This copies other. Both instances share same values but not references !*/
	copies: function(other) {
		this.thickness = other.thickness;
		this.blendMode = other.blendMode;
		this.color.copies(other.color);
		return this;
	}
};

Rumi.object.extend(Rumi.Stroke.prototype, Rumi.SociableInterface);
Rumi.Fill = function(color, blendMode) {
	this.initSociable([
		'ColorChange',
		'BlendModeChange'
	].join(' '));
	this._blendMode = blendMode || Rumi.BlendModes.SOURCE_OVER,
	this.color = color ? color.copy() : new Rumi.RGBA();

	this.color.on('*', function() {
		this.trigger('ColorChange');
	}.bind(this));
};

Rumi.Fill.prototype = {
	get blendMode() {
		return this._blendMode;
	},
	set blendMode(newBlendMode) {
		if(this._blendMode != newBlendMode) {
			this._blendMode = newBlendMode;
			this.trigger('BlendModeChange');
		}
	},
	apply: function(context) {
		context.save();
			this.applyRaw(context);
			context.fill();
		context.restore();
		return this;
	},
	/*
	*Polutes context but allows operations with the newly introduced changes to the context to be made !
	*Note: This method doesn't call context.fill automatically in order to be more flexible !
	*/
	applyRaw: function(context) {
		context.fillStyle = this.color.toRGBA();
		context.globalCompositeOperation = this.blendMode;
		context.globalAlpha = this.color.alpha;
		return this;
	},
	/*Returns a copy of this object.
	 * NOTE: All connections between the two objects are cut.
	 * 		They are independent objects sharing same values but not references !*/
	copy: function() {
		return new Rumi.Fill(this.color.copy(), this.blendMode);
	},
	/*This copies other. Both instances share same values but not references !*/
	copies: function(other) {
		this.color.copies(other.color);
		this.blendMode = other.blendMode;
	}
};

Rumi.object.extend(Rumi.Fill.prototype, Rumi.SociableInterface);
/** 
	@memberof Rumi
	@readonly
	@enum {string}
*/
Rumi.BlendModes = {
	RANDOM_EXCEPT: function(modesArray) {
		var rndMode = Rumi.BlendModes.RANDOM;
		return (!Rumi.array.hasElement(rndMode)) ? rndMode : Rumi.BlendModes.RANDOM_EXCEPT(modesArray);
	},
	/*Random BlendMode on each call to the RANDOM property of Rumi.BlendModes*/
	get RANDOM() {
		var keys = Object.keys(Rumi.BlendModes);
		return Rumi.BlendModes[keys[Rumi.math.randomInt(0, keys.length)]];
	},
	/**
		@desc This is the default setting and draws new shapes on top of the existing canvas content.
	*/
	SOURCE_OVER: 'source-over',
	/** 
		@desc The new shape is drawn only where both the new shape and the destination canvas overlap. Everything else is made transparent.
	*/
	SOURCE_IN: 'source-in',
	/**
		@desc The new shape is drawn where it doesn't overlap the existing canvas content.
	*/
	SOURCE_OUT: 'source-out',
	/** 
		@desc The new shape is only drawn where it overlaps the existing canvas content.
	*/
	SOURCE_ATOP: 'source-atop',
	/**
		@desc New shapes are drawn behind the existing canvas content.
	*/
	DESTINATION_OVER: 'destination-over',
	/**
		@desc The existing canvas content is kept where both the new shape and existing canvas content overlap. Everything else is made transparent.
	*/
	DESTINATION_IN: 'destination-in',
	/** 
		@desc The existing content is kept where it doesn't overlap the new shape.
	*/
	DESTINATION_OUT: 'destination-out',
	/** 
		@desc The existing canvas is only kept where it overlaps the new shape. The new shape is drawn behind the canvas content.
	*/
	DESTINATION_ATOP: 'destination-atop',
	/** 
		@desc Where both shapes overlap the color is determined by adding color values.
	*/
	LIGHTER: 'lighter',
	/** 
		@desc Only the existing canvas is present.
	*/
	COPY: 'copy',
	/** 
		@desc Shapes are made transparent where both overlap and drawn normal everywhere else.
	*/
	XOR: 'xor',
	/** 
		@desc The pixels are of the top layer are multiplied with the corresponding pixel of the bottom layer. A darker picture is the result.
	*/
	MULTIPLY: 'multiply',
	/** 
		@desc The pixels are inverted, multiplied, and inverted again. A lighter picture is the result (opposite of multiply)
	*/
	SCREEN: 'screen',
	/** 
		@desc A combination of multiply and screen. Dark parts on the base layer become darker, and light parts become lighter.
	*/
	OVERLAY: 'overlay',
	/** 
		@desc Retains the darkest pixels of both layers.
	*/
	DARKEN: 'darken',
	/** 
		@desc Retains the lightest pixels of both layers.
	*/
	LIGHTEN: 'lighten',
	/** 
		@desc Divides the bottom layer by the inverted top layer.
	*/
	COLOR_DODGE: 'color-dodge',
	/** 
		@desc Divides the inverted bottom layer by the top layer, and then inverts the result.
	*/
	COLOR_BURN: 'color-burn',
	/** 
		@desc A combination of multiply and screen like overlay, but with top and bottom layer swapped.
	*/
	HARD_LIGHT: 'hard-light',
	/** 
		@desc A softer version of hard-light. Pure black or white does not result in pure black or white.
	*/
	SOFT_LIGHT: 'soft-light',
	/** 
		@desc Subtracts the bottom layer from the top layer or the other way round to always get a positive value.
	*/
	DIFFERENCE: 'difference',
	/** 
		@desc Like difference, but with lower contrast.
	*/
	EXCLUSION: 'exclusion',
	/** 
		@desc Preserves the luma and chroma of the bottom layer, while adopting the hue of the top layer.
	*/
	HUE: 'hue',
	/** 
		@desc Preserves the luma and hue of the bottom layer, while adopting the chroma of the top layer.
	*/
	SATURATION: 'saturation',
	/** 
		@desc Preserves the luma of the bottom layer, while adopting the hue and chroma of the top layer.
	*/
	COLOR: 'color',
	/** 
		@desc Preserves the hue and chroma of the bottom layer, while adopting the luma of the top layer.
	*/
	LUMINOSITY: 'luminosity'
};/*ESAT stays for ExtendedSAT (i.e. Extended SAT Box => ESATBox*/
Rumi.ESATVector = function(x, y) {
    this.initSociable([
        'XChange',
        'YChange',
        'FillChange',
        'StrokeChange'
    ].join(' '));
    this.fill = new Rumi.Fill();
    this.stroke = new Rumi.Stroke();

    this._x = x || 0;
    this._y = y || 0;

    this.fill.on('*', function() {
        this.trigger('FillChange');
    }.bind(this));

    this.stroke.on('*', function() {
        this.trigger('StrokeChange');
    }.bind(this));
};

Rumi.ESATVector.prototype = {
    get x() {
        return this._x;
    },
    set x(newX) {
        if(this._x != newX) {
            this._x = newX;
            this.trigger('XChange');
        }
    },
    get y() {
        return this._y;
    },
    set y(newY) {
        if(this._y != newY) {
            this._y = newY;
            this.trigger('YChange');
        }
    },
    draw: function(context) {
        context.beginPath();
        context.rect(this.x, this.y, 1, 1);
        context.closePath();
        this.fill.apply(context);
        this.stroke.apply(context);
        return this;
    },
    copy: function() {
        var copy = new Rumi.ESATVector(this.x, this.y);
        copy.stroke.copies(this.stroke);
        copy.fill.copies(this.fill);
        return copy;
    },
    copies: function(other) {
        this.x = other.x;
        this.y = other.y;
        this.fill.copies(other.fill);
        this.stroke.copies(other.fill);
        return this;
    }
};

Rumi.object.unique_extend(Rumi.ESATVector.prototype, SAT.Vector.prototype);
Rumi.object.extend(Rumi.ESATVector.prototype, Rumi.SociableInterface);
/*ESAT stays for ExtendedSAT (i.e. Extended SAT Box => ESATBox*/
Rumi.ESATCircle = function(position, radius) {
    this.initSociable([
        'PositionChange',
        'RadiusChange',
        'FillChange',
        'StrokeChange'
    ].join(' '));
    this.fill = new Rumi.Fill();
    this.stroke = new Rumi.Stroke();

    this.position = position ? position.copy() : new Rumi.ESATVector();
    this.radius = radius || 0;

    this.fill.on('*', function() {
        this.trigger('FillChange');
    }.bind(this));

    this.stroke.on('*', function() {
        this.trigger('StrokeChange');
    }.bind(this));

    this.position.on('XChange YChange', function() {
        this.trigger('PositionChange');
    }.bind(this));
};

Rumi.ESATCircle.prototype = {
    draw: function(context) {
        context.beginPath();
        context.arc(this.position.x, this.position.y, this.radius, 0, 2 * Math.PI);
        context.closePath();
        this.fill.apply(context);
        this.stroke.apply(context);
        return this;
    },
    copy: function() {
        var copy = new Rumi.ESATCircle(this.position, this.radius);
        copy.stroke.copies(this.stroke);
        copy.fill.copies(this.fill);
        return copy;
    },
    copies: function(other) {
        this.pos.copies(other.position);
        this.radius = other.radius;
        this.fill.copies(other.fill);
        this.stroke.copies(other.stroke);
        return this;
    },
    get radius() {
        return this._radius;
    },
    set radius(newRadius) {
        if(this._radius != newRadius) {
            this._radius = newRadius;
            this.trigger('RadiusChange');
        }
    },
    /*For backward compatibility with SAT.js*/
    get pos() {
        return this.position;
    },
    /*For backward compatibility with SAT.js*/
    get r() {
        return this._radius;
    },
    /*For backward compatibility with SAT.js*/
    set r(newRadius) {
        if(this._radius != newRadius) {
            this._radius = newRadius;
            this.trigger('RadiusChange');
        }
    }
};

Rumi.object.unique_extend(Rumi.ESATCircle.prototype, SAT.Circle.prototype);
Rumi.object.extend(Rumi.ESATCircle.prototype, Rumi.SociableInterface);
/*ESAT stays for ExtendedSAT (i.e. Extended SAT Box => ESATBox*/
Rumi.ESATBox = function(position, width, height) {
    this.fill = new Rumi.Fill();
    this.stroke = new Rumi.Stroke();
    this.initSociable([
        'PositionChange',
        'WidthChange',
        'HeightChange',
        'FillChange',
        'StrokeChange'
    ].join(' '));

    this.position = position ? position.copy() : new Rumi.ESATVector();
    this._width = width || 0;
    this._height = height || 0;

    this.fill.on('*', function() {
        this.trigger('FillChange');
    }.bind(this));

    this.stroke.on('*', function() {
        this.trigger('StrokeChange');
    }.bind(this));

    this.pos.on('XChange YChange', function() {
        this.trigger('PositionChange');
    }.bind(this));
};

Rumi.ESATBox.prototype = {
    draw: function(context) {
        context.beginPath();
        context.rect(this.position.x, this.position.y, this.width, this.height);
        context.closePath();
        this.fill.apply(context);
        this.stroke.apply(context);
        return this;
    },
    copy: function() {
        var copy = new Rumi.ESATBox(this.position, this.width, this.height);
        copy.stroke.copies(this.stroke);
        copy.fill.copies(this.fill);
        return copy;
    },
    copies: function(other) {
        this.position.copies(other.position);
        this.width = other.width;
        this.height = other.height;
        this.fill.copies(other.fill);
        this.stroke.copies(other.stroke);
        return this;
    },
    /*Makes a polygon out of the current box and copies the additional properties of the box into it*/
    toPolygon: function() {
        var _poly = new Rumi.ESATPolygon(
            this.position,
            [
                new Rumi.ESATVector(),
                new Rumi.ESATVector(this.width, 0),
                new Rumi.ESATVector(this.width, this.height),
                new Rumi.ESATVector(0, this.height)
            ]
        );
        _poly.fill.copies(this.fill);
        _poly.stroke.copies(this.stroke);
        return _poly;
    },
    sameSizeAs: function(other) {
        return this.width === other.width && this.height === other.height;
    },
    above: function(other) {
        return this.position.y + this.height < other.position.y;
    },
    left: function(other) {
        return this.position.x + this.width < other.position.x;
    },
    below: function(other) {
        return this.position.y > other.position.y + other.height;
    },
    right: function(other) {
        return this.position.x > other.position.x + other.width;
    },
    neighbour: function(other) {
        return (
            this.above(other) ||
            this.left(other) ||
            this.below(other) ||
            this.right(other)
        );
    },
    get width() {
        return this._width;
    },
    set width(newWidth) {
        if(this._width != newWidth) {
            this._width = newWidth;
            this.trigger('WidthChange');
        }
    },
    get height() {
        return this._height;
    },
    set height(newHeight) {
        if(this._height != newHeight) {
            this._height = newHeight;
            this.trigger('HeightChange');
        }
    },
    /*For backward compatibility with SAT.js*/
    get pos() {
        return this.position;
    },
    /*For backward compatibility with SAT.js*/
    get w() {
        return this._width;
    },
    /*For backward compatibility with SAT.js*/
    set w(newWidth) {
        if(this._width != newWidth) {
            this._width = newWidth;
            this.trigger('WidthChange');
        }
    },
    /*For backward compatibility with SAT.js*/
    get h() {
        return this._height;
    },
    /*For backward compatibility with SAT.js*/
    set h(newHeight) {
        if(this._height != newHeight) {
            this._height = newHeight;
            this.trigger('HeightChange');
        }
    }
};

Rumi.object.unique_extend(Rumi.ESATBox.prototype, SAT.Box.prototype);
Rumi.object.extend(Rumi.ESATBox.prototype, Rumi.SociableInterface);
/*ESAT stays for ExtendedSAT (i.e. Extended SAT Box => ESATBox*/
Rumi.ESATPolygon = function(position, points) {
     this.initSociable([
        'PositionChange',
        'AnchorChange',
        'AngleChange',
        'PointsChange',
        'OffsetChange',
        'FillChange',
        'StrokeChange'
    ].join(' '));
    this.position = position ? position.copy() : new Rumi.ESATVector();
    this.offset = new Rumi.ESATVector();
    this.fill = new Rumi.Fill();
    this.stroke = new Rumi.Stroke();
    this.anchor = new Rumi.ESATVector();
    this._lastAnchor = this.anchor.copy();
    this._angleRad = 0;
    this._angleDeg = 0;
    this._lastAngleRad = this._angleRad;

    this.fill.on('*', function() {
        this.trigger('FillChange');
    }.bind(this));

    this.stroke.on('*', function() {
        this.trigger('StrokeChange');
    }.bind(this));

    this.offset.on('XChange YChange', function() {
        this.setOffset(this.offset);
        this.trigger('OffsetChange');
    }.bind(this));

    this.pos.on('XChange YChange', function() {
        this.trigger('PositionChange');
    }.bind(this));

    this.anchor.on('XChange YChange', function() {
        this.trigger('AnchorChange');
        this._rotateAroundAnchor();
    }.bind(this));

    this.setPoints(points || []);
};

Rumi.ESATPolygon.prototype = {
    draw: function(context) {
        var points = this.calcPoints;
        if(points.length === 0) return;
        context.beginPath();
        context.moveTo(this.position.x + points[0].x, this.position.y + points[0].y);
        for(var i = 0; i < points.length; i++) {
            context.lineTo(this.position.x + points[i].x, this.position.y + points[i].y);
        }
        context.closePath();
        this.fill.apply(context);
        this.stroke.apply(context);
        return this;
    },
    _copyArrayOfVectors: function(array) {
        var copy = [];
        for(var i = 0, len = array.length; i < len; ++i) {
            copy[i] = array[i].copy();
        }
        return copy;
    },
    /*For backward compatibility with SAT*/
    setPoints: function(newPoints) {
        SAT.Polygon.prototype.setPoints.call(this, this._copyArrayOfVectors(newPoints));
        this.trigger('PointsChange');
        return this;
    },
    _rotateAroundAnchor: function() {
        this.translate(-this.anchor.x, -this.anchor.y);
        this.rotate(-this._lastAngleRad);
        this.rotate(this._angleRad);
        this._lastAngleRad = this._angleRad;
        /*Revert last last rotation and apply the new one rotate(-angle); rotate(newAngle);*/
        this.translate(this.anchor.x, this.anchor.y);
        return this;
    },
    copy: function() {
        copy = new Rumi.ESATPolygon(this.position.copy(), this._copyArrayOfVectors(this.points));
        copy.anchor.copies(this.anchor);
        copy.angle = this.angle;
        copy.stroke.copies(this.stroke);
        copy.fill.copies(this.fill);
        return copy;
    },
    copies: function(other) {
        this.position.copies(other.pos);
        this.anchor.copies(other.anchor);
        this.setPoints(this._copyArrayOfVectors(other.points));
        this.angle = other.angle;
        this.fill.copies(other.fill);
        this.stroke.copies(other.stroke);
        return this;
    },
    /*For backward compatibility with SAT*/
    get angle() {
        return this._angleRad;
    },
    /*For backward compatibility with SAT*/
    set angle(newAngle) {
        if(this._angleRad != newAngle) {
            this._angleRad = newAngle;
            this.trigger('AngleChange');
        }
    },
    get angleRad() {
        return this._angleRad;
    },
    set angleRad(newAngleRad) {
        if(this._angleRad != newAngleRad) {
            this._angleRad = newAngleRad;
            this._angleDeg = Rumi.math.toDeg(newAngleRad);
            this._rotateAroundAnchor();
            this.trigger('AngleChange');
        }
    },
    get angleDeg() {
        return Rumi.math.toDeg(this._angleRad);
    },
    set angleDeg(newAngleDeg) {
        if(this.angleDeg != newAngleDeg) {
            this._angleDeg = newAngleDeg;
            this._angleRad = Rumi.math.toRad(newAngleDeg);
            this._rotateAroundAnchor();
            this.trigger('AngleChange');
        }
    },
    /*For backward compatibility with SAT*/
    get pos() {
        return this.position;
    }
};

Rumi.object.extend(Rumi.ESATPolygon.prototype, SAT.Polygon.prototype);
Rumi.object.extend(Rumi.ESATPolygon.prototype, Rumi.SociableInterface);
Rumi.ESATCollision = {
	vectorVsVector: function(vectorA, vectorB) {
		return (vectorA.x === vectorB.x && vectorA.y === vectorB.y);
	},
	vectorVsBox: function(vector, box) {
		return (
			!(vector.x < box.pos.x || vector.x > box.pos.x + box.width) &&
			!(vector.y < box.pos.y || vector.y > box.pos.y + box.height)
		);
	},
	vectorVsCircle: function(vector, circle) {
		return (
			!(vector.x < circle.pos.x - circle.radius || vector.x > circle.pos.x + circle.radius) &&
			!(vector.y < circle.pos.y - circle.radius || vector.y > circle.pos.y + circle.radius)
		);
	},
	vectorVsPolygon: function(vector, polygon) {
		return SAT.pointInPolygon(vector, polygon);
	},
	circleVsCircle: function(circleA, circleB) {
		return (
			!(
				circleA.pos.x - circleA.radius < circleB.pos.x - circleB.radius ||
				circleA.pos.x + circleA.radius > circleB.pos.x + circleB.radius
			)
			&&
			!(
				circleA.pos.y - circleA.radius < circleB.pos.y - circleB.radius ||
				circleA.pos.y + circleA.radius > circleB.pos.y + circleB.radius
			)
		);
	},
	circleVsBox: function(circle, box) {
		return (
			!(
				box.pos.x + box.width < circle.pos.x - circle.radius ||
				box.pos.x > circle.pos.x + circle.radius
			)
			&&
			!(
				box.pos.y + box.height < circle.pos.y - circle.radius ||
				box.pos.y > circle.pos.y + circle.radius
			)
		);
	},
	circleVsPolygon: function(circle, polygon) {
		return SAT.testPolygonCircle(polygon, circle);
	},
	boxVsBox: function(boxA, boxB) {
		return (
			!(boxA.pos.x < boxB.pos.x || boxA.pos.x > boxB.pos.x + boxB.width) &&
			!(boxA.pos.y < boxB.pos.y || boxA.pos.y > boxB.pos.y + boxB.height)
		);
	},
	boxVsPolygon: function(box, poly) {
		return SAT.testPolygonPolygon(box.toPolygon(), poly);
	},
	polygonVsPolygon: function(polygonA, polygonB) {
		return SAT.testPolygonPolygon(polygonA, polygonB);
	},
	getShapeType: function(shape) {
		if(shape instanceof Rumi.ESATVector) {
            return 'vector';
        }
        else if(shape instanceof Rumi.ESATBox) {
            return 'box';
        }
        else if(shape instanceof Rumi.ESATCircle) {
            return 'circle';
        }
        else if(shape instanceof Rumi.ESATPolygon) {
            return 'polygon';
        }
	}
};

/*These methods are defined after the object else during their creation the object is undefined !*/
Rumi.ESATCollision.circleVsVector = function(circle, vector) {
	return Rumi.ESATCollision.vectorVsCircle(vector, circle);
};

Rumi.ESATCollision.boxVsVector = function(box, vector) {
	return Rumi.ESATCollision.vectorVsBox(vector, box);
};

Rumi.ESATCollision.boxVsCircle = function(box, circle) {
	return Rumi.ESATCollision.circleVsBox(circle, box);
};

Rumi.ESATCollision.polygonVsVector = function(polygon, vector) {
	return Rumi.ESATCollision.vectorVsPolygon(vector, polygon);
};

Rumi.ESATCollision.polygonVsCircle = function(polygon, circle) {
	return Rumi.ESATCollision.circleVsPolygon(circle, polygon);
};

Rumi.ESATCollision.polygonVsBox = function(polygon, box) {
	return Rumi.ESATCollision.boxVsPolygon(box, polygon);
};

Rumi.ESATCollision._SHAPE_COLLISION_METHOD_LUT = {
	vector: {
		vector: Rumi.ESATCollision.vectorVsVector,
		circle: Rumi.ESATCollision.vectorVsCircle,
		box: Rumi.ESATCollision.vectorVsBox,
		polygon: Rumi.ESATCollision.vectorVsPolygon
	},
	circle: {
		vector: Rumi.ESATCollision.circleVsVector,
		circle: Rumi.ESATCollision.circleVsCircle,
		box: Rumi.ESATCollision.circleVsBox,
		polygon: Rumi.ESATCollision.circleVsPolygon
	},
	box: {
		vector: Rumi.ESATCollision.boxVsVector,
		circle: Rumi.ESATCollision.boxVsCircle,
		box: Rumi.ESATCollision.boxVsBox,
		polygon: Rumi.ESATCollision.boxVsPolygon
	},
	polygon: {
		vector: Rumi.ESATCollision.polygonVsVector,
		circle: Rumi.ESATCollision.polygonVsCircle,
		box: Rumi.ESATCollision.polygonVsBox,
		polygon: Rumi.ESATCollision.polygonVsBox
	}
};

Rumi.ESATCollision.anyVsAny = function(shapeA, shapeB) {
	var typeA = Rumi.ESATCollision.getShapeType(shapeA),
		typeB = Rumi.ESATCollision.getShapeType(shapeB);
	if(typeA && typeB) {
		return Rumi.ESATCollision._SHAPE_COLLISION_METHOD_LUT[typeA][typeB](shapeA, shapeB);
	}
	return false;
};

Rumi.ESATPolygon.prototype.colliding = function(other) {
	return Rumi.ESATCollision.anyVsAny(this, other);
};

Rumi.ESATVector.prototype.colliding = Rumi.ESATBox.prototype.colliding =
Rumi.ESATCircle.prototype.colliding = Rumi.ESATPolygon.prototype.colliding;
/**
 * Creates a drawable screen
 * @param {?Rumi.ESATBox} box - Box used to initialize the screen
 * @constructor
 */
Rumi.Graphics2D = function(box) {
	this.initSociable([
		'PositionChange',
		'WidthChange',
		'HeightChange'
	].join(' '));

	this.box = box ? box.copy() : new Rumi.ESATBox(new Rumi.ESATVector(), 800, 600);

	this._context = document.createElement('canvas').getContext('2d');
	this._context.canvas.style.position = 'absolute';

	this.box.on('PositionChange', function() {
		this._updateCanvasPosition();
		this.trigger('PositionChange');
	}.bind(this));

	this.box.on('WidthChange', function() {
		this._updateCanvasSize();
		this.trigger('WidthChange');
	}.bind(this));

	this.box.on('HeightChange', function() {
		this._updateCanvasSize();
		this.trigger('HeightChange');
	}.bind(this));

	/*Initialize canvas position and size*/
	this._updateCanvasPosition();
	this._updateCanvasSize();
};

Rumi.Graphics2D.prototype = {
	_savedContent: null,
	get context() {
		return this._context;
	},
	get canvas() {
		return this._context.canvas;
	},
	_updateCanvasPosition: function() {
		this._context.canvas.style.marginLeft = this.box.position.x + 'px';
		this._context.canvas.style.marginTop = this.box.position.y + 'px';
		return this;
	},
	_saveCanvasContent: function() {
		if(this.box.width > 0 && this.box.height > 0) {
			this._savedContent = this._context.getImageData(0, 0, this.box.width, this.box.height);
		}
		return this;
	},
	_restoreCanvasContent: function() {
		if(this._savedContent) {
			this._context.putImageData(this._savedContent, 0, 0);
			this._savedContent = null;
		}
		return this;
	},
	_updateCanvasSize: function() {
		this._saveCanvasContent();
		this._context.canvas.width = this.box.width;
		this._context.canvas.height = this.box.height;
		this._context.canvas.style.width = this.box.width + 'px';
		this._context.canvas.style.height = this.box.height + 'px';
		this._restoreCanvasContent();
		return this;
	},
	clear: function() {
		this._context.clearRect(0, 0, this.box.width, this.box.height);
		return this;
	}
};

Rumi.object.extend(Rumi.Graphics2D.prototype, Rumi.SociableInterface);
Rumi.load = {
    images: function(pseudonymToUrlHash, done) {
        pseudonymToUrlHash = Rumi.object.uniqueValues(pseudonymToUrlHash);
        var images = {},
            expected = Object.keys(pseudonymToUrlHash).length,
            loaded = 0,
            missed = 0,
            cleanHandlers = function(image) {
                delete image.pseudonym;
                image.onerror = null;
                image.onload = null;
            },
            signalIfDone = function() {
                if(loaded + missed === expected) {
                    done(images);
                }
            },
            onLoad = function() {
                loaded++;
                images[this.pseudonym] = this;
                cleanHandlers(this);
                signalIfDone();
            },
            onError = function() {
                missed++;
                cleanHandlers(this);
                signalIfDone();
            };

        for(var p in pseudonymToUrlHash) {
            var img = document.createElement('img');
            img.onload = onLoad.bind(img);
            img.onerror = onError.bind(img);
            img.pseudonym = p;
            img.src = pseudonymToUrlHash[p];
        }
    },
    audios: function(pseudonymToUrlHash, done) {
        pseudonymToUrlHash = Rumi.object.uniqueValues(pseudonymToUrlHash);
        var audios = {},
            expected = Object.keys(pseudonymToUrlHash).length,
            loaded = 0,
            missed = 0,
            cleanHandlers = function(audio) {
                delete audio.pseudonym;
                audio.onerror = null;
                audio.oncanplaythrough = null;
            },
            signalIfDone = function() {
                if(loaded + missed === expected) {
                    done(audios);
                }
            },
            onCanPlayThrough = function() {
                loaded++;
                audios[this.pseudonym] = this;
                cleanHandlers(this);
                signalIfDone();
            },
            onError = function() {
                missed++;
                cleanHandlers(this);
                signalIfDone();
            };

        for(var p in pseudonymToUrlHash) {
            var audio = document.createElement('audio');
            audio.oncanplaythrough = onCanPlayThrough.bind(audio);
            audio.onerror = onError.bind(audio);
            audio.pseudonym = p;
            audio.src = pseudonymToUrlHash[p];
        }
    }
};/**
 * Created by vili on 3/30/2016.
 */
Rumi.LoadingManager = function() {
    this._images = {};
    this._audios = {};
};

Rumi.LoadingManager.prototype = {
    images: function(pseudonymToUrlHash) {
        this._images = Rumi.object.shallowMerge(this._images, pseudonymToUrlHash);
    },
    audios: function(pseudonymToUrlHash) {
        this._audios = Rumi.object.shallowMerge(this._audios, pseudonymToUrlHash);
    },
    load: function(done) {
        if(this._hasEmptyQueue()) {
            done();
            return;
        }
        var result = {},
            expectCount = 0,
            count = 0,
            onAnyReady = function(files) {
                count++;
                result = Rumi.object.shallowMerge(result, files);
                if(count === expectCount) {
                    this._images = {};
                    this._audios = {};
                    done(result);
                }
            }.bind(this);
        if(Object.keys(this._images).length > 0) expectCount++;
        if(Object.keys(this._audios).length > 0) expectCount++;
        Rumi.load.images(this._images, onAnyReady);
        Rumi.load.audios(this._audios, onAnyReady);
    },
    _hasEmptyQueue: function() {
        return (Object.keys(this._images).length === 0 && Object.keys(this._audios).length === 0);
    }
};/**
 * Created by vili on 3/14/2016.
 */
Rumi.ajax = {
    load: function(url, success, fail, done) {
        if(XMLHttpRequest !== undefined) {
            var request = new XMLHttpRequest();
            request.onerror = fail;
            request.onloadend = done;
            request.onreadystatechange = function() {
                if(request.readyState === 4 && request.status === 200) {
                    success(request.response);
                }
            };
            request.open('GET', url, true);
            request.send('');
        }
    }
};/*Requires geometry.js and graphics2d.js*/
/**
	Represents ingame image object with more complex properties than regular image
	@class
	@constructor
 	@param {Rumi.ESATBox} box - Box used to initialize the position and dimension of the sprite
	@param {HTMLImageElement} image - Image object to create the sprite with
*/
Rumi.Sprite = function(box, image) {
	this.initSociable([
		'BoxChange',
		'ClipBoxChange',
		'ScaleChange',
		'ImageChange',
		'AngleChange',
		'BlendModeChange',
		'AlphaChannelChange'
	].join(' '));
	this._image = image ? image.cloneNode() : document.createElement('img');
	this.box = box ? box.copy() : new Rumi.ESATBox(new Rumi.ESATVector(), this._image.naturalWidth, this._image.naturalHeight);
	this.anchor = new Rumi.ESATVector();
	this._angleRad = 0;
	this._angleDeg = 0;
	this._alpha = 1;
	this._blendMode = Rumi.BlendModes.SOURCE_OVER;
	this.clipBox = new Rumi.ESATBox(
		new Rumi.ESATVector(),
		this._image.naturalWidth,
		this._image.naturalHeight
	);
	this.scale = new Rumi.ESATVector(1, 1);

	this.box.on(
		'PositionChange WidthChange HeightChange',
		this.trigger.bind(this, 'BoxChange')
	);

	this.clipBox.on(
		'PositionChange WidthChange HeightChange',
		this.trigger.bind(this, 'ClipBoxChange')
	);

	this.scale.on(
		'XChange YChange',
		this.trigger.bind(this, 'ScaleChange')
	);

	this.anchor.on(
		'XChange YChange',
		this.trigger.bind(this, 'AnchorChange')
	);
};

Rumi.Sprite.prototype = {
	get image() {
		return this._image;
	},
	set image(newImage) {
		if(this._image != newImage) {
			this._image = newImage;
			this.trigger('ImageChange');
		}
	},
	get angleRad() {
		return this._angleRad;
	},
	set angleRad(newAngleRad) {
		if(this._angleRad != newAngleRad) {
			this._angleRad = newAngleRad;
			this._angleDeg = Rumi.math.toDeg(newAngleRad);
			this.trigger('AngleChange');
		}
	},
	get angleDeg() {
		return this._angleRad;
	},
	set angleDeg(newAngleDeg) {
		if(this._angleRad != newAngleDeg) {
			this._angleDeg = newAngleDeg;
			this._angleRad = Rumi.math.toRad(newAngleDeg);
			this.trigger('AngleChange');
		}
	},
	get alpha() {
		return this._alpha;
	},
	set alpha(newAlpha) {
		if(this._alpha != newAlpha) {
			this._alpha = newAlpha;
			this.trigger('AlphaChannelChange');
		}
	},
	get blendMode() {
		return this._blendMode;
	},
	set blendMode(newBlendMode) {
		if(this._blendMode != newBlendMode) {
			this._blendMode = newBlendMode;
			this.trigger('BlendModeChange');
		}
	},
	/**
		Draws the sprite with all of it's modifiers (angle, alpha, blend) to a given screen.
		This method leaves the graphics object inner state intact !
		@param {Rumi.Graphics2D} graphics - Graphics object on which to draw the sprite
		@returns {Rumi.Sprite} This for chaining
	*/
	draw: function(context) {
		context.save();
		context.translate(this.box.position.x + this.anchor.x, this.box.position.y + this.anchor.y);
		context.rotate(this._angleRad);
		context.translate(-(this.box.position.x + this.anchor.x), -(this.box.position.y + this.anchor.y));
		context.scale(this.scale.x, this.scale.y);
		context.globalAlpha = this._alpha;
		context.globalCompositeOperation = this._blendMode;
		context.drawImage(
			this._image,
			Math.round(this.clipBox.position.x),
			Math.round(this.clipBox.position.y),
			Math.round(this.clipBox.width),
			Math.round(this.clipBox.height),
			Math.round((this.box.position.x / this.scale.x)),
			Math.round(this.box.position.y / this.scale.y),
			Math.round(this.box.width * Math.sign(this.scale.x)),
			Math.round(this.box.height * Math.sign(this.scale.y))
		);
		context.restore();
		return this;
	},
	boundingBox: function() {
		var collisionBox = this.box.copy();
		if(this._angleRad !== 0) {
			collisionBox = collisionBox.toPolygon();
			collisionBox.anchor.copies(this.anchor);
			collisionBox.angleRad = this._angleRad;
		}
		return collisionBox;
	},
	colliding: function(other) {
		return this.boundingBox().colliding(other);
	}
};

Rumi.object.extend(Rumi.Sprite.prototype, Rumi.SociableInterface);
/*Requires geometry.js and graphics2d.js*/
/**
 Represents ingame _image object with more complex properties than regular _image
 @class
 @constructor
 @param {Rumi.ESATBox} box - Box used to initialize the position and dimension of the sprite
 @param {HTMLImageElement} _image - Image object to create the sprite with
 @param {Rumi.ESATVector} frames - Amount of vertical and horizontal frames represented by 2d points with x and y
 */
Rumi.Spritesheet = function(box, image, frames) {
	this.initSociable([
		'AnimationEnd',
		'BoxChange',
		'AnchorChange',
		'ScaleChange',
		'ImageChange',
		'AngleChange',
		'FrameChange',
		'FramesChange',
		'BlendModeChange',
		'AlphaChannelChange'
	].join(' '));
	this.box = box ? box.copy() : new Rumi.ESATBox(new Rumi.ESATVector(), 100, 100);
	this.anchor = new Rumi.ESATVector();
	this.frames = frames ? frames.copy() : new Rumi.ESATVector(1, 1);
	this._animations = {};
	this._animationFPS = 60;
	this._currentAnimationName = null;
	this._currentAnimationIndex = 0;
	this._lastFrameChangeTime = 0;
	this._image = image ? image.cloneNode() : document.createElement('img');
	this._angleRad = 0;
	this._angleDeg = 0;
	this._frame = 0;
	this._alpha = 1;
	this._blendMode = Rumi.BlendModes.SOURCE_OVER;
	this.scale = new Rumi.ESATVector(1, 1);

	this.box.on(
		'PositionChange WidthChange HeightChange',
		this.on.bind(this, 'BoxChange')
	);

	this.frames.on(
		'XChange YChange',
		this.on.bind(this, 'FramesChange')
	);

	this.scale.on(
		'XChange YChange',
		this.on.bind(this, 'ScaleChange')
	);

	this.anchor.on(
		'XChange YChange',
		this.on.bind(this, 'AnchorChange')
	);
};

Rumi.Spritesheet.prototype = {
	get image() {
		return this._image;
	},
	set image(newImage) {
		if(this._image != newImage) {
			this._image = newImage;
			this.trigger('ImageChange');
		}
	},
	get angleRad() {
		return this._angleRad;
	},
	set angleRad(newAngleRad) {
		if(this._angleRad != newAngleRad) {
			this._angleRad = newAngleRad;
			this._angleDeg = Rumi.math.toDeg(newAngleRad);
			this.trigger('AngleChange');
		}
	},
	get angleDeg() {
		return this._angleDeg;
	},
	set angleDeg(newAngleDeg) {
		if(this._angleRad != newAngleDeg) {
			this._angleDeg = newAngleDeg;
			this._angleRad = Rumi.math.toRad(newAngleDeg);
			this.trigger('AngleChange');
		}
	},
	get alpha() {
		return this._alpha;
	},
	set alpha(newAlpha) {
		if(this._alpha != newAlpha) {
			this._alpha = newAlpha;
			this.trigger('AlphaChannelChange');
		}
	},
	get blendMode() {
		return this._blendMode;
	},
	set blendMode(newBlendMode) {
		if(this._blendMode != newBlendMode) {
			this._blendMode = newBlendMode;
			this.trigger('BlendModeChange');
		}
	},
	get frame() {
		return this._frame;
	},
	set frame(newFrame) {
		if(this._frame != newFrame) {
			this._frame = newFrame;
			this.trigger('FrameChange');
		}
	},
	addAnimation: function(name, arrayOfIndexes) {
		if(this._animations[name] === undefined) {
			this._animations[name] = arrayOfIndexes;
		}
	},
	removeAnimation: function(name) {
		if(this._animations[name] !== undefined) {
			delete this._animations[name];
			if(this._currentAnimationName === name) {
				this._currentAnimationName = null;
			}
		}
	},
	playAnimation: function(name, fps) {
		if(this._animations[name] !== undefined) {
			this._currentAnimationName = name;
			if(this._currentAnimationName !== name) this._currentAnimationIndex = 0;
			this._animationFPS = fps;
		}
	},
	stopAnimation: function() {
		this._currentAnimationName = null;
	},
	restartAnimation: function() {
		if(this._currentAnimationName === null) return;
		this._currentAnimationIndex = 0;
		this._frame = this._animations[this._currentAnimationName][this._currentAnimationIndex];
	},
	updateAnimation: function() {
		if(this._currentAnimationName === null) return;
		if((new Date() - this._lastFrameChangeTime) > (1000 / this._animationFPS)) {
			if(this._currentAnimationIndex < this._animations[this._currentAnimationName].length - 1) {
				this._currentAnimationIndex++;
			}
			else {
				this._currentAnimationIndex = 0;
				this.trigger('AnimationEnd');
			}
			this._frame = this._animations[this._currentAnimationName][this._currentAnimationIndex];
			this._lastFrameChangeTime = new Date();
		}
	},
	/**
	 Draws the sprite with all of it's modifiers (angle, alpha, blend) to a given screen.
	 This method leaves the graphics object inner state intact !
	 @param {Rumi.Graphics2D} graphics - Graphics object on which to draw the sprite
	 @returns {Rumi.Sprite} This for chaining
	 */
	draw: function(context) {
		this.updateAnimation();
		var frameWidth = this._image.naturalWidth / this.frames.x,
			frameHeight = this._image.naturalHeight / this.frames.y,
			frameX = parseInt((this._frame) % this.frames.x) * frameWidth,
			frameY = parseInt((this._frame) / this.frames.x) * frameHeight;
		context.save();
		context.translate(this.box.position.x + this.anchor.x, this.box.position.y + this.anchor.y);
		context.rotate(this._angleRad);
		context.translate(-(this.box.position.x + this.anchor.x), -(this.box.position.y + this.anchor.y));
		context.scale(this.scale.x, this.scale.y);
		context.globalAlpha = this.alpha;
		context.globalCompositeOperation = this.blendMode;
		context.drawImage(
			this._image,
			Math.round(frameX),
			Math.round(frameY),
			Math.round(frameWidth),
			Math.round(frameHeight),
			Math.round((this.box.position.x / this.scale.x)),
			Math.round(this.box.position.y / this.scale.y),
			Math.round(this.box.width * Math.sign(this.scale.x)),
			Math.round(this.box.height * Math.sign(this.scale.y))
		);
		context.restore();
		return this;
	},
	boundingBox: function() {
		var collisionBox = new Rumi.ESATBox();
		if(this._angleRad === 0) {
			return collisionBox;
		}
		else {
			var poly = this.box.toPolygon();
			poly.anchor.copies(this.anchor);
			poly.angleDeg = this._angleDeg;
			return poly;
		}
	},
	colliding: function(other) {
		return this.boundingBox().colliding(other);
	}
};

Rumi.object.extend(Rumi.Spritesheet.prototype, Rumi.SociableInterface);
Rumi.Text = function(position, font, string) {
	console.log(this);
	this.initSociable([
		'BoxChange',
		'StringChange',
		'SizeChange',
		'FontChange',
		'FillChange',
		'StrokeChange',
		'AngleChange'
	].join(' '));
	this.box = new Rumi.ESATBox((position ? position.copy() : new Rumi.ESATVector()), 0, 0);
	this._string = string;
	this.font = font ? font.copy() : new Rumi.Font();
	this._context = document.createElement('canvas').getContext('2d');
	this._angleRad = 0;
	this._angleDeg = 0;
	this.fill = new Rumi.Fill();
	this.stroke = new Rumi.Stroke();
	this.fill.on('*', this.trigger.bind(this, 'FillChange'));
	this.stroke.on('*', this.trigger.bind(this, 'StrokeChange'));
	this.box.on('PositionChange onWidthChange onHeightChange', this.trigger.bind(this, 'BoxChange'));
	this.font.on('*', this.trigger.bind(this, 'FontChange'));

	this._recalculateBoundingBox();
};

Rumi.Text.prototype = {
	get angleRad() {
		return this._angleRad;
	},
	set angleRad(newAngleRad) {
		if(this._angleRad != newAngleRad) {
			this._angleRad = newAngleRad;
			this._angleDeg = Rumi.math.toDeg(newAngleRad);
		}
	},
	get angleDeg() {
		return this._angleDeg;
	},
	set angleDeg(newAngleDeg) {
		if(this._angleDeg != newAngleDeg) {
			this._angleDeg = newAngleDeg;
			this._angleRad = Rumi.math.toRad(newAngleDeg);
		}
	},
	get string() {
		return this._string;
	},
	set string(newString) {
		if(this._string != newString) {
			this._string = newString;
			this.trigger('StringChange');
		}
	},
	_recalculateBoundingBox: function() {
		this._context.font = this.font.computed;
		this.box.width = this._context.measureText(this._string).width;
		this.box.height = this._context.measureText('M').width * 1.5;
	},
	draw: function(context) {
		context.save();
			context.translate(this.box.position.x, this.box.position.y);
			context.rotate(this._angleRad);
			context.translate(-this.box.position.x, -this.box.position.y);
			context.font = this.font.computed;
			context.textBaseline = 'top';
			this.fill.applyRaw(context);
			context.fillText(this._string, this.box.position.x, this.box.position.y);
			this.stroke.applyRaw(context);
			context.strokeText(this._string, this.box.position.x, this.box.position.y);
		context.restore();
		return this;
	},
	get boundingBox() {
		var collisionBox = this.box.copy();
		if(this._angleRad !== 0) {
			collisionBox = collisionBox.toPolygon();
			collisionBox.angleRad = this._angleRad;
		}
		return collisionBox;
	},
	colliding: function(other) {
		return this.boundingBox.colliding(other);
	}
};

Rumi.object.extend(Rumi.Text.prototype, Rumi.SociableInterface);
/**
	Represents computer keyboard
	@constructor
	@class
*/
Rumi.Keyboard = function() {
	this.eventsManager = new Rumi.EventsManager([
		'onKeyDown',
		'onKeyUp'
	].join(' '));
	var _that = this,
		_pressedKeyCodes = [],
		_onKeyDown = function(e) {
			var code = e.which;
			if(_pressedKeyCodes.join(' ').indexOf(code) === -1) {
				_pressedKeyCodes.push(code);
				_that.eventsManager.triggerEvent('onKeyDown');
			}
		},
		_onKeyUp = function(e) {
			var code = e.which;
			if(_pressedKeyCodes.join(' ').indexOf(code) >= 0) {
				for(var i = _pressedKeyCodes.length - 1; i > -1; --i) {
					if(_pressedKeyCodes[i] == code) {
						_pressedKeyCodes.splice(i , 1);
						_that.eventsManager.triggerEvent('onKeyUp');
						break;
					}
				}
			}
		},
		_destroy = function() {
			window.removeEventListener('keydown', _onKeyDown);
			window.removeEventListener('keyup', _onKeyUp);
		};

	Object.defineProperties(this, {
		pressedKeyCodesString: {
			get: function() {
				return _pressedKeyCodes.join(' ');
			}
		},
		destroy: {
			get: function() {
				return _destroy;
			}
		}
	});

	window.addEventListener('keydown', _onKeyDown);
	window.addEventListener('keyup', _onKeyUp);
};

Rumi.Keyboard.prototype.sequenceMatch = function(arrayOfKeys) {
	return (this.pressedKeyCodesString.indexOf(arrayOfKeys.join(' ')) > -1);
};

Rumi.Keyboard.prototype.combinationMatch = function(arrayOfKeys) {
	for(var i = 0, len = arrayOfKeys.length; i < len; ++i) {
		if(this.pressedKeyCodesString.indexOf(arrayOfKeys[i]) === -1) {
			return false;
		}
	}
	return true;
};

/*Table created using this info http://www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes*/
Rumi.Keyboard.prototype.Keys = {
	BACKSPACE: 8,
	TAB: 9,
	ENTER: 13,
	SHIFT: 16,
	CTRL: 17,
	ALT: 18,
	PAUSE_BREAK: 19,
	CAPS_LOCK: 20,
	ESCAPE: 27,
	SPACE: 32,
	PAGE_UP: 33,
	PAGE_DOWN: 34,
	END: 35,
	HOME: 36,
	LEFT: 37,
	UP: 38,
	RIGHT: 39,
	DOWN: 40,
	INSERT: 45,
	DELETE: 46,
	ZERO: 48,
	ONE: 49,
	TWO: 50,
	THREE: 51,
	FOUR: 52,
	FIVE: 53,
	SIX: 54,
	SEVEN: 55,
	EIGHT: 56,
	NINE: 57,
	A: 65,
	B: 66,
	C: 67,
	D: 68,
	E: 69,
	F: 70,
	G: 71,
	H: 72,
	I: 73,
	J: 74,
	K: 75,
	L: 76,
	M: 77,
	N: 78,
	O: 79,
	P: 80,
	Q: 81,
	R: 82,
	S: 83,
	T: 84,
	U: 85,
	V: 86,
	W: 87,
	X: 88,
	Y: 89,
	Z: 90,
	LEFT_META: 91,
	RIGHT_META: 92,
	SELECT: 93,
	NUM_0: 96,
	NUM_1: 97,
	NUM_2: 98,
	NUM_3: 99,
	NUM_4: 100,
	NUM_5: 101,
	NUM_6: 102,
	NUM_7: 103,
	NUM_8: 104,
	NUM_9: 105,
	ASTERISK: 106,
	PLUS: 107,
	MINUS: 109,
	F1: 112,
	F2: 113,
	F3: 114,
	F4: 115,
	F5: 116,
	F6: 117,
	F7: 118,
	F8: 119,
	F9: 120,
	F10: 121,
	F11: 122,
	NUM_LOCK: 144,
	SCROLL_LOCK: 145,
	SEMICOLON: 186,
	EQUAL: 187,
	COMMA: 188,
	DASH: 189,
	PERIOD: 190,
	SLASH: 191,
	APOSTROPHE: 192,
	OPENING_SQUARE_BRACKET: 219,
	CLOSING_SQUARE_BRACKET: 221
};
/**
	Represents the computer mouse over an HTML element
	@class
	@constructor
	@param {HTMLElement} element - Element this.on which to register and process mouse events
*/
Rumi.Mouse = function(HTMLElement) {
	this.eventsManager = new Rumi.EventsManager([
		'onDoubleClickIntervalChange',
		'onElementChange',
		'onMove',
		'onLeftDown',
		'onLeftUp',
		'onLeftClick',
		'onLeftDoubleClick',
		'onScrollDown',
		'onScrollUp',
		'onScrollClick',
		'onScrollDoubleClick',
		'onRightDown',
		'onRightUp',
		'onRightClick',
		'onRightDoubleClick'
	].join(' '));
	this.pagePos = new Rumi.ESATVector();
	this.clientPos = new Rumi.ESATVector();
	this.screenPos = new Rumi.ESATVector();
	this.movement = new Rumi.ESATVector();
	this.offset = new Rumi.ESATVector();

	var _doubleClickMSInterval = 500,
		_leftDown = false,
		_lastLeftClickTime = 0,
		_scrollDown = false,
		_lastScrollClickTime = 0,
		_rightDown = false,
		_lastRightClickTime = 0,
		_element = HTMLElement,
		_that = this,
		_updatePointerCoordinates = function(mouseEvent) {
			if(!_on) return;
			_that.pagePos.x = mouseEvent.pageX;
			_that.pagePos.y = mouseEvent.pageY;
			_that.clientPos.x = mouseEvent.clientX;
			_that.clientPos.y = mouseEvent.clientY;
			_that.screenPos.x = mouseEvent.screenX;
			_that.screenPos.y = mouseEvent.screenY;

			_that.offset.x = mouseEvent.offsetX;
			_that.offset.y = mouseEvent.offsetY;
			_that.movement.x = mouseEvent.movementX;
			_that.movement.y = mouseEvent.movementY;
		},
		_onMouseDown = function (e) {
			if(!_on) return;
			if (_preventDefault) e.preventDefault();
			switch (e.button) {
				case 0:
					_leftDown = true;
					_that.eventsManager.triggerEvent('onLeftDown');
					break;
				case 1:
					_scrollDown = true;
					_that.eventsManager.triggerEvent('onScrollDown');
					break;
				case 2:
					_rightDown = true;
					_that.eventsManager.triggerEvent('onRightDown');
					break;
			}
		},
		_onMouseUp = function (e) {
			if(!_on) return;
			if (_preventDefault) e.preventDefault();
			switch (e.button) {
				case 0:
					_leftDown = false;
					_that.eventsManager.triggerEvent('onLeftUp');
					_that.eventsManager.triggerEvent('onLeftClick');
					if((new Date() - _lastLeftClickTime) <= _doubleClickMSInterval) {
						_that.eventsManager.triggerEvent('onLeftDoubleClick');
					}
					_lastLeftClickTime = new Date();
					break;
				case 1:
					_scrollDown = false;
					_that.eventsManager.triggerEvent('onScrollUp');
					_that.eventsManager.triggerEvent('onScrollClick');
					if((new Date() - _lastScrollClickTime) <= _doubleClickMSInterval) {
						_that.eventsManager.triggerEvent('onScrollDoubleClick');
					}
					_lastScrollClickTime = new Date();
					break;
				case 2:
					_rightDown = false;
					_that.eventsManager.triggerEvent('onRightUp');
					_that.eventsManager.triggerEvent('onRightClick');
					if((new Date() - _lastRightClickTime) <= _doubleClickMSInterval) {
						_that.eventsManager.triggerEvent('onRightDoubleClick');
					}
					_lastRightClickTime = new Date();
					break;
			}
		},
		_preventDefault = true,
		_onMouseMove = function (e) {
			if(!_on) return;
			if (_preventDefault) e.preventDefault();
			_updatePointerCoordinates(e);
			_that.eventsManager.triggerEvent('onMove');
		},
		_routeEventsToElement = function(element) {
			element.addEventListener('mousemove', _onMouseMove);
			element.addEventListener('mousedown', _onMouseDown);
			element.addEventListener('mouseup', _onMouseUp);
		},
		_disconnectEventsFromElement = function(element) {
			element.removeEventListener('mousemove', _onMouseMove);
			element.removeEventListener('mousedown', _onMouseDown);
			element.removeEventListener('mouseup', _onMouseUp);
		},
		_destroy = function() {
			_that.eventsManager = null;
			_disconnectEventsFromElement(_element);
			for(var p in _that) {
				delete _that[p];
			}
			_element = null;
		},
		_on = true;

	_routeEventsToElement(_element);

	Object.defineProperties(this, {
		element: {
			get: function() {
				return _element;
			},
			set: function(newElement) {
				if(_element != newElement) {
					_disconnectEventsFromElement(_element);
					_element = newElement;
					_routeEventsToElement(_element);
					_that.eventsManager.triggerEvent('onElementChange');
				}
			}
		},
		doubleClickInterval: {
			get: function () {
				return _doubleClickMSInterval;
			},
			set: function(newInterval) {
				if(_doubleClickMSInterval != newInterval) {
					_doubleClickMSInterval = newInterval;
					_that.eventsManager.triggerEvent('onDoubleClickIntervalChange');
				}
			}
		},
		preventDefault: {
			get: function() {
				return _preventDefault;
			},
			set: function(newPrevent) {
				if(_preventDefault != newPrevent) {
					_preventDefault = newPrevent;
					_that.eventsManager.triggerEvent('onPreventDefaultChange');
				}
			}
		},
		on: {
			get: function() {
				return _on;
			},
			set: function(newOn) {
				if(_on != newOn) {
					_on = newOn;
					_that.eventsManager.triggerEvent('onOnChanged');
				}
			}
		},
		destroy: {
			get: function() {
				return _destroy;
			}
		}
	});
};
/**
 * Created by vili on 3/21/2016.
 */
Rumi.Hand = function(element) {
    this.eventsManager = new Rumi.EventsManager([
        'onTouchStart',
        'onTouchEnd',
        'onTouchCancel',
        'onTouchMove'
    ].join(' '));
    this.touches = [];
    var _element = element,
        _that = this,
        _onTouchStart = function(e) {
            for(var i = 0, len = e.changedTouches.length; i < len; ++i) {
                var vector = new Rumi.ESATVector(
                    e.changedTouches[i].pageX,
                    e.changedTouches[i].pageY
                );
                vector.id = e.changedTouches[i].identifier;
                _that.touches.push(vector);
            }
            _that.eventsManager.triggerEvent('onTouchStart');
        },
        _onTouchEnd = function(e) {
            for(var i = 0, len = e.changedTouches.length; i < len; ++i) {
                for (var i2 = _that.touches.length - 1; i2 > -1; --i2) {
                    if(_that.touches[i2].id === e.changedTouches[i].identifier) {
                        _that.touches.splice(i2, 1);
                    }
                }
            }
            _that.eventsManager.triggerEvent('onTouchEnd');
        },
        _onTouchCancel = function(e) {
            for(var i = 0, len = e.changedTouches.length; i < len; ++i) {
                for (var i2 = _that.touches.length - 1; i2 > -1; --i2) {
                    if(_that.touches[i2].id === e.changedTouches[i].identifier) {
                        _that.touches.splice(i2, 1);
                    }
                }
            }
            _that.eventsManager.triggerEvent('onTouchCancel');
        },
        _onTouchMove = function(e) {
            for(var i = 0, len = e.changedTouches.length; i < len; ++i) {
                for(var i2 = 0, len2 = _that.touches.length; i2 < len2; ++i2) {
                    if(_that.touches[i2].id === e.changedTouches[i].identifier) {
                        _that.touches[i2].x = e.changedTouches[i].pageX;
                        _that.touches[i2].y = e.changedTouches[i].pageY;
                    }
                }
            }
            _that.eventsManager.triggerEvent('onTouchMove');
        },
        _destroy = function() {
            _element.removeEventListener('touchstart', _onTouchStart);
            _element.removeEventListener('touchend', _onTouchEnd);
            _element.removeEventListener('touchcancel', _onTouchCancel);
            _element.removeEventListener('touchmove', _onTouchMove);
            _that.touches = null;
        };

    _element.addEventListener('touchstart', _onTouchStart);
    _element.addEventListener('touchend', _onTouchEnd);
    _element.addEventListener('touchcancel', _onTouchCancel);
    _element.addEventListener('touchmove', _onTouchMove);

    Object.defineProperties(this, {
        destroy: {
            get: function() {
                return _destroy;
            }
        }
    });
};

Rumi.Hand.prototype.touching = function(other) {
    for(var i = 0, len = this.touches.length; i < len; ++i) {
        if(this.touches[i].colliding(other)) {
            return true;
        }
    }
    return false;
};
/**
 * Created by vili on 3/23/2016.
 */
Rumi.Timer = function(interval) {
    this.eventsManager = new Rumi.EventsManager([
        'onStart',
        'onTick',
        'onStop'
    ].join(' '));
    this._interval = interval;
    this._running = false;
    this._startedAt = 0;
};

Rumi.Timer.prototype.start = function() {
    if(!this._running) {
        this._startedAt = new Date();
        this._running = true;
        this.eventsManager.triggerEvent('onStart');
    }
};

Rumi.Timer.prototype.stop = function() {
    if(this._running) {
        this._startedAt = 0;
        this._running = false;
        this.eventsManager.triggerEvent('onStop');
    }
};

Rumi.Timer.prototype.running = function() {
    return this._running;
};

Rumi.Timer.prototype.update = function() {
    if(this._running && (new Date() - this._startedAt) >= this._interval) {
        this._startedAt = new Date();
        this.eventsManager.triggerEvent('onTick');
    }
};
/**
 * Created by vili on 3/30/2016.
 */
Rumi.TimerManager = function() {
    this._timers = [];
};

Rumi.TimerManager.prototype.add = function(timer) {
    Rumi.array.addIfNotPresent(this._timers, timer);
};

Rumi.TimerManager.prototype.delete = function(timer) {
    Rumi.array.removeByVal(this._timers, timer);
};

Rumi.TimerManager.prototype.update = function() {
    for(var i = 0, len = this._timers.length; i < len; ++i) {
        this._timers[i].update();
    }
};/** Measures frames per second (FPS)
	@class
	@constructor
*/
Rumi.FPSMeter = function() {
	var startTime = 0,
		frameNumber = 0;
	this.getFPS = function() {
		frameNumber++;
		var d = new Date().getTime(),
			currentTime = ( d - startTime ) / 1000,
			result = Math.floor( ( frameNumber / currentTime ) );

		if( currentTime > 1 ){
			startTime = new Date().getTime();
			frameNumber = 0;
		}
		return result;
	};
};/**
 * Created by vili on 3/27/2016.
 */
Rumi.QuadTree = function(box) {
    this.box = box;
    this.entities = [];
    this.topLeft = null;
    this.bottomLeft = null;
    this.bottomRight = null;
    this.topRight = null;
};

Rumi.QuadTree.prototype.insert = function(entity) {
    if(!this.box.colliding(entity)) return false;
    if(this.entities.length < this.NODE_CAPACITY) {
        this.entities.push(entity);
        return true;
    }
    if(this.topLeft === null) this.subdivide();

    if(
        this.topLeft.insert(entity) ||
        this.bottomLeft.insert(entity) ||
        this.bottomRight.insert(entity) ||
        this.topRight.insert(entity)) {
        return true;
    }
    return false;
};

Rumi.QuadTree.prototype.subdivide = function() {
    var halfWidth = this.box.w / 2,
        halfHeight = this.box.h / 2;
    this.topLeft = new Rumi.QuadTree(
        new Rumi.ESATBox(
            new Rumi.ESATVector(
                this.box.pos.x,
                this.box.pos.y
            ),
            halfWidth,
            halfHeight
        )
    );
    this.bottomLeft = new Rumi.QuadTree(
        new Rumi.ESATBox(
            new Rumi.ESATVector(
                this.box.pos.x,
                this.box.pos.y + halfHeight
            ),
            halfWidth,
            halfHeight
        )
    );
    this.bottomRight = new Rumi.QuadTree(
        new Rumi.ESATBox(
            new Rumi.ESATVector(
                this.box.pos.x + halfWidth,
                this.box.pos.y + halfHeight
            ),
            halfWidth,
            halfHeight
        )
    );
    this.topRight = new Rumi.QuadTree(
        new Rumi.ESATBox(
            new Rumi.ESATVector(
                this.box.pos.x + halfWidth,
                this.box.pos.y
            ),
            halfWidth,
            halfHeight
        )
    );
};

Rumi.QuadTree.prototype.queryBox = function(box) {
    var result = [];
    if(!this.box.colliding(box)) {
        return result;
    }
    for(var i = 0, len = this.entities.length; i < len; ++i) {
        if(box.colliding(this.entities[i])) {
            result.push(this.entities[i]);
        }
    }

    if(this.topLeft === null) return result;
    result = result.concat(this.topLeft.queryBox(box));
    result = result.concat(this.bottomLeft.queryBox(box));
    result = result.concat(this.bottomRight.queryBox(box));
    result = result.concat(this.topRight.queryBox(box));
    return result;
};

Rumi.QuadTree.prototype.NODE_CAPACITY = 4;/**
 * Created by vili on 3/29/2016.
 */
Rumi.Tween = function(object, propValues, duration, onlyIntegers) {
    this.eventsManager = new Rumi.EventsManager([
        'onUpdate',
        'onEnd'
    ].join(' '));
    this.onlyIntegers = onlyIntegers;
    this.object = object;
    this.propValues = propValues;
    this.duration = duration;
    for(var p in propValues) {
        /*If start is ommited it defaults to the current value of the object property*/
        if(this.propValues[p].start === undefined) this.propValues[p].start = this.object[p];
        this.propValues[p].delta = (this.propValues[p].end - this.propValues[p].start) / this.duration;
    }
    this.started = new Date();
    this.ended = false;
};

Rumi.Tween.prototype.update = function() {
    if(new Date() - this.started > this.duration) {
        if(!this.ended) {
            this.ended = true;
            /*Due to time inaccuracy force the values to their ends*/
            for(var p in this.propValues) {
                this.object[p] = this.propValues[p].end;
            }
            this.eventsManager.triggerEvent('onEnd');
        }
        return;
    }
    for(var p in this.propValues) {
        var value = this.propValues[p].start + (this.propValues[p].delta * (new Date() - this.started));
        this.object[p] = this.onlyIntegers ? parseInt(value) : value;
    }
    this.eventsManager.triggerEvent('onUpdate');
};
/**
 * Created by vili on 3/30/2016.
 */
Rumi.TweenManager = function() {
    this._tweens = [];
};

Rumi.TweenManager.prototype.add = function(tween) {
    Rumi.array.addIfNotPresent(this._tweens, tween);
};

Rumi.TweenManager.prototype.delete = function(tween) {
    Rumi.array.removeByVal(this._tweens, tween);
};

Rumi.TweenManager.prototype.update = function() {
    for(var i = this._tweens.length - 1; i > -1; --i) {
        if(this._tweens[i].ended) {
            this._tweens.splice(i, 1);
        }
        else {
            this._tweens[i].update();
        }
    }
};/**
 * Created by vili on 3/23/2016.
 */
Rumi.Game = function(box, onPreload, onCreate, onUpdate) {
    this.eventsManager = new Rumi.EventsManager([
        'onPreload',
        'onCreate',
        'onUpdate',
        'onRender'
    ]);

    if(onPreload) this.eventsManager.attachEventListener('onPreload', onPreload.bind(this));
    if(onCreate) this.eventsManager.attachEventListener('onCreate', onCreate.bind(this));
    if(onUpdate) this.eventsManager.attachEventListener('onUpdate', onUpdate.bind(this));

    this.graphics = new Rumi.Graphics2D(box);

    this.loadingManager = new Rumi.LoadingManager();

    this._timerManager = new Rumi.TimerManager();

    this._tweenManager = new Rumi.TweenManager();

    var _that = this;

    this.input = {
        hand: new Rumi.Hand(_that.graphics.context.canvas),
        mouse: new Rumi.Mouse(_that.graphics.context.canvas),
        keyboard: new Rumi.Keyboard()
    };

    this._fpsm = new Rumi.FPSMeter();

    this.fps = 0;

    this.add = {
        vector: function(x, y) {
            var v = _that.create.vector(x, y);
            _that._renderables.push(v);
            return v;
        },
        circle: function(pos, radius) {
            var c = _that.create.circle(pos, radius);
            _that._renderables.push(c);
            return c;
        },
        box: function(pos, width, height) {
            var box = _that.create.box(pos, width, height);
            _that._renderables.push(box);
            return box;
        },
        polygon: function(pos, points) {
            var poly = _that.create.polygon(pos, points);
            _that._renderables.push(poly);
            return poly;
        },
        sprite: function(box, image) {
            var sprite = _that.create.sprite(box, image);
            _that._renderables.push(sprite);
            return sprite;
        },
        spritesheet: function(box, image, frames) {
            var sheet = _that.create.spritesheet(box, image, frames);
            _that._renderables.push(sheet);
            return sheet;
        },
        text: function(pos, string) {
            var text = _that.create.text(pos, string);
            _that._renderables.push(text);
            return text;
        },
        timer: function(interval) {
            var t = _that.create.timer(interval);
            _that._timerManager.add(t);
            return t;
        },
        tween: function(object, propValues, duration, onlyIntegers) {
            var t = _that.create.tween(object, propValues, duration, onlyIntegers);
            _that._tweenManager.add(t);
            return t;
        }
    };

    this.create = {
        vector: function(x, y) {
            return new Rumi.ESATVector(x, y);
        },
        circle: function(pos, radius) {
            return new Rumi.ESATCircle(pos, radius);
        },
        box: function(pos, width, height) {
            return new Rumi.ESATBox(pos, width, height);
        },
        polygon: function(pos, points) {
            return new Rumi.ESATPolygon(pos, points);
        },
        sprite: function(box, image) {
            return new Rumi.Sprite(box, image);
        },
        spritesheet: function(box, image, frames) {
            return new Rumi.Spritesheet(box, image, frames);
        },
        text: function(pos, string) {
            return new Rumi.Text(pos, string);
        },
        timer: function(interval) {
            return new Rumi.Timer(interval);
        },
        tween: function(object, propValues, duration, onlyIntegers) {
            return new Rumi.Tween(object, propValues, duration, onlyIntegers);
        }
    };

    this.attach = function(obj) {
        if(this._isRenderable(obj)) {
            Rumi.array.addIfNotPresent(this._renderables, obj);
        }
        else if(obj instanceof Rumi.Timer) {
            this._timerManager.add(obj);
        }
        else if(obj instanceof Rumi.Tween) {
            this._tweenManager.add(obj);
        }
    };

    this.detach = function(obj) {
        if(this._isRenderable(obj)) {
            Rumi.array.removeByVal(this._renderables, obj);
        }
        else if(obj instanceof Rumi.Timer) {
            this._timerManager.delete(obj);
        }
        else if(obj instanceof Rumi.Tween) {
            this._tweenManager.delete(obj);
        }
    };
    this._renderables = [];

    this._initialSequenceTimeoutID = setTimeout(function() {
        this.eventsManager.triggerEvent('onPreload');
        this.loadingManager.load(function(assets) {
            if(assets !== undefined) {
                this.assets = assets; 
            }
            this.eventsManager.triggerEvent('onCreate');
            this.eventsManager.triggerEvent('onUpdate');
            delete this._initialSequenceTimeoutID;
            this.update();
        }.bind(this));
    }.bind(this), 1);
};

Rumi.Game.prototype._isRenderable = function(obj) {
    for(var i = 0, len = this._renderableTypes.length; i < len; i++) {
        if(obj instanceof this._renderableTypes[i]) return true;
    }
    return false;
};

Rumi.Game.prototype._renderableTypes = [
    Rumi.ESATVector,
    Rumi.ESATCircle,
    Rumi.ESATBox,
    Rumi.ESATPolygon,
    Rumi.Sprite,
    Rumi.Spritesheet,
    Rumi.Text
];

Rumi.Game.prototype.update = function() {
    this.fps = this._fpsm.getFPS();
    this.eventsManager.triggerEvent('onUpdate');
    this.graphics.clear();
    this._renderables.forEach(function(e) {
        e.draw(this.graphics.context);
    }, this);
    this._timerManager.update();
    this._tweenManager.update();
    this.eventsManager.triggerEvent('onRender');
    this.nextAnimationFrameID = requestAnimationFrame(this.update.bind(this));
};

Rumi.Game.prototype.nextAnimationFrameID = null;

Rumi.Game.prototype.destroy = function() {
    if(this._initialSequenceTimeoutID) {
        clearTimeout(this._initialSequenceTimeoutID);
    }
    cancelAnimationFrame(this.nextAnimationFrameID);
    this.graphics.destroy();
    this.input.mouse.destroy();
    this.input.hand.destroy();
    for(var p in this) {
        delete this[p];
    }
};