var game = new Rumi.Game(
	new Rumi.ESATBox(
		new Rumi.ESATVector(0, 0),
		window.innerWidth,
		window.innerHeight
	),
	preload,
	create,
	update
);

function preload() {
	game.loadingManager.images({
		background: 'img/dark_wallpaper.jpg'
	});
};

function create() {
	var back = game.add.sprite(
		game.graphics.box.copy(),
		game.assets.background
	);
	var rgba = new Rumi.RGBA().fromColorName('blue');
	back.anchor.x = back.box.w / 2;
	back.anchor.y = back.box.h / 2;
	game.add.tween(
		back,
		{
			angle: {
				end: Math.PI * 100
			}
		},
		10000
	);
};

function update() {
	
};