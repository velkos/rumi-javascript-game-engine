/**
 * Created by vili on 3/30/2016.
 */
var game,
    textArray = [],
    text;
window.onload = function() {
    var game = new Rumi.Game(null, preload, create, update);
    //console.log(game);
    function preload() {
        game.loadingManager.images({
            hero: 'img/hero.png'
        });
    }

    function create() {
        text = game.add.spritesheet(null, game.assets.hero, game.create.vector(4, 4));
        text.addAnimation('left', [0, 1, 2, 3]);
        text.playAnimation('left', 10);
        setTimeout(function() {
            text.stopAnimation();
        }, 1000);
    }

    function update() {
    }
};