/*math.js tests*/
/*TODO: Write more detailed assertions !*/
QUnit.module('Rumi', {}, function() {
    QUnit.module('math',
        {},
        function() {
            QUnit.test('distance works', function(assert) {
                assert.ok(Rumi.math.distance(0, 0, 50, 0) == 50, 'True');
            });

            QUnit.test('angle works', function(assert) {
                assert.ok(Rumi.math.angle(0, 0, 50, 0) == 0, 'True');
            });

            QUnit.test('toDeg works', function(assert) {
                assert.ok(Rumi.math.toDeg(90 * Math.PI/180) == 90, 'True');
            });

            QUnit.test('toRad works', function(assert) {
                assert.ok(Rumi.math.toRad(90) == (90 * Math.PI/180), 'True');
            });

            QUnit.test('InRange works', function(assert) {
                assert.ok(
                    Rumi.math.inRange(5, 0, 1) === false &&
                    Rumi.math.inRange(5, 0, 10) === true,
                    'True'
                );
            });

            QUnit.test('isNumber works', function(assert) {
                assert.ok(Rumi.math.isNumber('I am a string') === false, 'String is not a number');
                assert.ok(Rumi.math.isNumber(NaN) === false, 'NaN is not a number');
                assert.ok(Rumi.math.isNumber(Infinity) === false, 'Infinity is not a number');
                assert.ok(Rumi.math.isNumber(5) === true, 'A number is a number');
            });

            QUnit.test('randomInt works', function(assert) {
                var min = 0,
                    max = 100,
                    randomInt = Rumi.math.randomInt(min, max);
                assert.ok(parseInt(randomInt) === randomInt, 'Result is an integer');
                assert.ok(randomInt >= 0 && randomInt <= 100, 'Result is in range');
            });

            QUnit.test('randomFloat works', function(assert) {
                var min = 0,
                    max = 100,
                    randomFloat = Rumi.math.randomFloat(min, max);
                assert.ok(parseFloat(randomFloat) === randomFloat, 'Result is a float');
                assert.ok(randomFloat >= min && randomFloat <= max, 'Result is in range');
            });

            QUnit.test('clamp works', function(assert) {
                var number = 321.92312,
                    min = 0,
                    max = 256;
                assert.ok(Rumi.math.clamp(number, min, max) === max, 'True');
            });

            QUnit.test('percent works', function(assert) {
                var value = 100;
                assert.ok(Rumi.math.percent(value, 10) === 10, 'True');
            });
        }
    );

    QUnit.module('Event',
        {
            beforeEach: function() {
                this.event = new Rumi.Event('event');
            }
        },
        function() {
            QUnit.test('Name is immutable', function(assert) {
                var name = this.event.name;
                this.event.name = 'test';
                assert.ok(this.event.name === name, 'True');
            });

            QUnit.test('attachListener works', function(assert) {
                var listener = function() {};
                this.event.attachListener(listener);
                assert.ok(this.event.handlers[0] === listener, 'True');
            });

            QUnit.test('detachListener works', function(assert) {
                var listener = function() {};
                this.event.attachListener(listener);
                /*Make sure it was initially attached*/
                if(this.event.handlers[0] === listener) {
                    this.event.detachListener(listener);
                    /*If it was detached it passes the test*/
                    assert.ok(this.event.handlers[0] !== listener, 'True');
                }
            });

            QUnit.test('trigger works', function(assert) {
                var listener = function() {
                    assert.ok(true, 'True');
                };
                this.event.attachListener(listener);
                this.event.trigger();
            });

            QUnit.test('trigger calls all listeners in the order they were attached', function(assert) {
                var vals = [];
                /*If the two handlers get called in different order the assertion will fail because the
                 * values pushed into vals are then concatenated and compared with constant expected result!*/
                var l1 = function() {
                    vals.push('1');
                };
                var l2 = function() {
                    vals.push('2');
                };
                this.event.attachListener(l1);
                this.event.attachListener(l2);
                this.event.trigger();
                assert.ok(vals.join(' ') === '1 2', 'True');
            });
        }
    );

    QUnit.module('EventsManager',
        {
            beforeEach: function() {
                this.eventsManager = new Rumi.EventsManager(['a', 'b']);
            }
        },
        function () {
            QUnit.test('Default constructor works', function(assert) {
                var noException = true;
                try {
                    this.eventsManager = new Rumi.EventsManager();
                }
                catch(ex) {
                    noException = false;
                }
                if(noException) {
                    assert.ok(true, 'Default constructor executed without throwing exceptions');
                }
                else {
                    assert.ok(false, 'Default constructor threw an exception')
                }
            });

            QUnit.test('Constructor works', function(assert) {
                assert.ok(
                    this.eventsManager.events['a'] !== undefined &&
                    this.eventsManager.events['b'] !== undefined,
                    'Events registered'
                );
                assert.ok(
                    this.eventsManager.events['a'] instanceof Rumi.Event &&
                    this.eventsManager.events['b'] instanceof Rumi.Event,
                    'Events type ok'
                );
            });

            QUnit.test('registerEvent works', function(assert) {
                if(this.eventsManager.events['test'] === undefined) {
                    this.eventsManager.registerEvent('test');
                    assert.ok(this.eventsManager.events['test'] !== undefined, 'Event registered');
                    assert.ok(this.eventsManager.events['test'] instanceof Rumi.Event, 'Event type ok');
                }
            });

            QUnit.test('unregisterEvent works', function(assert) {
                if(this.eventsManager.events['a'] !== undefined) {
                    this.eventsManager.unregisterEvent('a');
                    assert.ok(this.eventsManager.events['a'] === undefined, 'True');
                }
            });

            QUnit.test('attachEventListener works for single event', function(assert) {
                if(this.eventsManager.events['a'] !== undefined) {
                    var listener = function(){};
                    this.eventsManager.attachEventListener('a', listener);
                    assert.ok(this.eventsManager.events['a'].handlers[0] === listener, 'True');
                }
            });
            /*TODO: Add array operations like unique, sortAsc, sortDesc, filter and so on!*/
            QUnit.test('attachEventListener works for multiple events', function(assert) {
                var em = this.eventsManager,
                    ev = em.events;
                if(ev['a'] !== undefined && ev['b'] !== undefined) {
                    var listener = function(){};
                    em.attachEventListener('a b b a', listener);
                    assert.ok(ev['a'].handlers[0] === listener && ev['b'].handlers[0] === listener, 'True');
                }
            });

            QUnit.test('detachEventListener works for single event', function(assert) {
                var aHandlerFired = false,
                    aHandler = function() {
                        aHandlerFired = true;
                    };
                this.eventsManager.attachEventListener('a', aHandler);
                this.eventsManager.detachEventListener('a', aHandler);
                this.eventsManager.triggerEvent('a');
                assert.ok(!aHandlerFired, 'True');
            });

            QUnit.test('detachEventListener works for multiple events', function(assert) {
                var handlerFired = false,
                    handler = function() {handlerFired = true;};
                this.eventsManager.attachEventListener('a b', handler);
                this.eventsManager.detachEventListener('a b', handler);
                this.eventsManager.triggerEvent('a b');
                assert.ok(!handlerFired, 'True');
            });
        }
    );

    QUnit.module('Graphics2D',
        {
            beforeEach: function() {
                this.graphics = new Rumi.Graphics2D();
            }
        },
        function() {
            QUnit.test('Default constructor works', function(assert) {
                assert.ok(this.graphics.box instanceof Rumi.ESATBox, 'box type ok');
                assert.ok(this.graphics.box.pos.x === 0 && this.graphics.box.pos.y === 0, 'box.pos initialized ok');
                assert.ok(this.graphics.box.w === 800 && this.graphics.box.h === 600, 'box.w and box.h initialized ok');
                assert.ok(
                    this.graphics.context.canvas.style.marginLeft === this.graphics.box.pos.x + 'px' &&
                    this.graphics.context.canvas.style.marginTop === this.graphics.box.pos.y + 'px',
                    'canvas position update ok'
                );
                assert.ok(
                    this.graphics.context.canvas.style.width === this.graphics.box.w + 'px' &&
                    this.graphics.context.canvas.style.height === this.graphics.box.h + 'px' &&
                    this.graphics.context.canvas.width === this.graphics.box.w &&
                    this.graphics.context.canvas.height === this.graphics.box.h,
                    'canvas size update ok'
                );
                assert.ok(this.graphics.eventsManager instanceof Rumi.EventsManager, 'eventsManager type ok');
                assert.ok(
                    this.graphics.eventsManager.events['onPositionChange'] !== undefined &&
                    this.graphics.eventsManager.events['onPositionChange'] instanceof Rumi.Event,
                    'onPositionChange event registered and triggerable'
                );
                assert.ok(
                    this.graphics.eventsManager.events['onWidthChange'] !== undefined &&
                    this.graphics.eventsManager.events['onWidthChange'] instanceof Rumi.Event,
                    'onWidthChange event registered and triggerable'
                );
                assert.ok(
                    this.graphics.eventsManager.events['onHeightChange'] !== undefined &&
                    this.graphics.eventsManager.events['onHeightChange'] instanceof Rumi.Event,
                    'onHeightChange event registered and triggerable'
                );
            });

            QUnit.test('Constructor works', function(assert) {
                this.graphics = new Rumi.Graphics2D(new Rumi.ESATBox(new Rumi.ESATVector(42, 24), 420, 240));

                assert.ok(this.graphics.box instanceof Rumi.ESATBox, 'box type ok');
                assert.ok(this.graphics.box.pos.x === 42 && this.graphics.box.pos.y === 24, 'box.pos initialized ok');
                assert.ok(this.graphics.box.w === 420 && this.graphics.box.h === 240, 'box.w and box.h initialized ok');
                assert.ok(
                    this.graphics.context.canvas.style.marginLeft === this.graphics.box.pos.x + 'px' &&
                    this.graphics.context.canvas.style.marginTop === this.graphics.box.pos.y + 'px',
                    'canvas position initialized ok'
                );
                assert.ok(
                    this.graphics.context.canvas.style.width === this.graphics.box.w + 'px' &&
                    this.graphics.context.canvas.style.height === this.graphics.box.h + 'px' &&
                    this.graphics.context.canvas.width === this.graphics.box.w &&
                    this.graphics.context.canvas.height === this.graphics.box.h,
                    'canvas size initialized ok'
                );
                assert.ok(this.graphics.eventsManager instanceof Rumi.EventsManager, 'eventsManager type ok');
                assert.ok(
                    this.graphics.eventsManager.events['onPositionChange'] !== undefined &&
                    this.graphics.eventsManager.events['onPositionChange'] instanceof Rumi.Event,
                    'onPositionChange event registered and triggerable'
                );
                assert.ok(
                    this.graphics.eventsManager.events['onWidthChange'] !== undefined &&
                    this.graphics.eventsManager.events['onWidthChange'] instanceof Rumi.Event,
                    'onWidthChange event registered and triggerable'
                );
                assert.ok(
                    this.graphics.eventsManager.events['onHeightChange'] !== undefined &&
                    this.graphics.eventsManager.events['onHeightChange'] instanceof Rumi.Event,
                    'onHeightChange event registered and triggerable'
                );
            });

            QUnit.test('Fires proper events', function(assert) {
                var result = {
                        onPositionChangeFired: false,
                        onWidthChangeFired: false,
                        onHeightChangeFired: false
                    },
                    func = {
                        onPositionChange: function() {
                            result.onPositionChangeFired = true;
                        },
                        onWidthChange: function() {
                            result.onWidthChangeFired = true;
                        },
                        onHeightChange: function() {
                            result.onHeightChangeFired = true;
                        }
                    };

                this.graphics.eventsManager.attachEventListener('onPositionChange', func.onPositionChange);
                this.graphics.eventsManager.attachEventListener('onWidthChange', func.onWidthChange);
                this.graphics.eventsManager.attachEventListener('onHeightChange', func.onHeightChange);

                this.graphics.box.pos.x = 15;
                this.graphics.box.pos.y = 31;
                this.graphics.box.w = 150;
                this.graphics.box.h = 510;

                assert.ok(result.onPositionChangeFired, 'onPositionChange fired');
                assert.ok(result.onWidthChangeFired, 'onWidthChange fired');
                assert.ok(result.onHeightChangeFired, 'onHeightChange fired');
            });

            QUnit.test('_updateCanvasPosition and _updateCanvasSize works', function(assert) {
                this.graphics.box.pos.x = 123;
                this.graphics.box.pos.y = 321;

                assert.ok(
                    this.graphics.context.canvas.style.marginLeft === this.graphics.box.pos.x + 'px' &&
                    this.graphics.context.canvas.style.marginTop === this.graphics.box.pos.y + 'px',
                    '_updateCanvasPosition works'
                );

                assert.ok(
                    this.graphics.context.canvas.style.width === this.graphics.box.w + 'px' &&
                    this.graphics.context.canvas.style.height === this.graphics.box.h + 'px' &&
                    this.graphics.context.canvas.width === this.graphics.box.w &&
                    this.graphics.context.canvas.height === this.graphics.box.h,
                    '_updateCanvasSize works'
                );
            });

            QUnit.test('_saveContentBeforeResize works', function(assert) {
                var works = false;
                if(this.graphics._contentBeforeResize === null) {
                    this.graphics._saveContentBeforeResize();
                    if(this.graphics._contentBeforeResize !== null) {
                        var works = true;
                    }
                }
                assert.ok(works, 'True');
            });

            QUnit.test('_restoreContentBeforeResize works', function(assert) {
                this.graphics._saveContentBeforeResize();
                var works = false;
                if(this.graphics._contentBeforeResize !== null) {
                    this.graphics._restoreContentAfterResize();
                    if(this.graphics._contentBeforeResize === null) {
                        works = true;
                    }
                }
                assert.ok(works, 'True');
            });
        }
    );

    QUnit.module('RGBA',
        {
            beforeEach: function() {
                this.rgba = new Rumi.RGBA();
            }
        },
        function() {
            QUnit.test('Default constructor works', function(assert) {
                assert.ok(this.rgba.red === 0, 'Red component initialized ok');
                assert.ok(this.rgba.green === 0, 'Green component initialized ok');
                assert.ok(this.rgba.blue === 0, 'Blue component initialized ok');
                assert.ok(this.rgba.alpha === 1, 'Alpha channel initialized ok');
                assert.ok(this.rgba.eventsManager instanceof Rumi.EventsManager, 'eventsManager type ok');
                assert.ok(
                    this.rgba.eventsManager.events['onRedComponentChange'] !== undefined &&
                    this.rgba.eventsManager.events['onRedComponentChange'] instanceof Rumi.Event,
                    'onRedComponentChange event registered and triggerable'
                );
                assert.ok(
                    this.rgba.eventsManager.events['onGreenComponentChange'] !== undefined &&
                    this.rgba.eventsManager.events['onGreenComponentChange'] instanceof Rumi.Event,
                    'onGreenComponentChange event registered and triggerable'
                );
                assert.ok(
                    this.rgba.eventsManager.events['onBlueComponentChange'] !== undefined &&
                    this.rgba.eventsManager.events['onBlueComponentChange'] instanceof Rumi.Event,
                    'onBlueComponentChange event registered and triggerable'
                );
                assert.ok(
                    this.rgba.eventsManager.events['onAlphaChannelChange'] !== undefined &&
                    this.rgba.eventsManager.events['onAlphaChannelChange'] instanceof Rumi.Event,
                    'onAlphaChannelChange event registered and triggerable'
                );
            });

            QUnit.test('Constructor works', function(assert) {
                this.rgba = new Rumi.RGBA(1, 2, 3, 1);

                assert.ok(this.rgba.red === 1, 'Red component initialized ok');
                assert.ok(this.rgba.green === 2, 'Green component initialized ok');
                assert.ok(this.rgba.blue === 3, 'Blue component initialized ok');
                assert.ok(this.rgba.alpha === 1, 'Alpha channel initialized ok');
                assert.ok(this.rgba.eventsManager instanceof Rumi.EventsManager, 'eventsManager type ok');
                assert.ok(
                    this.rgba.eventsManager.events['onRedComponentChange'] !== undefined &&
                    this.rgba.eventsManager.events['onRedComponentChange'] instanceof Rumi.Event,
                    'onRedComponentChange event registered and triggerable'
                );
                assert.ok(
                    this.rgba.eventsManager.events['onGreenComponentChange'] !== undefined &&
                    this.rgba.eventsManager.events['onGreenComponentChange'] instanceof Rumi.Event,
                    'onGreenComponentChange event registered and triggerable'
                );
                assert.ok(
                    this.rgba.eventsManager.events['onBlueComponentChange'] !== undefined &&
                    this.rgba.eventsManager.events['onBlueComponentChange'] instanceof Rumi.Event,
                    'onBlueComponentChange event registered and triggerable'
                );
                assert.ok(
                    this.rgba.eventsManager.events['onAlphaChannelChange'] !== undefined &&
                    this.rgba.eventsManager.events['onAlphaChannelChange'] instanceof Rumi.Event,
                    'onAlphaChannelChange event registered and triggerable'
                );
            });

            QUnit.test('Fires proper events', function(assert) {
                var onRedComponentChangeFired = false,
                    onGreenComponentChangeFired = false,
                    onBlueComponentChangeFired = false,
                    onAlphaChannelChangeFired = false,
                    func = {
                        onRedComponentChange: function() {
                            onRedComponentChangeFired = true;
                        },
                        onGreenComponentChange: function() {
                            onGreenComponentChangeFired = true;
                        },
                        onBlueComponentChange: function() {
                            onBlueComponentChangeFired = true;
                        },
                        onAlphaChannelChange: function() {
                            onAlphaChannelChangeFired = true;
                        }
                    };
                this.rgba.eventsManager.attachEventListener('onRedComponentChange', func.onRedComponentChange);
                this.rgba.eventsManager.attachEventListener('onGreenComponentChange', func.onGreenComponentChange);
                this.rgba.eventsManager.attachEventListener('onBlueComponentChange', func.onBlueComponentChange);
                this.rgba.eventsManager.attachEventListener('onAlphaChannelChange', func.onAlphaChannelChange);

                this.rgba.red = 1;
                this.rgba.green = 2;
                this.rgba.blue = 3;
                this.rgba.alpha = 0.3;

                assert.ok(onRedComponentChangeFired, 'onRedComponentChange fired');
                assert.ok(onGreenComponentChangeFired, 'onGreenComponentChange fired');
                assert.ok(onBlueComponentChangeFired, 'onBlueComponentChange fired');
                assert.ok(onAlphaChannelChangeFired, 'onAlphaChannelChange fired');
            });

            QUnit.test('copies() works', function(assert) {
                var other = new Rumi.RGBA(123, 132, 231);
                this.rgba.copies(other);
                assert.ok(this.rgba.red === other.red, 'red copied');
                assert.ok(this.rgba.green === other.green, 'green copied');
                assert.ok(this.rgba.blue === other.blue, 'blue copied');
                assert.ok(this.rgba.alpha === other.alpha, 'alpha copied');
            });

            QUnit.test('copy() works', function(assert) {
                var copy = this.rgba.copy();
                assert.ok(this.rgba !== copy, 'Not the same instance');
                assert.ok(this.rgba.red === copy.red, 'red copied');
                assert.ok(this.rgba.green === copy.green, 'green copied');
                assert.ok(this.rgba.blue === copy.blue, 'blue copied');
                assert.ok(this.rgba.alpha === copy.alpha, 'alpha copied');
            });
        }
    );

    QUnit.module('Fill',
        {
            beforeEach: function() {
                this.fill = new Rumi.Fill();
                this.validBlendMode = function(mode) {
                    for(var bm in Rumi.BlendModes) {
                        if(Rumi.BlendModes[bm] === mode) return true;
                    }
                    return false;
                };
            }
        },
        function() {
            QUnit.test('Default constructor works', function(assert) {
                assert.ok(this.fill.eventsManager instanceof Rumi.EventsManager, 'eventsManager type ok');
                assert.ok(this.fill.color instanceof Rumi.RGBA, 'color type ok');
                assert.ok(this.fill.blendMode === Rumi.BlendModes.SOURCE_OVER, 'blendMode initialized ok');
                assert.ok(this.validBlendMode(this.fill.blendMode), 'blendMode valid');
                assert.ok(
                    this.fill.eventsManager.events['onColorChange'] !== undefined &&
                    this.fill.eventsManager.events['onColorChange'] instanceof Rumi.Event,
                    'onColorChange event registered and triggerable'
                );
                assert.ok(
                    this.fill.eventsManager.events['onBlendModeChange'] !== undefined &&
                    this.fill.eventsManager.events['onBlendModeChange'] instanceof Rumi.Event,
                    'onBlendModeChange event registered and triggerable'
                );
            });

            QUnit.test('Constructor works', function(assert) {
                this.fill = new Rumi.Fill(new Rumi.RGBA(25, 50, 75, 0.8), Rumi.BlendModes.SOURCE_IN);
                assert.ok(this.fill.eventsManager instanceof Rumi.EventsManager, 'eventsManager type ok');
                assert.ok(this.fill.color instanceof Rumi.RGBA, 'color type ok');
                assert.ok(this.fill.color.red === 25, 'color red component initialized ok');
                assert.ok(this.fill.color.green === 50, 'color green component initialized ok');
                assert.ok(this.fill.color.blue === 75, 'color blue component initialized ok');
                assert.ok(this.fill.color.alpha === 0.8, 'color alpha channel initialized ok');
                assert.ok(this.fill.blendMode === Rumi.BlendModes.SOURCE_IN, 'blendMode initialized ok');
                assert.ok(this.validBlendMode(this.fill.blendMode), 'blendMode valid');
                assert.ok(
                    this.fill.eventsManager.events['onColorChange'] !== undefined &&
                    this.fill.eventsManager.events['onColorChange'] instanceof Rumi.Event,
                    'onColorChange event registered and triggerable'
                );
                assert.ok(
                    this.fill.eventsManager.events['onBlendModeChange'] !== undefined &&
                    this.fill.eventsManager.events['onBlendModeChange'] instanceof Rumi.Event,
                    'onBlendModeChange event registered and triggerable'
                );
            });

            QUnit.test('Fires proper events', function(assert) {
                var onColorChangeFired = false,
                    onBlendModeChangeFired = false,
                    func = {
                        onColorChange: function() {
                            onColorChangeFired = true;
                        },
                        onBlendModeChange: function() {
                            onBlendModeChangeFired = true;
                        }
                    };
                this.fill.eventsManager.attachEventListener('onColorChange', func.onColorChange);
                this.fill.eventsManager.attachEventListener('onBlendModeChange', func.onBlendModeChange);

                this.fill.color.red = 155;
                this.fill.blendMode = Rumi.BlendModes.SOURCE_IN;

                assert.ok(onColorChangeFired, 'onColorChange fired');
                assert.ok(onBlendModeChangeFired, 'onBlendModeChange fired');
            });

            QUnit.test('copies() works', function (assert) {
                var other = new Rumi.Fill(new Rumi.RGBA(25, 26, 27, 0.3), Rumi.BlendModes.COLOR_BURN);
                this.fill.copies(other);

                assert.ok(
                    this.fill.color.red === other.color.red &&
                    this.fill.color.green === other.color.green &&
                    this.fill.color.blue === other.color.blue &&
                    this.fill.color.alpha === other.color.alpha,
                    'color copied'
                );

                assert.ok(this.fill.blendMode === other.blendMode, 'blendMode copied');
            });

            QUnit.test('copy() works', function(assert) {
                var copy = this.fill.copy();

                assert.ok(this.fill !== copy, 'Not the same instance');
                assert.ok(
                    this.fill.color.red === copy.color.red &&
                    this.fill.color.green === copy.color.green &&
                    this.fill.color.blue === copy.color.blue &&
                    this.fill.color.alpha === copy.color.alpha,
                    'color copied'
                );

                assert.ok(this.fill.blendMode === copy.blendMode, 'blendMode copied');
            });
        }
    );

    QUnit.module('Stroke',
        {
            beforeEach: function() {
                this.stroke = new Rumi.Stroke();
                this.validBlendMode = function(mode) {
                    for(var bm in Rumi.BlendModes) {
                        if(Rumi.BlendModes[bm] === mode) return true;
                    }
                    return false;
                };
            }
        },
        function() {
            QUnit.test('Default constructor works', function(assert) {
                assert.ok(this.stroke.color instanceof Rumi.RGBA, 'color type ok');
                assert.ok(this.stroke.thickness === 1, 'thickness initialized ok');
                assert.ok(this.stroke.blendMode === Rumi.BlendModes.SOURCE_OVER, 'blendMode initialized ok');
                assert.ok(this.validBlendMode(this.stroke.blendMode), 'blendMode valid');
                assert.ok(this.stroke.eventsManager instanceof Rumi.EventsManager, 'eventsManager type ok');
                assert.ok(
                    this.stroke.eventsManager.events['onThicknessChange'] !== undefined &&
                    this.stroke.eventsManager.events['onThicknessChange'] instanceof Rumi.Event,
                    'onThicknessChange event registered and triggerable'
                );
                assert.ok(
                    this.stroke.eventsManager.events['onColorChange'] !== undefined &&
                    this.stroke.eventsManager.events['onColorChange'] instanceof Rumi.Event,
                    'onColorChange event registered and triggerable'
                );
                assert.ok(
                    this.stroke.eventsManager.events['onBlendModeChange'] !== undefined &&
                    this.stroke.eventsManager.events['onBlendModeChange'] instanceof Rumi.Event,
                    'onBlendModeChange event registered and triggerable'
                );
            });

            QUnit.test('Constructor works', function(assert) {
                this.stroke = new Rumi.Stroke(5, new Rumi.RGBA(1, 2, 3, 1), Rumi.BlendModes.SOURCE_IN);
                assert.ok(this.stroke.color instanceof Rumi.RGBA, 'color type ok');
                assert.ok(this.stroke.color.red === 1, 'color red component initialized ok');
                assert.ok(this.stroke.color.green === 2, 'color green component initialized ok');
                assert.ok(this.stroke.color.blue === 3, 'color blue component initialized ok');
                assert.ok(this.stroke.color.alpha === 1, 'color alpha channel initialized ok');
                assert.ok(this.stroke.thickness === 5, 'thickness initialized ok');
                assert.ok(this.stroke.blendMode === Rumi.BlendModes.SOURCE_IN, 'blendMode initialized ok');
                assert.ok(this.validBlendMode(this.stroke.blendMode), 'blendMode valid');
                assert.ok(this.stroke.eventsManager instanceof Rumi.EventsManager, 'eventsManager type ok');
                assert.ok(
                    this.stroke.eventsManager.events['onThicknessChange'] !== undefined &&
                    this.stroke.eventsManager.events['onThicknessChange'] instanceof Rumi.Event,
                    'onThicknessChange event registered and triggerable'
                );
                assert.ok(
                    this.stroke.eventsManager.events['onColorChange'] !== undefined &&
                    this.stroke.eventsManager.events['onColorChange'] instanceof Rumi.Event,
                    'onColorChange event registered and triggerable'
                );
                assert.ok(
                    this.stroke.eventsManager.events['onBlendModeChange'] !== undefined &&
                    this.stroke.eventsManager.events['onBlendModeChange'] instanceof Rumi.Event,
                    'onBlendModeChange event registered and triggerable'
                );
            });

            QUnit.test('Fires proper events', function(assert) {
                var onThicknessChangeFired = false,
                    onColorChangeFired = false,
                    onBlendModeChangeFired = false,
                    func = {
                        onThicknessChange: function() {
                            onThicknessChangeFired = true;
                        },
                        onColorChange: function() {
                            onColorChangeFired = true;
                        },
                        onBlendModeChange: function() {
                            onBlendModeChangeFired = true;
                        }
                    }

                this.stroke.eventsManager.attachEventListener('onThicknessChange', func.onThicknessChange);
                this.stroke.eventsManager.attachEventListener('onColorChange', func.onColorChange);
                this.stroke.eventsManager.attachEventListener('onBlendModeChange', func.onBlendModeChange);

                this.stroke.thickness = 10;
                this.stroke.color.red = 125;
                this.stroke.blendMode = Rumi.BlendModes.SOURCE_IN;

                assert.ok(onThicknessChangeFired, 'onThicknessChange fired');
                assert.ok(onColorChangeFired, 'onColorChange fired');
                assert.ok(onBlendModeChangeFired, 'onBlendModeChange fired');
            });

            QUnit.test('copies() works', function (assert) {
                var other = new Rumi.Stroke(12, new Rumi.RGBA(12, 13, 14, 0.5), Rumi.BlendModes.COLOR_DODGE);
                this.stroke.copies(other);

                assert.ok(this.stroke.thickness === other.thickness, 'thickness copied');
                assert.ok(
                    this.stroke.color.red === other.color.red &&
                    this.stroke.color.green === other.color.green &&
                    this.stroke.color.blue === other.color.blue &&
                    this.stroke.color.alpha === other.color.alpha,
                    'color copied'
                );

                assert.ok(this.stroke.blendMode === other.blendMode, 'blendMode copied');
            });

            QUnit.test('copy() works', function(assert) {
                var copy = this.stroke.copy();

                assert.ok(this.stroke !== copy, 'Not the same instance');
                assert.ok(this.stroke.thickness === copy.thickness, 'thickness copied');
                assert.ok(
                    this.stroke.color.red === copy.color.red &&
                    this.stroke.color.green === copy.color.green &&
                    this.stroke.color.blue === copy.color.blue &&
                    this.stroke.color.alpha === copy.color.alpha,
                    'color copied'
                );

                assert.ok(this.stroke.blendMode === copy.blendMode, 'blendMode copied');
            });
        }
    );

    QUnit.module('ESATVector',
        {
            beforeEach: function() {
                this.vector = new Rumi.ESATVector();
            }
        },
        function() {
            QUnit.test('Default constructor works', function(assert) {
                assert.ok(this.vector instanceof SAT.Vector, 'Inheritance ok');
                assert.ok(this.vector.x === 0, 'x initialized ok');
                assert.ok(this.vector.y === 0, 'y initialized ok');
                assert.ok(this.vector.fill instanceof Rumi.Fill, 'fill type ok');
                assert.ok(this.vector.stroke instanceof Rumi.Stroke, 'stroke type ok');
                assert.ok(this.vector.eventsManager instanceof Rumi.EventsManager, 'eventsManager type ok');
                assert.ok(
                    this.vector.eventsManager.events['onXChange'] !== undefined &&
                    this.vector.eventsManager.events['onXChange'] instanceof Rumi.Event,
                    'onXChange event registered and triggerable'
                );
                assert.ok(
                    this.vector.eventsManager.events['onYChange'] !== undefined &&
                    this.vector.eventsManager.events['onYChange'] instanceof Rumi.Event,
                    'onYChange event registered and triggerable'
                );
                assert.ok(
                    this.vector.eventsManager.events['onFillChange'] !== undefined &&
                    this.vector.eventsManager.events['onFillChange'] instanceof Rumi.Event,
                    'onFillChange event registered and triggerable'
                );
                assert.ok(
                    this.vector.eventsManager.events['onStrokeChange'] !== undefined &&
                    this.vector.eventsManager.events['onStrokeChange'] instanceof Rumi.Event,
                    'onStrokeChange event registered and triggerable'
                );
            });

            QUnit.test('Constructor works', function(assert) {
                this.vector = new Rumi.ESATVector(15, 25);
                assert.ok(this.vector instanceof SAT.Vector, 'Inheritance ok');
                assert.ok(this.vector.x === 15, 'x initialized ok');
                assert.ok(this.vector.y === 25, 'y initialized ok');
                assert.ok(this.vector.fill instanceof Rumi.Fill, 'fill type ok');
                assert.ok(this.vector.stroke instanceof Rumi.Stroke, 'stroke type ok');
                assert.ok(this.vector.eventsManager instanceof Rumi.EventsManager, 'eventsManager type ok');
                assert.ok(
                    this.vector.eventsManager.events['onXChange'] !== undefined &&
                    this.vector.eventsManager.events['onXChange'] instanceof Rumi.Event,
                    'onXChange event registered and triggerable'
                );
                assert.ok(
                    this.vector.eventsManager.events['onYChange'] !== undefined &&
                    this.vector.eventsManager.events['onYChange'] instanceof Rumi.Event,
                    'onYChange event registered and triggerable'
                );
                assert.ok(
                    this.vector.eventsManager.events['onFillChange'] !== undefined &&
                    this.vector.eventsManager.events['onFillChange'] instanceof Rumi.Event,
                    'onFillChange event registered and triggerable'
                );
                assert.ok(
                    this.vector.eventsManager.events['onStrokeChange'] !== undefined &&
                    this.vector.eventsManager.events['onStrokeChange'] instanceof Rumi.Event,
                    'onStrokeChange event registered and triggerable'
                );
            });

            QUnit.test('Fires proper events', function(assert) {
                var onXChangeFired = false,
                    onYChangeFired = false,
                    onFillChangeFired = false,
                    onStrokeChangeFired = false,
                    func = {
                        onXChange: function () {
                            onXChangeFired = true;
                        },
                        onYChange: function() {
                            onYChangeFired = true;
                        },
                        onFillChange: function() {
                            onFillChangeFired = true;
                        },
                        onStrokeChange: function() {
                            onStrokeChangeFired = true;
                        }
                    };

                this.vector.eventsManager.attachEventListener('onXChange', func.onXChange);
                this.vector.eventsManager.attachEventListener('onYChange', func.onYChange);
                this.vector.eventsManager.attachEventListener('onFillChange', func.onFillChange);
                this.vector.eventsManager.attachEventListener('onStrokeChange', func.onStrokeChange);

                this.vector.x = 10;
                this.vector.y = 100;
                this.vector.fill.color.red = 100;
                this.vector.stroke.color.red = 124;

                assert.ok(onXChangeFired, 'onXChange fired');
                assert.ok(onYChangeFired, 'onYChange fired');
                assert.ok(onFillChangeFired, 'onFillChange fired');
                assert.ok(onStrokeChangeFired, 'onStrokeChange fired');
            });

            QUnit.test('copy() works', function(assert) {
                var copy = this.vector.copy();

                assert.ok(
                    copy.x === this.vector.x &&
                    copy.y === this.vector.y,
                    'x and y copied'
                );

                assert.ok(
                    copy.fill.color.red === this.vector.fill.color.red &&
                    copy.fill.color.green === this.vector.fill.color.green &&
                    copy.fill.color.blue === this.vector.fill.color.blue &&
                    copy.fill.color.alpha === this.vector.fill.color.alpha &&
                    copy.fill.blendMode === this.vector.fill.blendMode,
                    'fill copied'
                );

                assert.ok(
                    copy.stroke.thickness === this.vector.stroke.thickness &&
                    copy.stroke.color.red === this.vector.stroke.color.red &&
                    copy.stroke.color.green === this.vector.stroke.color.green &&
                    copy.stroke.color.blue === this.vector.stroke.color.blue &&
                    copy.stroke.color.alpha === this.vector.stroke.color.alpha &&
                    copy.stroke.blendMode === this.vector.stroke.blendMode,
                    'stroke copied'
                );
            });

            QUnit.test('copies() works', function(assert) {
                var other = new Rumi.ESATVector(25, 30);
                other.fill.copies(new Rumi.Fill(new Rumi.RGBA(1, 2, 3, 1), Rumi.BlendModes.COLOR_DODGE));
                var copy = this.vector.copy();

                assert.ok(
                    copy.x === this.vector.x &&
                    copy.y === this.vector.y,
                    'x and y copied'
                );

                assert.ok(
                    copy.fill.color.red === this.vector.fill.color.red &&
                    copy.fill.color.green === this.vector.fill.color.green &&
                    copy.fill.color.blue === this.vector.fill.color.blue &&
                    copy.fill.color.alpha === this.vector.fill.color.alpha &&
                    copy.fill.blendMode === this.vector.fill.blendMode,
                    'fill copied'
                );

                assert.ok(
                    copy.stroke.thickness === this.vector.stroke.thickness &&
                    copy.stroke.color.red === this.vector.stroke.color.red &&
                    copy.stroke.color.green === this.vector.stroke.color.green &&
                    copy.stroke.color.blue === this.vector.stroke.color.blue &&
                    copy.stroke.color.alpha === this.vector.stroke.color.alpha &&
                    copy.stroke.blendMode === this.vector.stroke.blendMode,
                    'stroke copied'
                );
            });
        }
    );

    /*Continue from here*/
    QUnit.module('ESATCircle',
        {
            beforeEach: function() {
                this.circle = new Rumi.ESATCircle();
            }
        },
        function() {
            QUnit.test('Default constructor works', function(assert) {
                assert.ok(this.circle instanceof SAT.Circle, 'Inheritance ok');
                assert.ok(this.circle.pos instanceof Rumi.ESATVector, 'pos type ok');
                assert.ok(this.circle.pos.x === 0, 'pos.x initialized ok');
                assert.ok(this.circle.pos.y === 0, 'pos.y initialized ok');
                assert.ok(this.circle.r === 0, 'r initialized ok');
                assert.ok(this.circle.fill instanceof Rumi.Fill, 'fill type ok');
                assert.ok(this.circle.stroke instanceof Rumi.Stroke, 'stroke type ok');
                assert.ok(this.circle.eventsManager instanceof Rumi.EventsManager, 'eventsManager type ok');
                assert.ok(
                    this.circle.eventsManager.events['onPositionChange'] !== undefined &&
                    this.circle.eventsManager.events['onPositionChange'] instanceof Rumi.Event,
                    'onPositionChange event registered and triggerable'
                );
                assert.ok(
                    this.circle.eventsManager.events['onRadiusChange'] !== undefined &&
                    this.circle.eventsManager.events['onRadiusChange'] instanceof Rumi.Event,
                    'onRadiusChange event registered and triggerable'
                );
                assert.ok(
                    this.circle.eventsManager.events['onFillChange'] !== undefined &&
                    this.circle.eventsManager.events['onFillChange'] instanceof Rumi.Event,
                    'onFillChange event registered and triggerable'
                );
                assert.ok(
                    this.circle.eventsManager.events['onStrokeChange'] !== undefined &&
                    this.circle.eventsManager.events['onStrokeChange'] instanceof Rumi.Event,
                    'onStrokeChange event registered and triggerable'
                );
            });

            QUnit.test('Constructor works', function(assert) {
                this.circle = new Rumi.ESATCircle(new Rumi.ESATVector(25, 50), 15);

                assert.ok(this.circle instanceof SAT.Circle, 'Inheritance ok');
                assert.ok(this.circle.pos instanceof Rumi.ESATVector, 'pos type ok');
                assert.ok(this.circle.pos.x === 25, 'pos.x initialized ok');
                assert.ok(this.circle.pos.y === 50, 'pos.y initialized ok');
                assert.ok(this.circle.r === 15, 'r initialized ok');
                assert.ok(this.circle.fill instanceof Rumi.Fill, 'fill type ok');
                assert.ok(this.circle.stroke instanceof Rumi.Stroke, 'stroke type ok');
                assert.ok(this.circle.eventsManager instanceof Rumi.EventsManager, 'eventsManager type ok');
                assert.ok(
                    this.circle.eventsManager.events['onPositionChange'] !== undefined &&
                    this.circle.eventsManager.events['onPositionChange'] instanceof Rumi.Event,
                    'onPositionChange event registered and triggerable'
                );
                assert.ok(
                    this.circle.eventsManager.events['onRadiusChange'] !== undefined &&
                    this.circle.eventsManager.events['onRadiusChange'] instanceof Rumi.Event,
                    'onRadiusChange event registered and triggerable'
                );
                assert.ok(
                    this.circle.eventsManager.events['onFillChange'] !== undefined &&
                    this.circle.eventsManager.events['onFillChange'] instanceof Rumi.Event,
                    'onFillChange event registered and triggerable'
                );
                assert.ok(
                    this.circle.eventsManager.events['onStrokeChange'] !== undefined &&
                    this.circle.eventsManager.events['onStrokeChange'] instanceof Rumi.Event,
                    'onStrokeChange event registered and triggerable'
                );
            });

            QUnit.test('Fires proper events', function(assert) {
                var onPositionChangeFired = false,
                    onRadiusChangeFired = false,
                    onFillChangeFired = false,
                    onStrokeChangeFired = false,
                    func = {
                        onPositionChange: function() {
                            onPositionChangeFired = true;
                        },
                        onRadiusChange: function() {
                            onRadiusChangeFired = true;
                        },
                        'onFillChange': function() {
                            onFillChangeFired = true;
                        },
                        'onStrokeChange': function() {
                            onStrokeChangeFired = true;
                        }
                    };
                this.circle.eventsManager.attachEventListener('onPositionChange', func.onPositionChange);
                this.circle.eventsManager.attachEventListener('onRadiusChange', func.onRadiusChange);
                this.circle.eventsManager.attachEventListener('onFillChange', func.onFillChange);
                this.circle.eventsManager.attachEventListener('onStrokeChange', func.onStrokeChange);

                this.circle.pos.x = 25;
                this.circle.pos.y = 25;
                this.circle.r = 10;
                this.circle.fill.color.red = 250;
                this.circle.stroke.color.red = 25;

                assert.ok(onPositionChangeFired, 'onPositionChange fired');
                assert.ok(onRadiusChangeFired, 'onRadiusChange fired');
                assert.ok(onFillChangeFired, 'onFillChange fired');
                assert.ok(onStrokeChangeFired, 'onStrokeChange fired');
            });

            QUnit.test('copy() works', function(assert) {
                var copy = this.circle.copy();
                assert.ok(
                    this.circle.pos.x === copy.pos.x &&
                    this.circle.pos.y === copy.pos.y,
                    'pos copied'
                );

                assert.ok(this.circle.r === copy.r, 'r copied');

                assert.ok(
                    copy.fill.color.red === this.circle.fill.color.red &&
                    copy.fill.color.green === this.circle.fill.color.green &&
                    copy.fill.color.blue === this.circle.fill.color.blue &&
                    copy.fill.color.alpha === this.circle.fill.color.alpha &&
                    copy.fill.blendMode === this.circle.fill.blendMode,
                    'fill copied'
                );

                assert.ok(
                    copy.stroke.thickness === this.circle.stroke.thickness &&
                    copy.stroke.color.red === this.circle.stroke.color.red &&
                    copy.stroke.color.green === this.circle.stroke.color.green &&
                    copy.stroke.color.blue === this.circle.stroke.color.blue &&
                    copy.stroke.color.alpha === this.circle.stroke.color.alpha &&
                    copy.stroke.blendMode === this.circle.stroke.blendMode,
                    'stroke copied'
                );
            });

            QUnit.test('copies() works', function(assert) {
                var other = new Rumi.ESATCircle(new Rumi.ESATVector(12, 13), 5);
                other.fill.copies(new Rumi.Fill(new Rumi.RGBA(5, 6, 7, 1), Rumi.BlendModes.SOURCE_ATOP));
                other.stroke.copies(new Rumi.Stroke(5, new Rumi.RGBA(10, 12, 14, 0), Rumi.BlendModes.COLOR_BURN));
                this.circle.copies(other);

                assert.ok(
                    this.circle.pos.x === other.pos.x &&
                    this.circle.pos.y === other.pos.y,
                    'pos copied'
                );

                assert.ok(this.circle.r === other.r, 'r copied');

                assert.ok(
                    other.fill.color.red === this.circle.fill.color.red &&
                    other.fill.color.green === this.circle.fill.color.green &&
                    other.fill.color.blue === this.circle.fill.color.blue &&
                    other.fill.color.alpha === this.circle.fill.color.alpha &&
                    other.fill.blendMode === this.circle.fill.blendMode,
                    'fill copied'
                );

                assert.ok(
                    other.stroke.thickness === this.circle.stroke.thickness &&
                    other.stroke.color.red === this.circle.stroke.color.red &&
                    other.stroke.color.green === this.circle.stroke.color.green &&
                    other.stroke.color.blue === this.circle.stroke.color.blue &&
                    other.stroke.color.alpha === this.circle.stroke.color.alpha &&
                    other.stroke.blendMode === this.circle.stroke.blendMode,
                    'stroke copied'
                );
            });
        }
    );

    QUnit.module('ESATBox',
        {
            beforeEach: function() {
                this.box = new Rumi.ESATBox();
            }
        },
        function() {
            QUnit.test('Default constructor works', function(assert) {
                assert.ok(this.box instanceof SAT.Box, 'Inheritance ok');
                assert.ok(this.box.pos instanceof Rumi.ESATVector, 'pos type ok');
                assert.ok(this.box.pos.x === 0, 'pos.x initialized ok');
                assert.ok(this.box.pos.y === 0, 'pos.y initialized ok');
                assert.ok(this.box.w === 0, 'w initialized ok');
                assert.ok(this.box.h === 0, 'h initialized ok');
                assert.ok(this.box.fill instanceof Rumi.Fill, 'fill type ok');
                assert.ok(this.box.stroke instanceof Rumi.Stroke, 'stroke type ok');
                assert.ok(this.box.eventsManager instanceof Rumi.EventsManager);
                assert.ok(
                    this.box.eventsManager.events['onPositionChange'] !== undefined &&
                    this.box.eventsManager.events['onPositionChange'] instanceof Rumi.Event,
                    'onPositionChange event registered and triggerable'
                );
                assert.ok(
                    this.box.eventsManager.events['onWidthChange'] !== undefined &&
                    this.box.eventsManager.events['onWidthChange'] instanceof Rumi.Event,
                    'onWidthChange event registered and triggerable'
                );
                assert.ok(
                    this.box.eventsManager.events['onHeightChange'] !== undefined &&
                    this.box.eventsManager.events['onHeightChange'] instanceof Rumi.Event,
                    'onHeightChange event registered and triggerable'
                );
                assert.ok(
                    this.box.eventsManager.events['onFillChange'] !== undefined &&
                    this.box.eventsManager.events['onFillChange'] instanceof Rumi.Event,
                    'onFillChange event registered and triggerable'
                );
                assert.ok(
                    this.box.eventsManager.events['onStrokeChange'] !== undefined &&
                    this.box.eventsManager.events['onStrokeChange'] instanceof Rumi.Event,
                    'onStrokeChange event registered and triggerable'
                );
            });

            QUnit.test('Constructor works', function(assert) {
                this.box = new Rumi.ESATBox(new Rumi.ESATVector(5, 6), 50, 140);

                assert.ok(this.box instanceof SAT.Box, 'Inheritance ok');
                assert.ok(this.box.pos instanceof Rumi.ESATVector, 'pos type ok');
                assert.ok(this.box.pos.x === 5, 'pos.x initialized ok');
                assert.ok(this.box.pos.y === 6, 'pos.y initialized ok');
                assert.ok(this.box.w === 50, 'w initialized ok');
                assert.ok(this.box.h === 140, 'h initialized ok');
                assert.ok(this.box.fill instanceof Rumi.Fill, 'fill type ok');
                assert.ok(this.box.stroke instanceof Rumi.Stroke, 'stroke type ok');
                assert.ok(this.box.eventsManager instanceof Rumi.EventsManager);
                assert.ok(
                    this.box.eventsManager.events['onPositionChange'] !== undefined &&
                    this.box.eventsManager.events['onPositionChange'] instanceof Rumi.Event,
                    'onPositionChange event registered and triggerable'
                );
                assert.ok(
                    this.box.eventsManager.events['onWidthChange'] !== undefined &&
                    this.box.eventsManager.events['onWidthChange'] instanceof Rumi.Event,
                    'onWidthChange event registered and triggerable'
                );
                assert.ok(
                    this.box.eventsManager.events['onHeightChange'] !== undefined &&
                    this.box.eventsManager.events['onHeightChange'] instanceof Rumi.Event,
                    'onHeightChange event registered and triggerable'
                );
                assert.ok(
                    this.box.eventsManager.events['onFillChange'] !== undefined &&
                    this.box.eventsManager.events['onFillChange'] instanceof Rumi.Event,
                    'onFillChange event registered and triggerable'
                );
                assert.ok(
                    this.box.eventsManager.events['onStrokeChange'] !== undefined &&
                    this.box.eventsManager.events['onStrokeChange'] instanceof Rumi.Event,
                    'onStrokeChange event registered and triggerable'
                );
            });

            QUnit.test('Fires proper events', function(assert) {
                var onPositionChangeFired = false,
                    onWidthChangeFired = false,
                    onHeightChangeFired = false,
                    onFillChangeFired = false,
                    onStrokeChangeFired = false,
                    func = {
                        onPositionChange: function() {
                            onPositionChangeFired = true;
                        },
                        onWidthChange: function() {
                            onWidthChangeFired = true;
                        },
                        onHeightChange: function() {
                            onHeightChangeFired = true;
                        },
                        onFillChange: function() {
                            onFillChangeFired = true;
                        },
                        onStrokeChange: function() {
                            onStrokeChangeFired = true;
                        }
                    };
                this.box.eventsManager.attachEventListener('onPositionChange', func.onPositionChange);
                this.box.eventsManager.attachEventListener('onWidthChange', func.onWidthChange);
                this.box.eventsManager.attachEventListener('onHeightChange', func.onHeightChange);
                this.box.eventsManager.attachEventListener('onFillChange', func.onFillChange);
                this.box.eventsManager.attachEventListener('onStrokeChange', func.onStrokeChange);

                this.box.pos.x = 15;
                this.box.pos.y = 15;
                this.box.w = 50;
                this.box.h = 10;
                this.box.fill.color.red = 172;
                this.box.stroke.color.red = 27;

                assert.ok(onPositionChangeFired, 'onPositionChange fired');
                assert.ok(onWidthChangeFired, 'onWidthChange fired');
                assert.ok(onHeightChangeFired, 'onHeightChange fired');
                assert.ok(onFillChangeFired, 'onFillChange fired');
                assert.ok(onStrokeChangeFired, 'onStrokeChange fired');
            });

            QUnit.test('copy() works', function(assert) {
                var copy = this.box.copy();
                assert.ok(
                    this.box.pos.x === copy.pos.x &&
                    this.box.pos.y === copy.pos.y,
                    'pos copied'
                );

                assert.ok(
                    this.box.w === copy.w &&
                    this.box.h === copy.h,
                    'w and h copied'
                );

                assert.ok(
                    copy.fill.color.red === this.box.fill.color.red &&
                    copy.fill.color.green === this.box.fill.color.green &&
                    copy.fill.color.blue === this.box.fill.color.blue &&
                    copy.fill.color.alpha === this.box.fill.color.alpha &&
                    copy.fill.blendMode === this.box.fill.blendMode,
                    'fill copied'
                );

                assert.ok(
                    copy.stroke.thickness === this.box.stroke.thickness &&
                    copy.stroke.color.red === this.box.stroke.color.red &&
                    copy.stroke.color.green === this.box.stroke.color.green &&
                    copy.stroke.color.blue === this.box.stroke.color.blue &&
                    copy.stroke.color.alpha === this.box.stroke.color.alpha &&
                    copy.stroke.blendMode === this.box.stroke.blendMode,
                    'stroke copied'
                );
            });

            QUnit.test('copies() works', function(assert) {
                var other = new Rumi.ESATBox(new Rumi.ESATVector(12, 13), 5, 5);
                other.fill.copies(new Rumi.Fill(new Rumi.RGBA(5, 6, 7, 1), Rumi.BlendModes.SOURCE_ATOP));
                other.stroke.copies(new Rumi.Stroke(5, new Rumi.RGBA(10, 12, 14, 0), Rumi.BlendModes.COLOR_BURN));
                this.box.copies(other);

                assert.ok(
                    this.box.pos.x === other.pos.x &&
                    this.box.pos.y === other.pos.y,
                    'pos copied'
                );

                assert.ok(
                    this.box.w === other.w &&
                    this.box.h === other.h,
                    'w and h copied'
                );

                assert.ok(
                    other.fill.color.red === this.box.fill.color.red &&
                    other.fill.color.green === this.box.fill.color.green &&
                    other.fill.color.blue === this.box.fill.color.blue &&
                    other.fill.color.alpha === this.box.fill.color.alpha &&
                    other.fill.blendMode === this.box.fill.blendMode,
                    'fill copied'
                );

                assert.ok(
                    other.stroke.thickness === this.box.stroke.thickness &&
                    other.stroke.color.red === this.box.stroke.color.red &&
                    other.stroke.color.green === this.box.stroke.color.green &&
                    other.stroke.color.blue === this.box.stroke.color.blue &&
                    other.stroke.color.alpha === this.box.stroke.color.alpha &&
                    other.stroke.blendMode === this.box.stroke.blendMode,
                    'stroke copied'
                );
            });
        }
    );

    QUnit.module('ESATPolygon',
        {
            beforeEach: function() {
                this.polygon = new Rumi.ESATPolygon();
            }
        },
        function() {
            QUnit.test('Default constructor works', function(assert) {
                assert.ok(this.polygon instanceof SAT.Polygon, 'Inheritance ok');
                assert.ok(this.polygon.pos instanceof Rumi.ESATVector, 'pos type ok');
                assert.ok(this.polygon.pos.x === 0 && this.polygon.pos.y === 0, 'pos initialization ok');
                assert.ok(this.polygon.eventsManager instanceof Rumi.EventsManager, 'eventsManager type ok');
                assert.ok(
                    this.polygon.eventsManager.events['onPositionChange'] !== undefined &&
                    this.polygon.eventsManager.events['onPositionChange'] instanceof Rumi.Event,
                    'onPositionChange event registered and triggerable'
                );
                assert.ok(
                    this.polygon.eventsManager.events['onAngleChange'] !== undefined &&
                    this.polygon.eventsManager.events['onAngleChange'] instanceof Rumi.Event,
                    'onAngleChange event registered and triggerable'
                );
                assert.ok(
                    this.polygon.eventsManager.events['onOffsetChange'] !== undefined &&
                    this.polygon.eventsManager.events['onOffsetChange'] instanceof Rumi.Event,
                    'onOffsetChange event registered and triggerable'
                );
                assert.ok(
                    this.polygon.eventsManager.events['onPointsChange'] !== undefined &&
                    this.polygon.eventsManager.events['onPointsChange'] instanceof Rumi.Event,
                    'onPointsChange event registered and triggerable'
                );
                assert.ok(
                    this.polygon.eventsManager.events['onFillChange'] !== undefined &&
                    this.polygon.eventsManager.events['onFillChange'] instanceof Rumi.Event,
                    'onFillChange event registered and triggerable'
                );
                assert.ok(
                    this.polygon.eventsManager.events['onStrokeChange'] !== undefined &&
                    this.polygon.eventsManager.events['onStrokeChange'] instanceof Rumi.Event,
                    'onStrokeChange event registered and triggerable'
                );
            });

            QUnit.test('Constructor works', function(assert) {
                this.polygon = new Rumi.ESATPolygon(new Rumi.ESATVector(25, 30), [
                    new Rumi.ESATVector(),
                    new Rumi.ESATVector(13, 12)
                ]);
                assert.ok(this.polygon instanceof SAT.Polygon, 'Inheritance ok');
                assert.ok(this.polygon.pos instanceof Rumi.ESATVector, 'pos type ok');
                assert.ok(this.polygon.pos.x === 25 && this.polygon.pos.y === 30, 'pos initialization ok');
                assert.ok(
                    this.polygon.points[0].x === 0 && this.polygon.points[0].y === 0 &&
                    this.polygon.points[1].x === 13 && this.polygon.points[1].y === 12,
                    'Points initialization ok'
                );
                assert.ok(this.polygon.eventsManager instanceof Rumi.EventsManager, 'eventsManager type ok');
                assert.ok(
                    this.polygon.eventsManager.events['onPositionChange'] !== undefined &&
                    this.polygon.eventsManager.events['onPositionChange'] instanceof Rumi.Event,
                    'onPositionChange event registered and triggerable'
                );
                assert.ok(
                    this.polygon.eventsManager.events['onAngleChange'] !== undefined &&
                    this.polygon.eventsManager.events['onAngleChange'] instanceof Rumi.Event,
                    'onAngleChange event registered and triggerable'
                );
                assert.ok(
                    this.polygon.eventsManager.events['onOffsetChange'] !== undefined &&
                    this.polygon.eventsManager.events['onOffsetChange'] instanceof Rumi.Event,
                    'onOffsetChange event registered and triggerable'
                );
                assert.ok(
                    this.polygon.eventsManager.events['onPointsChange'] !== undefined &&
                    this.polygon.eventsManager.events['onPointsChange'] instanceof Rumi.Event,
                    'onPointsChange event registered and triggerable'
                );
                assert.ok(
                    this.polygon.eventsManager.events['onFillChange'] !== undefined &&
                    this.polygon.eventsManager.events['onFillChange'] instanceof Rumi.Event,
                    'onFillChange event registered and triggerable'
                );
                assert.ok(
                    this.polygon.eventsManager.events['onStrokeChange'] !== undefined &&
                    this.polygon.eventsManager.events['onStrokeChange'] instanceof Rumi.Event,
                    'onStrokeChange event registered and triggerable'
                );
            });

            QUnit.test('Fires proper events', function(assert) {
                var result = {
                        onPositionChangeFired: false,
                        onAngleChangeFired: false,
                        onOffsetChangeFired: false,
                        onPointsChangeFired: false,
                        onFillChangeFired: false,
                        onStrokeChangeFired: false
                    },
                    handlers = {
                        onPositionChange: function() {
                            result.onPositionChangeFired = true;
                        },
                        onAngleChange: function() {
                            result.onAngleChangeFired = true;
                        },
                        onOffsetChange: function() {
                            result.onOffsetChangeFired = true;
                        },
                        onPointsChange: function() {
                            result.onPointsChangeFired = true;
                        },
                        onFillChange: function() {
                            result.onFillChangeFired = true;
                        },
                        onStrokeChange: function() {
                            result.onStrokeChangeFired = true;
                        }
                    };
                
                this.polygon.eventsManager.attachEventListener('onPositionChange', handlers.onPositionChange);
                this.polygon.eventsManager.attachEventListener('onAngleChange', handlers.onAngleChange);
                this.polygon.eventsManager.attachEventListener('onOffsetChange', handlers.onOffsetChange);
                this.polygon.eventsManager.attachEventListener('onPointsChange', handlers.onPointsChange);
                this.polygon.eventsManager.attachEventListener('onFillChange', handlers.onFillChange);
                this.polygon.eventsManager.attachEventListener('onStrokeChange', handlers.onStrokeChange);
                
                this.polygon.pos.x = 100;
                this.polygon.pos.y = 200;
                this.polygon.offset.x = 12;
                this.polygon.offset.y = 13;
                this.polygon.angle = Rumi.math.toRad(43);
                this.polygon.setPoints([]);
                this.polygon.fill.color.red = 15;
                this.polygon.stroke.thickness = 12;
                
                assert.ok(result.onPositionChangeFired === true, 'onPositionChange works');
                assert.ok(result.onAngleChangeFired === true, 'onAngleChange works');
                assert.ok(result.onOffsetChangeFired === true, 'onOffsetChange works');
                assert.ok(result.onPointsChangeFired === true, 'onPointsChange works');
                assert.ok(result.onFillChangeFired === true, 'onFillChange works');
                assert.ok(result.onStrokeChangeFired === true, 'onStrokeChange works')
            });

            QUnit.test('copy() works', function(assert) {
                var copy = this.polygon.copy();
                assert.ok(
                    this.polygon.pos.x === copy.pos.x &&
                    this.polygon.pos.y === copy.pos.y,
                    'pos copied'
                );

                var pointsCopied = true;

                for(var i = 0; i < this.polygon.points.length; i++) {
                    var a = this.polygon.points[i],
                        b = copy.points[i];
                    if(a !== b) {
                        if(!(a.x === b.x && a.y === b.y)) {
                            pointsCopied = false;
                            break;
                        }
                    }
                }

                assert.ok(pointsCopied, 'points copied');

                assert.ok(this.polygon.angle === copy.angle, 'angle copied');

                assert.ok(
                    copy.fill.color.red === this.polygon.fill.color.red &&
                    copy.fill.color.green === this.polygon.fill.color.green &&
                    copy.fill.color.blue === this.polygon.fill.color.blue &&
                    copy.fill.color.alpha === this.polygon.fill.color.alpha &&
                    copy.fill.blendMode === this.polygon.fill.blendMode,
                    'fill copied'
                );

                assert.ok(
                    copy.stroke.thickness === this.polygon.stroke.thickness &&
                    copy.stroke.color.red === this.polygon.stroke.color.red &&
                    copy.stroke.color.green === this.polygon.stroke.color.green &&
                    copy.stroke.color.blue === this.polygon.stroke.color.blue &&
                    copy.stroke.color.alpha === this.polygon.stroke.color.alpha &&
                    copy.stroke.blendMode === this.polygon.stroke.blendMode,
                    'stroke copied'
                );
            });

            QUnit.test('copies() works', function(assert) {
                var other = new Rumi.ESATPolygon(new Rumi.ESATVector(12, 13), [
                        new Rumi.ESATVector(25, 10),
                        new Rumi.ESATVector(510, 652)
                    ]);
                other.fill.copies(new Rumi.Fill(new Rumi.RGBA(5, 6, 7, 1), Rumi.BlendModes.SOURCE_ATOP));
                other.stroke.copies(new Rumi.Stroke(5, new Rumi.RGBA(10, 12, 14, 0), Rumi.BlendModes.COLOR_BURN));
                this.polygon.copies(other);

                assert.ok(
                    this.polygon.pos.x === other.pos.x &&
                    this.polygon.pos.y === other.pos.y,
                    'pos copied'
                );

                var pointsCopied = true;

                for(var i = 0; i < this.polygon.points.length; i++) {
                    var a = this.polygon.points[i],
                        b = other.points[i];
                    if(a !== b) {
                        if(!(a.x === b.x && a.y === b.y)) {
                            pointsCopied = false;
                            break;
                        }
                    }
                }

                assert.ok(pointsCopied, 'points copied');

                assert.ok(this.polygon.angle === other.angle, 'angle copied');

                assert.ok(
                    other.fill.color.red === this.polygon.fill.color.red &&
                    other.fill.color.green === this.polygon.fill.color.green &&
                    other.fill.color.blue === this.polygon.fill.color.blue &&
                    other.fill.color.alpha === this.polygon.fill.color.alpha &&
                    other.fill.blendMode === this.polygon.fill.blendMode,
                    'fill copied'
                );

                assert.ok(
                    other.stroke.thickness === this.polygon.stroke.thickness &&
                    other.stroke.color.red === this.polygon.stroke.color.red &&
                    other.stroke.color.green === this.polygon.stroke.color.green &&
                    other.stroke.color.blue === this.polygon.stroke.color.blue &&
                    other.stroke.color.alpha === this.polygon.stroke.color.alpha &&
                    other.stroke.blendMode === this.polygon.stroke.blendMode,
                    'stroke copied'
                );
            });
        }
    );

    QUnit.module('Sprite',
        {
            beforeEach: function(assert) {
                var done = assert.async();
                this.image = document.createElement('img');
                var _that = this;
                this.image.onload = function() {
                    _that.sprite = new Rumi.Sprite(null, _that.image);
                    done();
                };
                this.image.src = '../img/player.png';
            }
        },
        function() {
            QUnit.test('Default constructor works', function(assert) {
                this.sprite = new Rumi.Sprite();
                assert.ok(this.sprite.box instanceof Rumi.ESATBox, 'box type ok');
                assert.ok(
                    this.sprite.box.pos.x === 0 &&
                    this.sprite.box.pos.y === 0,
                    'box.pos initialized ok'
                );
                assert.ok(
                    this.sprite.box.w === 100 &&
                    this.sprite.box.h === 100,
                    'box.w and box.h initialized ok'
                );
                assert.ok(this.sprite.image instanceof HTMLImageElement, 'image type and initialization ok');
                assert.ok(this.sprite.angle === 0, 'angle initialized ok');
                assert.ok(this.sprite.clipBox instanceof Rumi.ESATBox, 'clipBox type ok');
                assert.ok(
                    this.sprite.clipBox.pos.x === 0 &&
                    this.sprite.clipBox.pos.y === 0,
                    'clipBox.pos initialized ok'
                );
                assert.ok(
                    this.sprite.clipBox.w === this.sprite.image.naturalWidth &&
                    this.sprite.clipBox.h === this.sprite.image.naturalHeight,
                    'clipBox.w and clipBox.h initialized ok'
                );
                assert.ok(this.sprite.scale instanceof Rumi.ESATVector, 'scale type ok');
                assert.ok(
                    this.sprite.scale.x === 1 && this.sprite.scale.y == 1,
                    'scale initialized ok'
                );
                assert.ok(this.sprite.blendMode === Rumi.BlendModes.SOURCE_OVER, 'blendMode initialized ok');
                assert.ok(this.sprite.alpha === 1, 'alpha initialized ok');
                assert.ok(
                    this.sprite.eventsManager.events['onBoxChange'] !== undefined &&
                    this.sprite.eventsManager.events['onBoxChange'] instanceof Rumi.Event,
                    'onBoxChange event registered and triggerable'
                );
                assert.ok(
                    this.sprite.eventsManager.events['onClipBoxChange'] !== undefined &&
                    this.sprite.eventsManager.events['onClipBoxChange'] instanceof Rumi.Event,
                    'onClipBoxChange event registered and triggerable'
                );
                assert.ok(
                    this.sprite.eventsManager.events['onScaleChange'] !== undefined &&
                    this.sprite.eventsManager.events['onScaleChange'] instanceof Rumi.Event,
                    'onScaleChange event registered and triggerable'
                );
                assert.ok(
                    this.sprite.eventsManager.events['onImageChange'] !== undefined &&
                    this.sprite.eventsManager.events['onImageChange'] instanceof Rumi.Event,
                    'onImageChange event registered and triggerable'
                );
                assert.ok(
                    this.sprite.eventsManager.events['onAngleChange'] !== undefined &&
                    this.sprite.eventsManager.events['onAngleChange'] instanceof Rumi.Event,
                    'onAngleChange event registered and triggerable'
                );
            });

            QUnit.test('Constructor works', function(assert) {
                this.sprite = new Rumi.Sprite(new Rumi.ESATBox(new Rumi.ESATVector(25, 40), 50, 150), this.image);
                assert.ok(this.sprite.box instanceof Rumi.ESATBox, 'box type ok');
                assert.ok(
                    this.sprite.box.pos.x === 25 &&
                    this.sprite.box.pos.y === 40,
                    'box.pos initialized ok'
                );
                assert.ok(
                    this.sprite.box.w === 50 &&
                    this.sprite.box.h === 150,
                    'box.w and box.h initialized ok'
                );
                assert.ok(this.sprite.image instanceof HTMLImageElement, 'image type ok');
                assert.ok(this.sprite.image === this.image, 'image initialized ok');
                assert.ok(this.sprite.angle === 0, 'angle initialized ok');
                assert.ok(this.sprite.clipBox instanceof Rumi.ESATBox, 'clipBox type ok');
                assert.ok(
                    this.sprite.clipBox.pos.x === 0 &&
                    this.sprite.clipBox.pos.y === 0,
                    'clipBox.pos initialized ok'
                );
                assert.ok(
                    this.sprite.clipBox.w === this.image.naturalWidth &&
                    this.sprite.clipBox.h === this.image.naturalHeight,
                    'clipBox.w and clipBox.h initialized ok'
                );
                assert.ok(this.sprite.scale instanceof Rumi.ESATVector, 'scale type ok');
                assert.ok(
                    this.sprite.scale.x === 1 && this.sprite.scale.y == 1,
                    'scale initialized ok'
                );
                assert.ok(this.sprite.blendMode === Rumi.BlendModes.SOURCE_OVER, 'blendMode initialized ok');
                assert.ok(this.sprite.alpha === 1, 'alpha initialized ok');
                assert.ok(
                    this.sprite.eventsManager.events['onBoxChange'] !== undefined &&
                    this.sprite.eventsManager.events['onBoxChange'] instanceof Rumi.Event,
                    'onBoxChange event registered and triggerable'
                );
                assert.ok(
                    this.sprite.eventsManager.events['onClipBoxChange'] !== undefined &&
                    this.sprite.eventsManager.events['onClipBoxChange'] instanceof Rumi.Event,
                    'onClipBoxChange event registered and triggerable'
                );
                assert.ok(
                    this.sprite.eventsManager.events['onScaleChange'] !== undefined &&
                    this.sprite.eventsManager.events['onScaleChange'] instanceof Rumi.Event,
                    'onScaleChange event registered and triggerable'
                );
                assert.ok(
                    this.sprite.eventsManager.events['onImageChange'] !== undefined &&
                    this.sprite.eventsManager.events['onImageChange'] instanceof Rumi.Event,
                    'onImageChange event registered and triggerable'
                );
                assert.ok(
                    this.sprite.eventsManager.events['onAngleChange'] !== undefined &&
                    this.sprite.eventsManager.events['onAngleChange'] instanceof Rumi.Event,
                    'onAngleChange event registered and triggerable'
                );
            });

            QUnit.test('Fires proper events', function(assert) {
                var result = {
                        onBoxChangeFired: false,
                        onClipBoxChangeFired: false,
                        onImageChangeFired: false,
                        onScaleChangeFired: false,
                        onAngleChangeFired: false,
                        onAlphaChannelChangeFired: false,
                        onBlendModeChangeFired: false
                    },
                    func = {
                        onBoxChange: function () {
                            result.onBoxChangeFired = true;
                        },
                        onClipBoxChange: function () {
                            result.onClipBoxChangeFired = true;
                        },
                        onImageChange: function () {
                            result.onImageChangeFired = true;
                        },
                        onScaleChange: function () {
                            result.onScaleChangeFired = true;
                        },
                        onAngleChange: function () {
                            result.onAngleChangeFired = true;
                        },
                        onAlphaChannelChange: function() {
                            result.onAlphaChannelChangeFired = true;
                        },
                        onBlendModeChange: function() {
                            result.onBlendModeChangeFired = true;
                        }
                    };

                this.sprite.eventsManager.attachEventListener('onBoxChange', func.onBoxChange);
                this.sprite.eventsManager.attachEventListener('onClipBoxChange', func.onClipBoxChange);
                this.sprite.eventsManager.attachEventListener('onImageChange', func.onImageChange);
                this.sprite.eventsManager.attachEventListener('onScaleChange', func.onScaleChange);
                this.sprite.eventsManager.attachEventListener('onAngleChange', func.onAngleChange);
                this.sprite.eventsManager.attachEventListener('onAlphaChannelChange', func.onAlphaChannelChange);
                this.sprite.eventsManager.attachEventListener('onBlendModeChange', func.onBlendModeChange);

                this.sprite.box.pos.x = 25;
                this.sprite.clipBox.w = 12;
                this.sprite.image = null;
                this.sprite.scale.x = 2;
                this.sprite.angle = Rumi.math.toRad(15);
                this.sprite.alpha = 0.2;
                this.sprite.blendMode = Rumi.BlendModes.SOURCE_ATOP;

                assert.ok(result.onBoxChangeFired, 'onBoxChange fired');
                assert.ok(result.onClipBoxChangeFired, 'onClipBoxChange fired');
                assert.ok(result.onImageChangeFired, 'onImageChange fired');
                assert.ok(result.onScaleChangeFired, 'onScaleChange fired');
                assert.ok(result.onAngleChangeFired, 'onAngleChange fired');
                assert.ok(result.onAlphaChannelChangeFired, 'onAlphaChannelChange fired');
                assert.ok(result.onBlendModeChangeFired, 'onBlendModeChange fired');
            });
        }
    );

    QUnit.module('Spritesheet',
        {
            beforeEach: function(assert) {
                var done = assert.async(),
                    _that = this;
                this.sheet = null;
                Rumi.load.image('../img/hero.png', function(image) {
                    _that.sheet = new Rumi.Spritesheet(
                        new Rumi.ESATBox(
                            new Rumi.ESATVector(
                                12,
                                13
                            ),
                            25,
                            26
                        ),
                        image,
                        new Rumi.ESATVector(4, 4)
                    );
                    done();
                });
            }
        },
        function() {
            QUnit.test('Default constructor works', function(assert) {
                this.sheet = new Rumi.Spritesheet();
                assert.ok(this.sheet.box instanceof Rumi.ESATBox, 'box type ok');
                assert.ok(this.sheet.image instanceof HTMLImageElement, 'image type ok');
                assert.ok(this.sheet.frames instanceof Rumi.ESATVector, 'frames type ok');
                assert.ok(this.sheet.frames.x === 1 && this.sheet.frames.y === 1, 'frames value ok');
                assert.ok(this.sheet.scale instanceof Rumi.ESATVector, 'scale type ok');
                assert.ok(this.sheet.scale.x === 1 && this.sheet.scale.y === 1, 'scale value ok');
                assert.ok(
                    this.sheet.eventsManager.events['onBoxChange'] !== undefined &&
                    this.sheet.eventsManager.events['onBoxChange'] instanceof Rumi.Event,
                    'onBoxChange event registered and triggerable'
                );
                assert.ok(
                    this.sheet.eventsManager.events['onScaleChange'] !== undefined &&
                    this.sheet.eventsManager.events['onScaleChange'] instanceof Rumi.Event,
                    'onScaleChange event registered and triggerable'
                );
                assert.ok(
                    this.sheet.eventsManager.events['onImageChange'] !== undefined &&
                    this.sheet.eventsManager.events['onImageChange'] instanceof Rumi.Event,
                    'onImageChange event registered and triggerable'
                );
                assert.ok(
                    this.sheet.eventsManager.events['onAngleChange'] !== undefined &&
                    this.sheet.eventsManager.events['onAngleChange'] instanceof Rumi.Event,
                    'onAngleChange event registered and triggerable'
                );
                assert.ok(
                    this.sheet.eventsManager.events['onFrameChange'] !== undefined &&
                    this.sheet.eventsManager.events['onFrameChange'] instanceof Rumi.Event,
                    'onFrameChange event registered and triggerable'
                );
                assert.ok(
                    this.sheet.eventsManager.events['onFramesChange'] !== undefined &&
                    this.sheet.eventsManager.events['onFramesChange'] instanceof Rumi.Event,
                    'onFramesChange event registered and triggerable'
                );
                assert.ok(
                    this.sheet.eventsManager.events['onBlendModeChange'] !== undefined &&
                    this.sheet.eventsManager.events['onBlendModeChange'] instanceof Rumi.Event,
                    'onBlendModeChange event registered and triggerable'
                );
                assert.ok(
                    this.sheet.eventsManager.events['onAlphaChannelChange'] !== undefined &&
                    this.sheet.eventsManager.events['onAlphaChannelChange'] instanceof Rumi.Event,
                    'onAlphaChannelChange event registered and triggerable'
                );
            });
            QUnit.test('Constructor works', function(assert) {
                assert.ok(this.sheet.box instanceof Rumi.ESATBox, 'box type ok');
                assert.ok(this.sheet.box.pos.x === 12 && this.sheet.box.pos.y === 13, 'box pos value ok');
                assert.ok(this.sheet.box.w === 25 && this.sheet.box.h === 26, 'box w and h value ok');
                assert.ok(this.sheet.image instanceof HTMLImageElement, 'image type ok');
                assert.ok(this.sheet.frames instanceof Rumi.ESATVector, 'frames type ok');
                assert.ok(this.sheet.frames.x === 4 && this.sheet.frames.y === 4, 'frames value ok');
                assert.ok(this.sheet.scale instanceof Rumi.ESATVector, 'scale type ok');
                assert.ok(this.sheet.scale.x === 1 && this.sheet.scale.y === 1, 'scale value ok');
                assert.ok(
                    this.sheet.eventsManager.events['onBoxChange'] !== undefined &&
                    this.sheet.eventsManager.events['onBoxChange'] instanceof Rumi.Event,
                    'onBoxChange event registered and triggerable'
                );
                assert.ok(
                    this.sheet.eventsManager.events['onScaleChange'] !== undefined &&
                    this.sheet.eventsManager.events['onScaleChange'] instanceof Rumi.Event,
                    'onScaleChange event registered and triggerable'
                );
                assert.ok(
                    this.sheet.eventsManager.events['onImageChange'] !== undefined &&
                    this.sheet.eventsManager.events['onImageChange'] instanceof Rumi.Event,
                    'onImageChange event registered and triggerable'
                );
                assert.ok(
                    this.sheet.eventsManager.events['onAngleChange'] !== undefined &&
                    this.sheet.eventsManager.events['onAngleChange'] instanceof Rumi.Event,
                    'onAngleChange event registered and triggerable'
                );
                assert.ok(
                    this.sheet.eventsManager.events['onFrameChange'] !== undefined &&
                    this.sheet.eventsManager.events['onFrameChange'] instanceof Rumi.Event,
                    'onFrameChange event registered and triggerable'
                );
                assert.ok(
                    this.sheet.eventsManager.events['onFramesChange'] !== undefined &&
                    this.sheet.eventsManager.events['onFramesChange'] instanceof Rumi.Event,
                    'onFramesChange event registered and triggerable'
                );
                assert.ok(
                    this.sheet.eventsManager.events['onBlendModeChange'] !== undefined &&
                    this.sheet.eventsManager.events['onBlendModeChange'] instanceof Rumi.Event,
                    'onBlendModeChange event registered and triggerable'
                );
                assert.ok(
                    this.sheet.eventsManager.events['onAlphaChannelChange'] !== undefined &&
                    this.sheet.eventsManager.events['onAlphaChannelChange'] instanceof Rumi.Event,
                    'onAlphaChannelChange event registered and triggerable'
                );
            });
            QUnit.test('Fires proper events', function(assert) {
                var result = {
                        onBoxChangeFired: false,
                        onScaleChangeFired: false,
                        onImageChangeFired: false,
                        onAngleChangeFired: false,
                        onFrameChangeFired: false,
                        onFramesChangeFired: false,
                        onBlendModeChangeFired: false,
                        onAlphaChannelChangeFired: false
                    },
                    func = {
                        onBoxChange: function() {
                            result.onBoxChangeFired = true;
                        },
                        onScaleChange: function() {
                            result.onScaleChangeFired = true;
                        },
                        onImageChange: function() {
                            result.onImageChangeFired = true;
                        },
                        onFrameChange: function() {
                            result.onFrameChangeFired = true;
                        },
                        onFramesChange: function() {
                            result.onFramesChangeFired = true;
                        },
                        onBlendModeChange: function() {
                            result.onBlendModeChangeFired = true;
                        },
                        onAlphaChannelChange: function() {
                            result.onAlphaChannelChangeFired = true;
                        }
                    };

                this.sheet.eventsManager.attachEventListener('onBoxChange', func.onBoxChange);
                this.sheet.eventsManager.attachEventListener('onScaleChange', func.onScaleChange);
                this.sheet.eventsManager.attachEventListener('onImageChange', func.onImageChange);
                this.sheet.eventsManager.attachEventListener('onFrameChange', func.onFrameChange);
                this.sheet.eventsManager.attachEventListener('onFramesChange', func.onFramesChange);
                this.sheet.eventsManager.attachEventListener('onBlendModeChange', func.onBlendModeChange);
                this.sheet.eventsManager.attachEventListener('onAlphaChannelChange', func.onAlphaChannelChange);

                this.sheet.box.pos.x++;
                this.sheet.box.pos.y--;
                this.sheet.box.w++;
                this.sheet.box.h--;
                this.sheet.scale.x++;
                this.sheet.scale.y--;
                this.sheet.image = null;
                this.sheet.frame++;
                this.sheet.frames.x++;
                this.sheet.frames.y--;
                this.sheet.blendMode = '';
                this.sheet.alpha -= 0.4;

                assert.ok(result.onBoxChangeFired, 'onBoxChange fired');
                assert.ok(result.onScaleChangeFired, 'onScaleChange fired');
                assert.ok(result.onImageChangeFired, 'onImageChange fired');
                assert.ok(result.onFrameChangeFired, 'onFrameChange fired');
                assert.ok(result.onFramesChangeFired, 'onFramesChange fired');
                assert.ok(result.onBlendModeChangeFired, 'onBlendModeChange fired');
                assert.ok(result.onAlphaChannelChangeFired, 'onAlphaChannelChange fired');
            });
        }
    );

    QUnit.module('Text',
        {
            beforeEach: function() {
                this.text = new Rumi.Text(new Rumi.ESATVector(12, 13), 'Test');
            }
        },
        function() {
            QUnit.test('Default constructor works', function(assert) {
                this.text = new Rumi.Text();
                assert.ok(this.text.box instanceof Rumi.ESATBox, 'box type ok');
                assert.ok(this.text.string === 'Default text content', 'string value ok');
                assert.ok(this.text.fontFamily === 'Arial', 'fontFamily value ok');
                assert.ok(this.text.fontSize === 16, 'fontSize value ok');
                assert.ok(this.text.fill instanceof Rumi.Fill, 'fill type ok');
                assert.ok(this.text.stroke instanceof Rumi.Stroke, 'stroke type ok');
                assert.ok(
                    this.text.eventsManager.events['onBoxChange'] !== undefined &&
                    this.text.eventsManager.events['onBoxChange'] instanceof Rumi.Event,
                    'onBoxChange event registered and triggerable'
                );
                assert.ok(
                    this.text.eventsManager.events['onStringChange'] !== undefined &&
                    this.text.eventsManager.events['onStringChange'] instanceof Rumi.Event,
                    'onStringChange event registered and triggerable'
                );
                assert.ok(
                    this.text.eventsManager.events['onSizeChange'] !== undefined &&
                    this.text.eventsManager.events['onSizeChange'] instanceof Rumi.Event,
                    'onSizeChange event registered and triggerable'
                );
                assert.ok(
                    this.text.eventsManager.events['onFontSizeChange'] !== undefined &&
                    this.text.eventsManager.events['onFontSizeChange'] instanceof Rumi.Event,
                    'onFontSizeChange event registered and triggerable'
                );
                assert.ok(
                    this.text.eventsManager.events['onFontFamilyChange'] !== undefined &&
                    this.text.eventsManager.events['onFontFamilyChange'] instanceof Rumi.Event,
                    'onFontFamilyChange event registered and triggerable'
                );
                assert.ok(
                    this.text.eventsManager.events['onFillChange'] !== undefined &&
                    this.text.eventsManager.events['onFillChange'] instanceof Rumi.Event,
                    'onFillChange event registered and triggerable'
                );
                assert.ok(
                    this.text.eventsManager.events['onStrokeChange'] !== undefined &&
                    this.text.eventsManager.events['onStrokeChange'] instanceof Rumi.Event,
                    'onStrokeChange event registered and triggerable'
                );
            });
        }
    );
});